# [Democratic System](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94)

> state: **open** opened by: **Flarp** on: **2018-6-6**

We need some kind of democratic system for removing posts and all that junk. If the majority of the community supports it, a &quot;benevolent&quot; dictator isn&#x27;t required.

### Comments

---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94#issuecomment-395049048) on: **2018-6-6**

Okay let&#x27;s leave the main concern finding a GitHub alternative and focus on creating a democratic repository.

Outstanding idea. 
---
> from: [**Flarp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94#issuecomment-395049436) on: **2018-6-6**

We can nominate alternatives and acknowledge feedback without being owned by one person
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94#issuecomment-395049687) on: **2018-6-6**

Is there an easy way? 
---
> from: [**Flarp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94#issuecomment-395049850) on: **2018-6-6**

For what?
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94#issuecomment-395050071) on: **2018-6-6**

For that &quot;some kind of democratic system&quot;. 
---
> from: [**Flarp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94#issuecomment-395050274) on: **2018-6-6**

Reaction, maybe? It&#x27;s primitive but quick (just a suggestion)
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94#issuecomment-395050921) on: **2018-6-6**

I don&#x27;t know. I think we should focus on solving the problem. I&#x27;m fine with the repository owner. But if are willing to put the necessary effort to make it happen, OK then. 
---
> from: [**Flarp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94#issuecomment-395053795) on: **2018-6-6**

We could have some kind of democratic bot running, I could do that with a little help.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94#issuecomment-395578022) on: **2018-6-7**

Hey guys. If you want to build a democratic system that would be amazing if you succeeded. Please read #114.
---
> from: [**Flarp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94#issuecomment-395728183) on: **2018-6-8**

Awesome, I&#x27;ll look for a democratic git merge bot.
