# [Any decentralized version idea?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44)

> state: **open** opened by: **genacrys** on: **2018-6-5**

&#x60;gitchain&#x60;... maybe?

### Comments

---
> from: [**ghost**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394577041) on: **2018-6-5**

https://git.scuttlebot.io/%25n92DiQh7ietE%2BR%2BX%2FI403LQoyf2DtR3WQfCkDKlheQU%3D.sha256 is i think what you&#x27;re looking for????
---
> from: [**boxdot**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394612830) on: **2018-6-5**

I would love to see a federation of self-hosted GitLab instances. The is already an issue on GitLab: https://gitlab.com/gitlab-org/gitlab-ee/issues/4517
---
> from: [**covrom**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394627725) on: **2018-6-5**

try build it based on gogs.io
---
> from: [**Bystroushaak**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394649485) on: **2018-6-5**

[Gitchain: That decentralized GitHub story... or &quot;What happens after Microsoft acquires GitHub?&quot;](https://www.kickstarter.com/projects/612530753/gitchain/posts/2205202)
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394650550) on: **2018-6-5**

Perhaps we can build a &quot;network&quot; of independently-hosted GitLab (or similar) instances and join those ? Or perhaps collectively contribute for expenses regarding the hosting of one or more community GitLab (or similar) instance(s).

The way Hyperboria handles new users joining (one needs to ask an existing member to enter the network) may be of interest.
---
> from: [**revelt**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394651659) on: **2018-6-5**

I think all open source should be hosted by a non-profit company, like Mozilla-level. It&#x27;s pity they don&#x27;t step in.
---
> from: [**Bystroushaak**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394711537) on: **2018-6-5**

@Serphentas I would contribute (with money) to something like gitlab federation.
---
> from: [**Bystroushaak**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394712300) on: **2018-6-5**

@revelt I think it is better idea to make it decentralized in the way email or jabber is decentralized. I would rather have all my data on my server, if it didn&#x27;t kill discoverability and community spirit (no one will register and login on my forgotten web).
---
> from: [**revelt**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394712623) on: **2018-6-5**

@Bystroushaak logical.
---
> from: [**YerkoPalma**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394713986) on: **2018-6-5**

Hi 👋 
Is nice that you are looking at Gitlab, but descentralized github has existed for a long time ago. As @ghost said

Here is a good intro: https://github.com/noffle/git-ssb-intro

---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394717214) on: **2018-6-5**

https://zeronet.io/ This project may help for this purpose. A FOSS which makes web apps decentralized.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394720722) on: **2018-6-5**

@ferittuncer I wonder how this compares to Hyperboria, though in essence I think the real problem is finding a service that acts like GitHub. The actual way of hosting that service (if community-driven) is yet another issue.
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394735462) on: **2018-6-5**

@Serphentas Nice project, to my understanding Hyperboria is a transport layer solution, help to decentralize the media we transmit our data.

ZeroNet is an application layer solution. It uses WWW at the moment but not restricted to any transport layer afaik. 

ZeroNet can help us to build a decentralized GitHub, where each user also hosts the platform. This will guarantee censorship resistance and high-availability.

To build such service, I think we can use git-ssb and we need to build a front-end. We adapt gogs.io front-end to this hypothetical solution as it&#x27;s open-source.
---
> from: [**oltdaniel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394745050) on: **2018-6-5**

https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25 We are talking about how to build a decentralized version, that is from the community, for the community.
---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394791371) on: **2018-6-5**

@ferittuncer @Serphentas There&#x27;s already a GitHub-like alternative on ZeroNet, called GitCenter (created by @imachug). It uses ZeroNet&#x27;s merger site concept, which allows users to create their own sites for their repositories, and then the Git Center site aggregates all the merger sites a person has downloaded into one interface.
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394804472) on: **2018-6-5**

@krixano Thanks for heads up, gonna check it. 
---
> from: [**charjac**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394804677) on: **2018-6-5**

https://github.com/noffle/git-ssb-intro
---
> from: [**millette**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394863001) on: **2018-6-5**

@ferittuncer in &lt;https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394717214&gt; mentions ZeroNet, specifically &lt;https://github.com/imachug/gitcenter&gt; as mentionned in #72.
---
> from: [**ralphtheninja**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-394901017) on: **2018-6-6**

Federated git is definitely what we need. Split those servers and enable a user on any server to interact with everyone else!
---
> from: [**AnaRobynn**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395046092) on: **2018-6-6**

Don&#x27;t re-invent the wheel: https://github.com/noffle/git-ssb-intro
---
> from: [**millette**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395302138) on: **2018-6-7**

A few more options out there:

* &lt;https://blog.printf.net/articles/2015/05/29/announcing-gittorrent-a-decentralized-github/&gt;
* &lt;https://github.com/raybejjani/gitsync&gt;

---
> from: [**Bystroushaak**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395378298) on: **2018-6-7**

https://github.com/git-federation/gitpub
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395400383) on: **2018-6-7**

@AnaRobynn GIT-SSB is already on the [comparison spreadsheet](https://docs.google.com/spreadsheets/d/1LZryaOx4VYHQ_oWLtRjvFrr4RhVjR484eFe7du3Usa4). Feel free to contribute there.

@millette @Bystroushaak  See above, you can suggest other alternatives over there.
---
> from: [**ralphtheninja**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395433724) on: **2018-6-7**

The &#x60;scuttlebot&#x60; column should be removed.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395435051) on: **2018-6-7**

@ralphtheninja How so ?
---
> from: [**ralphtheninja**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395471271) on: **2018-6-7**

@Serphentas Because it&#x27;s not related to git or github at all, &#x60;git-ssb&#x60; is an application built on top of &#x60;scuttlebot&#x60;.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395503106) on: **2018-6-7**

Makes sense, removing it then.
---
> from: [**ralphtheninja**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395543269) on: **2018-6-7**

Just got some news from @noffle

&#x60;&#x60;&#x60;
npm i -g hypergit git-remote-hypergit &amp;&amp; \
git clone hypergit://6b3752a2c3b04ed25ae2b4d380f3ed4d3d46a9d6b42850a3522e2dd80c332387 \
lisp-code
&#x60;&#x60;&#x60;

&#x60;git&#x60; over &#x60;dat&#x60; :fireworks: 
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395557497) on: **2018-6-7**

@ralphtheninja Could you please give more details about git over dat? 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395575656) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
