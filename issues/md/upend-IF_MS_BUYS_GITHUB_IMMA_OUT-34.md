# [Addressing the problem of community](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/34)

> state: **open** opened by: **aaronduino** on: **2018-6-4**

While alternatives such as gitlab, gitea, gogs, etc, are great for internal projects where the authors rightly want to be in control of their own data, they (at least for the time being) lack the community that drives many to GitHub.

More specifically, the following may need to be addressed:

- stars. This positive feedback is a big motivator for many of my open source developer friends.
- visibility. Very few Show HNs are projects hosted on gitlab. Self-hosted repos are harder to find online.
- professional-ness. It is commonplace (afaik) to share one&#x27;s GitHub profile page with peers and recuiters.

This is by no means a complete list. Feel free to list more things and they come to mind.

### Comments

---
> from: [**bashrc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/34#issuecomment-394446518) on: **2018-6-4**

I see visibility as being the main issue. You can always give a repo URL to peers or recruiters. It doesn&#x27;t have to be Github.

I&#x27;ll be thinking about ways to publish project summaries to some kind of p2p directory. FSF has its software directory, but I think something easier is needed.
---
> from: [**Bystroushaak**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/34#issuecomment-394471459) on: **2018-6-4**

I learned today that gitlab has its own section for hosted projects just like github. It just isn&#x27;t much visible; https://gitlab.com/explore
---
> from: [**bashrc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/34#issuecomment-394476415) on: **2018-6-4**

But unfortunately gitlab.com is a similar kind of deal to Github. Venture backed and presumably will be bought at some point, probably far more cheaply than $7.5 billion. It also runs on Microsoft Azure.
---
> from: [**Bystroushaak**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/34#issuecomment-394477806) on: **2018-6-4**

It is, but at least you can self-host it. And also there are propositions to make it federated:

* https://gitlab.com/gitlab-org/gitlab-ce/issues/4013

I think there will be much more pressure to implement it now.
---
> from: [**cmatzenbach**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/34#issuecomment-394482386) on: **2018-6-4**

@Bystroushaak That is true, but it&#x27;s not the issue here.  People are missing the point.  This isn&#x27;t (or rather, shouldn&#x27;t) be some #deletefacebook thing about privacy - this is about GitHub, a company that just got sold for $7.5 billion solely because it has been the choice hub of the open source movement these past ten years, selling the platform to a company who has been the biggest enemy to open source software.  And no, the past five years don&#x27;t make up for the past decades they&#x27;ve spent fighting FOSS tooth and nail.  They were losing out as web dev exploded and no one wanted to pay for their enterprise tools, so to stay relevant they opened up to linux and open source.  Then they realized that by doing so they can keep people on their proprietary platform and continue to make money, so they expanded things like WSL.  They&#x27;re not doing it for the good of the community, it&#x27;s all for profit.  Which is fine, that&#x27;s the point of a company!  BUT that wasn&#x27;t the point of GitHub, and that&#x27;s certainly not why it became so popular.  We built it, and they sold it to the very people who have been trying to destroy this movement since the late 90s.
---
> from: [**nukeop**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/34#issuecomment-394979938) on: **2018-6-6**

The only thing that changed in the past 5 years is that they started seeing &quot;open source&quot; developers as a source of free labor that can be easily exploited. Many people contribute to VS Code for example, but all credit goes to Microsoft, it continues to be seen as a Microsoft project, and Microsoft is being praised for its development. They never license their work under GPL, they use MIT, or other licenses that make the projects useless for anyone that isn&#x27;t Microsoft if they can get away with it.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/34#issuecomment-395578378) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
