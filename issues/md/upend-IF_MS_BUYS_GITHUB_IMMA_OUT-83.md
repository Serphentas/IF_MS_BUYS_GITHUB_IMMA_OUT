# [A p2p decentralized repository](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/83)

> state: **open** opened by: **roqueando** on: **2018-6-6**

Creating a repository platform like Dat Base based on p2p network or blockchain concept.

### Comments

---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/83#issuecomment-394935110) on: **2018-6-6**

DatBase is a good project. We need more information though. I&#x27;ll dig it when I get on my computer.

Meanwhile, we found such application but on ZeroNet. Please see #72
---
> from: [**RangerMauve**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/83#issuecomment-395121378) on: **2018-6-6**

Some existing info about using git with dat:

https://github.com/noffle/git-remote-hypergit
https://github.com/benwiley4000/hypergit
https://github.com/alexmorley/dgit-web
https://github.com/isomorphic-git/isomorphic-git/issues/97
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/83#issuecomment-395578103) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
---
> from: [**RangerMauve**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/83#issuecomment-395579550) on: **2018-6-8**

If someone wants to talk more about this in the context of dat, go here: https://gitter.im/datproject/discussions
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/83#issuecomment-395911710) on: **2018-6-9**

reminder this forum is shutting down in 5 hours 20 minutes. 
