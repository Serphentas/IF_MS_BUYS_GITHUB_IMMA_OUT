# [Need an unbiased person to lead the writeup of GitHub alternatives ](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32)

> state: **open** opened by: **vassudanagunta** on: **2018-6-4**

I&#x27;m not an expert and have lived in GitHub too long to know the alternatives.

Can someone volunteer?  We either need a single person who is very unbiased, or two or more volunteers who can cancel out each other&#x27;s biases. 

All the alternatives have pros and cons, and we owe it to the people reading a fair and honest assessment. 

Please use [issues tagged GitHub alternatives](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/labels/GitHub%20alternatives) as a resource of people and their ideas.

### Comments

---
> from: [**ghost**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32#issuecomment-394461440) on: **2018-6-4**

While I’m unable to specifically help with your request, I’d like to for now _at least_ suggest people have a look @ https://savannah.nongnu.org/ - it looks promising for projects that are 100% FOSS. While this may not suit everyone’s needs, I believe it’s a good refuge for those projects that are free of any proprietary code. They also state that any projects hosted with them are also able to be run on open source devices. I’ve just joined, so I’ll have to look more closely as to how that may be interpreted, e.g. would it be okay to host projects that run on both open and closed devices. Considering that GNU’s own software is run on devices that aren’t necessarily 100% open, I think there’s definite room for interpretation. Thank-you for the work and time being given to this takeover.
---
> from: [**Bystroushaak**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32#issuecomment-394472130) on: **2018-6-4**

&gt; While I’m unable to specifically help with your request, I’d like to for now at least suggest people have a look @ https://savannah.nongnu.org/

It looks horrible. It is badly designed and doesn&#x27;t follow &quot;code first&quot; principle. And also it is missing most of the social features github has.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32#issuecomment-394473235) on: **2018-6-4**

@Bystroushaak you don&#x27;t have to be unkind to be honest. 
---
> from: [**Bystroushaak**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32#issuecomment-394476203) on: **2018-6-4**

Github replacement question isn&#x27;t really about how to host git repos. That is solved by many projects. It is about the social part and the explorability of projects. It is really easy to self-host your repositories in some forgotten part of the internet. But no one will find them, no one will comment your issues, no one will register on your website and send pull requests. Github is really unique in being _social network for programmers_. That will be hard to replace.
---
> from: [**Bystroushaak**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32#issuecomment-394476725) on: **2018-6-4**

@vassudanagunta sorry, I am not a native speaker and my power to express exactly what I want to express is limited. If it sounded unkind, then I take it back.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32#issuecomment-394480149) on: **2018-6-4**

@Bystroushaak likewise I&#x27;m sorry I implied you were unkind. Thanks for being so kind about this!
---
> from: [**ghost**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32#issuecomment-394520156) on: **2018-6-5**

@Bystroushaak,
Hello. As I stated its FOSS focus may not be for everyone, especially considering that, being a GNU.org non profit, community sponsored hosting site, its definitely not GitHub or any of GitHub’s clones. Having been a member of the Free Software Fondation for nearly a decade has given me a bias toward FOSS, thus the reason I’m not able to directly help with @vassudanagunta’s request. It also may require learning new skills for those unfamiliar with Nix. I do appreciate your feedback, though I’m unsure if any real time was spent digging about its workings. At the very least, I’d hope people would create accounts to learn about the features available to members before passing judgement on any site. My _suggestion_ has certainly caused a bit of misunderstanding, though I supppse any single suggestion will not be adapted by every single person. And that is, I believe, the point to freedom no matter the realm. ☮️
---
> from: [**likethemammal**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32#issuecomment-394596363) on: **2018-6-5**

Didn&#x27;t know where to put this, but could the Internet Archive help with alternatives? I know its centralized, but its already a non-profit and setup to handle requests from across the world.

 
---
> from: [**Bengt**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32#issuecomment-394888666) on: **2018-6-6**

Additional alternatives: https://tutswiki.com/github-alternatives/
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/32#issuecomment-395577108) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
