# [troll](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/49)

> state: **closed** opened by: **kdambekalns** on: **2018-6-5**

I am an attention seeking troll. I violated the rules of this place. On top of that, I posted a link to this violation on Twitter to get more attention for myself.

### Comments

---
> from: [**Salakar**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/49#issuecomment-394620702) on: **2018-6-5**

I think this all comes down to GitHub historically being an independent entity free from having it&#x27;s strings pulled by some large corporations agenda.

Microsoft has been doing good by OSS lately however the times they&#x27;ve done bad by it are over a much longer period, Satya genuinely seems full on OSS supportive which is great definitely. I guess the worry is all it takes is different leadership at MS for them to change back to their old ways.

Personally though I&#x27;m staying put. Am still concerned obviously about it all but, GH/OSS has become my life and no other site has the community aspects nailed like GH does.

A shame really that there&#x27;s going to be fragmentation now. 

Be interesting to see what comes of this as there&#x27;s obviously a large number of people who are genuinely concerned, and when developers face a problem together they generally build something, that&#x27;s what I&#x27;m here for 👀
