# [Copy GitHub stars to GitLab](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/109)

> state: **open** opened by: **pawamoy** on: **2018-6-7**

I wrote a small Bash script to copy the stars you gave from GitHub to GitLab :slightly_smiling_face: Of course the repositories you starred must exist on GitLab on the same namespace and with the same name (both case-insensitive).

https://gitlab.com/snippets/1721089

I&#x27;ve wrote it because I love GitLab (I&#x27;m a fan of all-in-one) and I wanted to bring a bit of GitHub&#x27;s great community to GitLab. One of the ways is using stars. Also I simply want to enjoy my starred list on GitLab.

If someone wants to improve it or integrate in some bigger project, I don&#x27;t mind it.

I posted here because I think it can help in transitioning (or simply mirroring) from GitHub to something else, for people interested. Surely the script can be adapted to other centralized Git services.


### Comments

---
> from: [**pawamoy**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/109#issuecomment-395438684) on: **2018-6-7**

Actually I rewrote it in Python :grinning: More robust and cross-platform :slightly_smiling_face: 

https://gitlab.com/pawamoy/moving-stars
https://github.com/Pawamoy/moving-stars
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/109#issuecomment-395579992) on: **2018-6-8**

Hey @Pawamoy. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
---
> from: [**DavidGriffith**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/109#issuecomment-395605851) on: **2018-6-8**

This brought to mind a deficiency in Gitlab.  There is no way there (at present) to star a snippet or track changes in them using Git.  See https://gitlab.com/gitlab-org/gitlab-ce/issues/47557.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/109#issuecomment-395911631) on: **2018-6-9**

reminder this forum is shutting down in 5 hours 20 minutes. 
