# [Request for sanity / honesty from repo owners](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85)

> state: **closed** opened by: **mcmonkey4eva** on: **2018-6-6**

There are a fair number of issues/PR&#x27;s where the posts written by users have been deleted/modified by the repo owner.

While it is reasonable to say that you don&#x27;t want troll/counter-productive posts, and close/re-title them, it is not reasonable to alter or suppress the content of the posts.

How can you claim to be &#x60;a resistance movement for a free, open and people-driven internet in service of a free, open and people-driven world&#x60; (src: README.md) if you are actively censoring opposition?
Regardless of your preference or ideology, you can surely see how that action is not in alignment with stated goals?

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85#issuecomment-394931183) on: **2018-6-6**

When I have time I&#x27;ll address this. Thank you for being respectful.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85#issuecomment-394972436) on: **2018-6-6**

@mcmonkey4eva There&#x27;s more I&#x27;d want to say, but I don&#x27;t have time. So for now [this will have to do](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394995909). 

I think it&#x27;s perfectly fair for the people in the non-violent civil rights movement to take down the Black Panther posters and the KKK posters posted in their meeting halls against their wishes.

Can&#x27;t debate this anymore. I hope you get it now. 
---
> from: [**neko**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85#issuecomment-395254679) on: **2018-6-7**

there&#x27;s a difference between censoring hate speech and censoring opinions that don&#x27;t align with your own.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85#issuecomment-395255799) on: **2018-6-7**

@neko you&#x27;re not listening nor addressing my concerns, just restating your opinion. 

I have ZERO power to censor people&#x27;s opinions. I am just saying they don&#x27;t get to state those opinions here. **This IS NOT an open forum.**

Read my points at the top of #91. Tell my how you would run the ACLU? Because you are fighting for the KKK&#x27;s free speech rights, does that mean you would allow them into your ACLU planning and organizing meetings? What if they wanted to add anti-Semitic or racist items to the agenda? What if when you said no, they kept interrupting the meeting with complaints that you were censoring them?

Have you ever led a group or a meeting of a large number of people not of the same mind? 


---
> from: [**neko**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85#issuecomment-395257372) on: **2018-6-7**

you&#x27;re comparing people talking about microsoft not being the end of the world with anti-semites and racists - complete madness rly, that is why i will not be addressing your opinion.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85#issuecomment-395257692) on: **2018-6-7**

My example was extreme to make it clear. The question is still valid. But the truth is that you don&#x27;t have an answer. 
---
> from: [**neko**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85#issuecomment-395258295) on: **2018-6-7**

i will answer it to humour you.

i would allow people to critique my cause and engage in mature conversation with me about it - as opposed to outright censoring anything that does not align with my own opinions. ofc if they&#x27;re disrespectful they deserve to get removed from the conversation.

&gt;Have you ever led a group or a meeting of a large number of people not of the same mind?

ye
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85#issuecomment-395259731) on: **2018-6-7**

Don&#x27;t be condescending with this &quot;I will humour you&quot; line. Are you a child?

You didn&#x27;t answer my question, you just made up a new one that was easy to answer.

But as I said in the other thread, it won&#x27;t matter very soon. You get last word. Bye.
---
> from: [**neko**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85#issuecomment-395260094) on: **2018-6-7**

Don&#x27;t be condescending with this &quot;Are you a child?&quot; line. Are you a child?

What question are you referring to? You asked at least 4 in the one paragraph, so i sort of just condensed it into &quot;What would you do?&quot; as it seemed like the recurring theme.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/85#issuecomment-395581200) on: **2018-6-8**

Please read #114.
