# [Some suggested alternatives and how-tos](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14)

> state: **open** opened by: **jussihi** on: **2018-6-4**

Maybe there should be a list of some alternatives/tutorials on where to/how to migrate away from github. For example, GitLab just released this yesterday;

https://www.youtube.com/watch?v&#x3D;VYOXuOg9tQI

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394241433) on: **2018-6-4**

That&#x27;s totally the plan, @jussihi! But GitLab is doing much to promote itself, taking advantage of this news. So I&#x27;d rather host a neutral discussion of alternatives.  I really need some sleep, but will work on something tonight or in the morning. I hope you can help.
---
> from: [**xkr47**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394244758) on: **2018-6-4**

More alternatives:
* https://news.ycombinator.com/item?id&#x3D;17213780
* https://news.ycombinator.com/item?id&#x3D;17223477
* https://news.ycombinator.com/item?id&#x3D;17223675
* https://news.ycombinator.com/item?id&#x3D;17223201
---
> from: [**sguzman**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394264847) on: **2018-6-4**

Why is GitLab capitalizing on this opportunity a problem for you?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394277852) on: **2018-6-4**

Did I say it was a problem for me? I&#x27;m saying they are a for profit business and they can take care of themselves. If the community decides on its own that GitLab is a good alternative, then great. But that should be done on the merits, not based on fandom.
---
> from: [**sguzman**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394283571) on: **2018-6-4**

The way you phrased it originally implied some degree of skepticism or animosity towards GitLab which didn&#x27;t seem substantiated.

Why does them being a profit business matter? Github is a profit business too. 

I just think you&#x27;re dismissing GitLab prematurely. They seem to currently be the forerunner for being able to pickup the torch that Github is putting down. Whether that comes to fruition, we&#x27;ll have to see what the community chooses. 
---
> from: [**Rymix**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394302223) on: **2018-6-4**

Just windows bashing 
---
> from: [**bashrc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394308527) on: **2018-6-4**

I saw that video, but I think Gitlab is a similar deal to Github. It&#x27;s marginally better because Gitlab is actually free software, but gitlab.com could still be bought in a similar way. Gitlab is also too big and maintenance heavy for me to self-host. So I&#x27;ll be moving elsewhere.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394440519) on: **2018-6-4**

Subsumed by #32. This thread will be used a a resource.
---
> from: [**franzos**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394667634) on: **2018-6-5**

For very lightweight use, this may work: https://keybase.io/blog/encrypted-git-for-everyone
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394779901) on: **2018-6-5**

But we actually need an open platform @franzos like GitHub and Bitbucket. 
---
> from: [**nkoehring**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-394835542) on: **2018-6-5**

@ferittuncer None of them is open. They are only free. A hosted, free and open – especially as in OpenSource – SCM service is not so easy to find. Gitlab is, what they by themselves call &quot;Open Core&quot;. The hosted version is Gitlab EE, which used to be under an OpenSource license. They later changed that because it was &quot;confusing&quot;. More to read about gitlab: https://akr.am/blog/posts/gitlab-isnt-really-open-source 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-395102955) on: **2018-6-6**

https://news.ycombinator.com/item?id&#x3D;17241487
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-395116184) on: **2018-6-6**

You misunderstood @nkoehring. I meant that we need an open platform where everyone can easily reach each other&#x27;s repositories discover what others do. I&#x27;m not saying GitHub and/or BitBucket is FOSS.


---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/14#issuecomment-395578073) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
