# [Decentralized (federated) git service using the ActivityPub standard](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/84)

> state: **open** opened by: **erikuhlmann** on: **2018-6-6**

[ActivityPub](https://activitypub.rocks) is a [new W3C standard](https://www.w3.org/TR/activitypub/) for decentralized, federated social networks. It is currently being implemented by networks such as [Mastodon](https://joinmastodon.org) (a decentralized social network with over 1 million users across all instances right now) and [PeerTube](https://joinpeertube.org/) (a decentralized BitTorrent-based Youtube alternative).

ActivityPub&#x27;s model fits every non-code aspect of github quite well - an ActivityPub-based system could easily have repos, stars, issue comments, pull requests, and even code reviews as federated updates - and as a bonus, these would be immediately compatible with other networks like Mastodon, which means people could e.g. star repos and open issues without ever leaving their Mastodon client! This could be the decentralized solution #44 is looking for.

As for actual code, git itself already provides decentralization as designed. Federated git servers could simply clone each other&#x27;s repos when requested by users, and as such repos would be become decentralized as soon as people from multiple servers start using them.

Obviously this would be a bit of a challenge given that ActivityPub is such a recent standard, but I think if there was enough interest and momentum, it wouldn&#x27;t take very long to at least build a proof-of-concept based on this.

### Comments

---
> from: [**erikuhlmann**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/84#issuecomment-395268463) on: **2018-6-7**

Turns out some people are already working on a standard for ActivityPub-based git services - wanted to link it here: https://github.com/git-federation/gitpub/tree/draft-0.1
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/84#issuecomment-395578502) on: **2018-6-7**

Hey @erikuhlmann. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
