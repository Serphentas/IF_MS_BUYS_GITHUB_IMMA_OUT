# [Remove sexist imagery from readme/CTA](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58)

> state: **closed** opened by: **mtwentyman** on: **2018-6-5**

While I understand the attempt at humor, this is the wrong message to be sending if you&#x27;re shooting for inclusiveness.  Please consider the wider community here and remove the image.

Also, thank you for raising awareness of this issue and I hope you don&#x27;t see this as a nitpick.

### Comments

---
> from: [**HollayHorvath**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394738603) on: **2018-6-5**

http://knowyourmeme.com/memes/distracted-boyfriend
---
> from: [**mtwentyman**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394747466) on: **2018-6-5**

As I said, I understand the attempt.  I am hoping you can see past the joke and understand that there are a lot of people who don&#x27;t see this meme as funny; they see yet another way the tech industry as a whole has a problem with sexism.  We can do better.
---
> from: [**Addvilz**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394753693) on: **2018-6-5**

A lot of people don&#x27;t find many things funny. I personally find it hilarious, even considering I do not agree with the premise of this repository in general. If you feel personally offended by something, it is your personal responsibility, not anyone elses. As for the &quot;wider community&quot;, on what grounds are you elected to represent interests of said &quot;wider community&quot;?...

https://i.imgflip.com/2bnlst.jpg
---
> from: [**TeoTwawki**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394755015) on: **2018-6-5**

I don&#x27;t see it as particularly sexist but there are better less overused meme images that could be better used there anyway so I am for replacing it. Just my 2 cents.
---
> from: [**SocketByte**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394768138) on: **2018-6-5**

what
---
> from: [**kacperduras**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394769162) on: **2018-6-5**

lol
---
> from: [**sci4me**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394776373) on: **2018-6-5**

I don&#x27;t see it as sexist.
---
> from: [**juandjara**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394780312) on: **2018-6-5**

Maybe if you find another meme to replace this one, people will accept it.
---
> from: [**Bengt**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394797656) on: **2018-6-5**

The repository title references a meme already, so why not stick to that?

http://knowyourmeme.com/memes/kanye-interrupts-imma-let-you-finish
---
> from: [**mtwentyman**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394800500) on: **2018-6-5**

@juandjara, I appreciate the suggestion, but I think it would support the argument that this element is important to the messaging.  I don&#x27;t think it is, but I&#x27;d be glad to see a replacement that made the point more inclusively.

Also, I hope it is very clear; I am _not_ suggesting people are sexist for finding this funny.  I am saying that there is ample room for misinterpretation with this choice of image because of the unfortunate history of sexism in tech.  I&#x27;d like to think that considering other people needn&#x27;t be seen as a creative straightjacket, but an opportunity to reach a broader audience.  I&#x27;ll also add, I&#x27;m no saint, I still make mistakes and am happy when people point them out to me, otherwise I can&#x27;t even consider the behavior and improve.

Either way, I think having these conversations matters, and regardless of the decision I appreciate the opportunity to speak with all of you, even if you disagree.  Thank you for adding your voices.
---
> from: [**xoores**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394803448) on: **2018-6-5**

It is **your personal problem** that you don&#x27;t think something is funny.

Let me give you few points that you can maybe try to think about:
- Why do you think that anyone is interested in knowing that you are offended?
- Why should anyone even care about you or your feelings that are completely irrelevant in this particular topic?
- Why do you think that everyone else around you should change just because your small mind feels offended?
- What makes you think that you are a valid authority in a field of &quot;what is funny&quot;? And please, don&#x27;t even try to start with that &quot;_everyone knows that I love humor, but this is just not funny_&quot; crap.
- And also who the fuck brings this crap to the topic that is completely irrelevant to the subject in question? No sane person would start spilling shit about sexism in a discussion about fleeing GitHub

Noone is obligated to wipe your ass. If you don&#x27;t like something, thats fine. But please don&#x27;t wipe your nitpicking shit on others. It is impolite.

(edit: accidentaly [ctrl-x]&#x27;ed a point)
---
> from: [**TeoTwawki**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394804488) on: **2018-6-5**

I would like to mention that github is the only repository host where I see people bring up how people might be offended by this or that. Its great to want to make the world a better place and have a cause but I think often people are not realizing they are making an issue where one really would not exist - while I do think a better image could be put there I also highly doubt anyone would be genuinely upset at this specific image rather and more likely would be simply trying to ise it to help a cause. 

This isn&#x27;t an accusation, I think your concern is genuine but misplaced because you&#x27;ve gotten used to people being professionally offended at the drop of a hat. I never see these things happen at gitlab.

I&#x27;m still in favor of meme replacement rather than removal.
---
> from: [**komali2**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394808152) on: **2018-6-5**

That&#x27;s the highest image quality I&#x27;ve ever seen of this meme, therefore it should stay up. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394811185) on: **2018-6-5**

As a man, the thing to do when you’re not sure is to ask women you respect. Especially in a male dominated arena such as tech. So I did. It turns out @mtwentyman is spot on. 

I’m in dentist chair at the moment (doing penance for my male blindness). I’ll take down the image using my phone asap. I’ll write a more detail reply here and on Twitter when I’m out of this chair. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394812094) on: **2018-6-5**

oh wait. i can just merge this. thank you so much @mtwentyman for the call out and for not being silent. 
---
> from: [**TeoTwawki**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394812155) on: **2018-6-5**

Could easily be turned out he other way. Instead of being seen as sexist againts *women* how about noticing it&#x27;s the stereotype of a man looking at another woman.

But I don&#x27;t think anyone would seriously make a big deal out of it.


¯\\\_(ツ)\_/¯  

@vassudanagunta maybe get a safer meme later?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394812971) on: **2018-6-5**

@mtwentyman can you delete the image from the repo too?
---
> from: [**xoores**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394814680) on: **2018-6-5**

@vassudanagunta: I was gonna reply that you can merge this now with that link ¯\\_(ツ)_/¯ You were faster.
---
> from: [**mtwentyman**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394819047) on: **2018-6-5**

@vassudanagunta yes!  I made the removal part of the PR to save a step.  I am hugely appreciative for your consideration of this request, I am truly thankful.
---
> from: [**xoores**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394840990) on: **2018-6-5**

Sorry for the late reaction - had to finish some work first...


I&#x27;m still quite afraid that it is never a good idea to try to &quot;pack&quot; a good idea with any ideology whatsover. 
And it does not matter how good or righteous that ideology might be in your eyes.

The reason being that people that would support you for the good idea itself will now be forced to support the &quot;whole package&quot; in which case you will lose them if they will not agree with said ideology in that package.

And call me crazy, because as hard as I try to look into it, I cannot find any connection between the &quot;_sex_&quot; and &quot;_Microsoft getting GitHub_&quot;. Maybe if we accept the premise that _Microsoft are a bunch of dicks_... Maybe then I would be able to somehow insert _genitals_ into this equation...

And more importantly it will attract unnecessary and irrelevant fights and trolling that will further derail the whole thing to the battle of social (in)justice, which in my opinion has nothing to do with the original topic of acquistion.

(edit: typo)
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394853846) on: **2018-6-5**

[Here&#x27;s my public mea-culpa on Twitter](https://twitter.com/UpEnd_org/status/1004098207494877185 ). Please heart or retweet it if you agree. My apologies to this group as my mistake was under the movement&#x27;s account, not my personal one. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394868909) on: **2018-6-5**

I banned @xoores from the org because his comment above goes completely against the ethos of UpEnd. We really need a Code of Conduct: #73
---
> from: [**TeoTwawki**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394870420) on: **2018-6-5**

Oh wow..I&#x27;m out. Good luck.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394873735) on: **2018-6-5**

@TeoTwawki Did you read his comment? 

&gt; your small mind

&gt; who the fuck brings this crap

&gt; No sane person would start spilling shit about sexism

&gt; Noone is obligated to wipe your ass. If you don&#x27;t like something, thats fine. But please don&#x27;t wipe your nitpicking shit on others. It is impolite.

If you think that is acceptable in an organization about making the world a better place, then yes, this may not be the place for you. 


---
> from: [**TeoTwawki**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394874111) on: **2018-6-5**

Comments can be deleted, users warned, issue threads locked. By banning there is zero chance he&#x27;ll decide to contribute productively without insults in the future.

You look to have no interaction prior to ban and that says &quot;if I offend someone in a disagreement, I may get banned&quot;
---
> from: [**TeoTwawki**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394874928) on: **2018-6-6**

And yes, this is not a place for me, hence leaving. I don&#x27;t care for either people like him or people like you.  Good luck with the movement sir, and I will leave you alone.
---
> from: [**albjeremias**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394966608) on: **2018-6-6**

hmpf.. my merge request was more epic... fight sexism! Fck this shit... 

![image](https://user-images.githubusercontent.com/26681446/41022687-99268aa2-696a-11e8-8b2d-006a85299338.png)

https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/51
---
> from: [**iwanttobethelittlegirl**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-395548961) on: **2018-6-7**

i am a childish, sexist troll
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-395594098) on: **2018-6-8**

RE censorship: please read Issue 114. @TeoTwawki
