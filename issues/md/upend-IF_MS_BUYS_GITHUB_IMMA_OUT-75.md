# [Canonical Launchpad.net](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/75)

> state: **open** opened by: **carverh** on: **2018-6-6**

[Launchpad.net](https://launchpad.net) is one of the most feature-full SCM solutions there is. It has BZR and GIT support as well as Issues, Q&amp;A, Teams, Projects and Blueprints.

### Comments

---
> from: [**carverh**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/75#issuecomment-395277869) on: **2018-6-7**

You are ok with Canonical but not Microsoft? What?
---
> from: [**Vdragon**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/75#issuecomment-395497394) on: **2018-6-7**

Yes, I&#x27;m OK with Canonical.  What?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/75#issuecomment-395576225) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
