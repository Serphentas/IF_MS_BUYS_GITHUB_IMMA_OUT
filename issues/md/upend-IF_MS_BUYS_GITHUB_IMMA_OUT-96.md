# [Backup copy of all conversations (issues/PRs) of this repo](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96)

> state: **closed** opened by: **limonte** on: **2018-6-6**

In case (when) they will delete this repo (#92), all our communication will be vanished.

As I see the solution, some service should periodically trigger GitHub API to fetch every issue and all issue comments and store them somewhere.

Does anyone have any idea how can we make a reserve copy of all conversations of this repo?

Whatwver the solution will be, it should be linked in the README, so all forks will have that information as well.

### Comments

---
> from: [**limonte**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395063672) on: **2018-6-6**

If they can remove the repo from trending list without any T&amp;C violations, what will stop them from removing it from GitHub? Seriously, @nicholas-c try to answer this question.
---
> from: [**nicholas-c**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395064490) on: **2018-6-6**

Because it&#x27;s simply not in their interest to do so? What would they gain from doing so? It&#x27;s a pretty simple precedent really.
---
> from: [**limonte**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395065695) on: **2018-6-6**

&gt; What would they gain from doing so?

Exactly the same as they&#x27;re gaining from removal this repo from trending. Prevent spreading information about GitHub issues and its alternatives. 

Fewer people will know about GitLab/BitBucket/whatnot &#x3D;&gt; More people will stay here &#x3D;&gt; Moar $$$ to M$. 
---
> from: [**limonte**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395070850) on: **2018-6-6**

So, in your reality @nicholas-c, what is the reason for removing this repo from trending? Just a small bug in GitHub algorithms? Or somebody gives just a couple of craps because this repo is growing quickly?

PS. just ordered a couple of foil caps from AliExpress ![image](https://user-images.githubusercontent.com/6059356/41041671-5c38ed4a-69a8-11e8-81a7-c9167db19ffd.png)

---
> from: [**tristanbettany**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395072110) on: **2018-6-6**

I imagine you&#x27;re just not as trendy as you thought you were :trollface: 
---
> from: [**limonte**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395092122) on: **2018-6-6**

Come on, guys! I&#x27;m trying to be paranoid here, stop posting foil-cap jokes and making me laugh :D
---
> from: [**7twin**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395182224) on: **2018-6-6**

@limonte are you planning on removing your repos like sweetalert2 from github?
---
> from: [**limonte**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395183999) on: **2018-6-6**

@7twin I don&#x27;t know yet. Probably no, at least not before M$ will start censorship. Anyways, the decision on moving out isn&#x27;t just mine, there are multiple collaborators in that repository and we will vote before making any decisions.
---
> from: [**evyatarmeged**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395186545) on: **2018-6-6**

@dkfiresky I almost died laughing.
---
> from: [**ghost**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395192128) on: **2018-6-6**

@evyatarmeged great to hear that.
---
> from: [**AnaRobynn**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395198285) on: **2018-6-6**

@limonte Microsoft has no power over GitHub yet... Sigh... 
---
> from: [**TwinProduction**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395262736) on: **2018-6-7**

I really doubt that they&#x27;ll delete this repo, but we could just make a discord group or a Slack.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395263320) on: **2018-6-7**

Guys, can you figure out the based/easiest way for users to export all the Issues and comments under them ASAP?  We are going to need it. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395264228) on: **2018-6-7**

Sorry I have no tolerance for people who consider this an open forum when it isn&#x27;t. But all of this won&#x27;t matter very soon. I&#x27;ll be making an announcement and a change to this repo so that everyone can have what they want (though they&#x27;ve always had that freedom).
---
> from: [**TwinProduction**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395264498) on: **2018-6-7**

@vassudanagunta Like I said, it&#x27;s probably a better idea to get all developers together in a discord or a slack instead of just trying to export the issues/comments. Issues by themselves have very little power -- but a group of developers do. If this repository were to be removed, we have no way of finding each others again.

_But I don&#x27;t see why the repository would be removed anyways, it&#x27;s not like we&#x27;re being disrespectful or causing havoc._
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395264918) on: **2018-6-7**

I&#x27;m a believer in self-organization and teams doing whatever works for them. That&#x27;s what makes a team. What we have here is not a team and will not gel into one because of all the differences. 

So please, organize as you see fit! I will post a link to whatever you set up.
---
> from: [**TwinProduction**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395266364) on: **2018-6-7**

@vassudanagunta Alright...

Well to respond to your question, you can access all the issues on a repository by using GitHub&#x27;s API:

https://api.github.com/repos/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/events

I uploaded them to an unlisted pastebin: https://pastebin.com/uJpB1n6z

As for the comments in each issues, &#x60; ¯\_(ツ)_/¯&#x60;



---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395267113) on: **2018-6-7**

The comments are more important than the issue titles... they contain the work of people so far, their research, their ideas.
---
> from: [**TwinProduction**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395267734) on: **2018-6-7**

It does not just contain the issues titles, it contains the issues description too.

I&#x27;m just trying to contribute here, it&#x27;s better than nothing. Quite frankly, you could also just save each issue one by one as &#x27;html page&#x27; if you&#x27;re really worried about it.

If we keep using the GitHub API approach, then you could use 

https://api.github.com/repos/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96/comments

to get the comments of the #96 

https://api.github.com/repos/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/94/comments

to get the comments of the #94 

etc.



---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395579305) on: **2018-6-8**

Here&#x27;s why a way to export issues and comments is really important: #114.

If you guys have a solution that everyone can use, that would be awesome. 
---
> from: [**limonte**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395839897) on: **2018-6-8**

This is terrible: 
![image](https://user-images.githubusercontent.com/6059356/41172823-b7bfeaa4-6b5d-11e8-8c32-10137311db2f.png)

I disagree with @nicholas-c but I do respect his opinions. He spent his personal time to put his thoughts and numbers into very structured comments and you vanished all his effort.

Good luck @vassudanagunta with what you&#x27;re doing. I was with you and your initiative from the very beginning, but now IMMA OUT from here.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/96#issuecomment-395841493) on: **2018-6-8**

@limonte See #116. I have not hit the delete button on anything on this site except those posts that disagreed with our issues with the acquisition, because this IS NOT a forum for that debate. 
