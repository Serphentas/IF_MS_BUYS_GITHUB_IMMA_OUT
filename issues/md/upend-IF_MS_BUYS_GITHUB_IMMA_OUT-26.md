# [Tone Down the Rhetoric](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26)

> state: **closed** opened by: **sethpuckett** on: **2018-6-4**

I understand that some folks are not happy about the acquisition, but the sincere &quot;refugee camp&quot; language in this repo is a bit much. Developers looking for a new repo platform is very different than families being ripped apart and struggling to survive in war-torn countries. It&#x27;s a little offensive, to be honest.

Maybe update the &#x60;readme&#x60; to refer to supporters as &quot;defectors&quot; (or something similar) instead of &quot;refugees&quot;? And maybe tone down some of the conflict zone rhetoric (&quot;resistance movement&quot;, &quot;escape routes&quot;)?

### Comments

---
> from: [**immexerxez**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394417256) on: **2018-6-4**

I agree this repo&#x27;s gone a little extreme I can&#x27;t agree with the idea of using certain words somehow delegitimizes someones struggle. It&#x27;s this sort of thinking is off topic and in a lot of ways dangerous. 
---
> from: [**bashrc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394423381) on: **2018-6-4**

I also agree, although elsewhere people moving out of silos onto the open web are usually called &quot;$systemname refugees&quot;.

Also people are passionate about their projects, and they don&#x27;t like it when the space they inhabited is being colonized by corporate schmucks with a solid history of antisocial and disruptive behavior against FOSS developers.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394428629) on: **2018-6-4**

@sethpuckett perhaps you are right.

[See my reply](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/24#issuecomment-394424060) to @immexerxez to understand why I chose those words, even if you are right that it would be better to change them. 

But #resist and #resistance are well establish terms of fighting &quot;the power&quot;,  There&#x27;s a reason this org is named [upend.org](http://www.upend.org). 

So, no, I&#x27;m not going to tone it down, because I think it&#x27;s appropriately toned up for the perspective and spirit of this organization and future website. But yes, if it actually is offensive to refugees, I/we should change those words. Though I&#x27;d like to hear from an actual refugee. They might disagree with you. They might think that the metaphor is appropriate and appropriately powerful. I&#x27;m willing to listen.

At some point, I don&#x27;t want this to be me running this thing. I&#x27;d rather have a team of people making collective decisions. But that team needs to have a shared vision. This isn&#x27;t up for vote from people with entirely different visions, the extreme case being a GitHub or Microsoft employee coming in here and steering it their way.

And I&#x27;m sorry if you misunderstood the spirit of this effort. Follow your heart. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394429308) on: **2018-6-4**

@bashrc 

&gt; I also agree, although elsewhere people moving out of silos onto the open web are usually called &quot;$systemname refugees&quot;.

I used &quot;refugee&quot; because this is the second time I&#x27;ve been part of a community that got destroyed by a takeover, and we called ourselves the &quot;Newsvine Refugees&quot;. Funny, only now that you make me think about it, it was Microsoft in that one too... it was a take-over of a citizenship journalism startup that we users poured our blood and sweat into for no pay, and then the founder just sold out to MSNBC for millions and destroyed everything we worked for.
---
> from: [**willyhakim**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394439103) on: **2018-6-4**

Ok, as a kid who grew up in the refugee camp, I honestly was disgusted by this. I mean, have you seen pictures of Syrian Refugees? Com&#x27;on man! The day you go for 5 days without a meal because of this acquisition, you&#x27;ll have my support to call this thing a refugee camp. For now, let&#x27;s respect the people who are dying everyday in the literal refugee camps
---
> from: [**immexerxez**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394440283) on: **2018-6-4**

This is off topic and is actually counterintuitive to the entire point of this repo. 
---
> from: [**ghost**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394440717) on: **2018-6-4**

1) I have gone 5+ consecutive days without a meal for some of the projects on github.  Now Microsoft has effective control of those projects, and renders my sacrifices moot.  These projects represent the life&#x27;s work of many people and extreme language is warranted for their sacrifices being made potentially in vain, especially if we don&#x27;t act quick to save them.

2) &#x27;refugee&#x27; has established meaning beyond refugee camps that does cover this kind of use

Maybe &#x27;refugee&#x27; is over the top, but do keep 1 and 2 in mind when considering just how much.
---
> from: [**sirjessthebrave**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394443837) on: **2018-6-4**

Tech does not exist in a vacuum and words matter.... ESPECIALLY in our current political climate.  There are plenty of ways to express anger, frustration, and anti-capitalism without using the language of refugees and their unique, dire struggle.  

Some good documentation for better language: 
http://www.thesaurus.com/
https://blog.thejuntoinstitute.com/hs-fs/hubfs/social-suggested-images/The_Junto_Institute_Emotion_Wheel.jpeg?t&#x3D;1527792324096&amp;width&#x3D;400&amp;name&#x3D;The_Junto_Institute_Emotion_Wheel.jpeg


---
> from: [**immexerxez**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394444992) on: **2018-6-4**

this is way out off topic and should be closed. The entire point of this repo was to promote a free and open Github. Free and open includes language **some** might deem **offensive**.  
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394445153) on: **2018-6-4**

Ok, message heard loud and clear. I&#x27;m sorry. didn&#x27;t mean to offend. I&#x27;m on it. Though I haven&#x27;t slept in 38 hours.

People have different opinions on this. But there are alternative expressions, so there&#x27;s no need to offend.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394445762) on: **2018-6-4**

I&#x27;d love it if someone can suggest a rewrite that maintains the spirit of resistance. Again see [my reply](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/24#issuecomment-394424060) to @immexerxez.  

Pull request yo! 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394447248) on: **2018-6-4**

&gt; this is way out off topic and should be closed. The entire point of this repo was to promote a free and open Github. Free and open includes language some might deem offensive.

Free and open also means allowing people to be led by their hearts, not their wallets. Free and open is about humanity. Free and open to form a community with a culture of your choosing. I choose love and empathy over attitudes of entitlement. 
---
> from: [**sirjessthebrave**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394448873) on: **2018-6-4**

&gt; Free and open is about humanity.

Humanity is about recognizing the lager context including language ect.

@vassudanagunta My ideas for better words include: renegade, outlaw, deserter, rebel

all have connotations of rebellion, resistance, and free, open communities somewhat outside the norm(capitalism) 
---
> from: [**immexerxez**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394449426) on: **2018-6-4**

this is not an anti-capitalism repo? This has nothing to do with economic systems? This is not on topic and is way off in the weeds. The irony of someone trying to promote freedom but being against capitalism. 
---
> from: [**sirjessthebrave**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394450467) on: **2018-6-4**

Bro we get it - you don&#x27;t agree.... 

@vassudanagunta  has mentioned being free of corporate control hence capitalism comments. It takes very little effort to update language if it offends people. It is on topic for those on the thread so deal with it.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394450703) on: **2018-6-4**

@immexerxez i&#x27;m getting the since we&#x27;re on different wavelengths. If you don&#x27;t align much with that long answer I gave you on the other thread (linked above a couple of times), then, frankly, I don&#x27;t think you fit. I&#x27;m not going to change the spirit of this. It was there from the get-go. Maybe you starred without looking.

Please don&#x27;t make a fuss if you don&#x27;t agree. Respect me, and those who agree and choose to stay.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394451476) on: **2018-6-4**

@willyhakim you up for helping with the wording? Can you keep it strong?
---
> from: [**immexerxez**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394451780) on: **2018-6-4**

I don&#x27;t think you do get it or you wouldn&#x27;t suggest changing rhetoric because someone may be offended. I starred because when I found this it had all the right intentions. 

---
> from: [**sirjessthebrave**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394452772) on: **2018-6-4**

@vassudanagunta Proposed PR: https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/35

If you find a better word than outlaw you can always update it but for now it allows anger without offense.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394462375) on: **2018-6-4**

OK folks, I changed the wording. &quot;Evacuation Center&quot; and &quot;evacuees&quot;.  That ok?

@sirjessthebrave thank you, but we&#x27;re the good guys. 
---
> from: [**sethpuckett**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394463542) on: **2018-6-4**

Thanks for updating the wording, @vassudanagunta! 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/26#issuecomment-394466778) on: **2018-6-4**

@sethpuckett @willyhakim Thanks for calling this out. If you knew me, you would know this was innocent. I call out others on this kind of stuff all the time, but in this case I simply did not make the connection (hence I did not offend myself).
