# [Want to hear a different side of Microsoft?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12)

> state: **closed** opened by: **adamstac** on: **2018-6-4**

We&#x27;ve done our best to cover the dramatic shift of Microsoft over the years.

Here are several episodes from our backlog to potentially give you a new perspective on this shift.

_(listed in chronological order)_

- [The Changelog #298 — The beginnings of Microsoft Azure with Julia White](https://changelog.com/podcast/298)
- [The Changelog #282 — The Impact and Future of Kubernetes with Brendan Burns and Gabe Monroy](https://changelog.com/podcast/282)
- [The Changelog #277 — The Story of Visual Studio Code with Julia Liuson, Chris Dias, and PJ Meyer](https://changelog.com/podcast/277)
- [The Changelog #275 — The History of GNOME, Mono, and Xamarin with Miguel de Icaza](https://changelog.com/podcast/275)
- [Go Time #63 — Changelog Takeover — K8s and Virtual Kubelet with Erk St. Martin and Brian Ketelsen](https://changelog.com/gotime/63)
- [The Changelog #256 — Ubuntu Snaps and Bash on Windows Server with Dustin Kirkland](https://changelog.com/podcast/256)
- [Go Time #50 — Bringing Kubernetes to Azure with Kris Nova](https://changelog.com/gotime/50)
- [Go Time #49 — Adventures in VS Code with Ramya Rao](https://changelog.com/gotime/49)
- [The Changelog #249 — Open Source at Microsoft, Inclusion, Diversity, and OSCON with Scott Hanselman](https://changelog.com/podcast/249)
- [Spotlight #13 — Node at Microsoft, ChakraCore, and VM Neutrality with Gaurav Seth and Arunesh Chandra](https://changelog.com/spotlight/13)
- [The Changelog #224 — .NET Core and Microsoft&#x27;s Shift to Open Source with Bertrand Le Roy](https://changelog.com/podcast/224)
- [The Changelog #207 — Ubuntu Everywhere with Dustin Kirkland](https://changelog.com/podcast/207)
- [The Changelog #152 — TypeScript and Open Source at Microsoft with Anders Hejlsberg and Jonathan Turner](https://changelog.com/podcast/152)
- [The Changelog #134 — Open Sourcing .NET Core with the Microsoft .NET team](https://changelog.com/podcast/134)

If you have questions, I&#x27;m here and happy to answer what I can.

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394230189) on: **2018-6-4**

This is not all about Microsoft. It&#x27;s about the **independence** of the de-facto home of the #opensource community. It shouldn&#x27;t be owned by any company with any agenda other than than hosting that home and taking care of that community.

Time to find a new home.
---
> from: [**adamstac**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394230563) on: **2018-6-4**

&gt; It shouldn&#x27;t be owned by any company with any agenda other than than hosting that home and taking care of that community.

How do you know that&#x27;s not the agenda of Microsoft?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394231275) on: **2018-6-4**

Seriously? I have a bridge to sell ya.

And you very conveniently skipped key words in my statement, forcing me to shout it: It shouldn&#x27;t be owned by any company with **ANY** agenda **OTHER THAN** hosting that home and taking care of that community.

That&#x27;s what independence needs. Conflict of interest and all. You know, like an independent Judiciary and and independent Justice Department that we are all counting on.
---
> from: [**quantumpacket**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394231564) on: **2018-6-4**

Just because they may appear to be turning over a new leaf does not make them opensource friendly. Their past record when it came to undermining opensource every chance they could, makes me think of them more as wolves in sheep&#x27;s clothing. We can agree to disagree.
---
> from: [**galengidman**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394233054) on: **2018-6-4**

GitHub is a privately-owned company and as such can sell to whoever they wish, for whatever reason they wish, whenever they wish. They don&#x27;t owe us anything. I&#x27;m sure that will piss most of the people in this thread off, but it&#x27;s still the truth. If you&#x27;re unhappy with the new ownership, no one is forcing you to use it.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394234094) on: **2018-6-4**

@galengidman no need to patronize. We all get that. This repo *is exactly about leaving GitHub* if they sell out. But thanks anyway for telling us to leave. 

Though as to &quot;owe&quot;... Sure, they don&#x27;t owe us anything *legally*. But this is a dishonest move. GitHub knows that the open source community wants independence. They know what the open source ethos is. But they just screwed it and us.
---
> from: [**adamstac**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394234486) on: **2018-6-4**

As you can see from the list above, we have literally been in the trenches for years covering this change.

Early on, we were _very skeptical_. Over time we kept hearing the same thing. The change was leadership. The change was Satya Nadella. Even in the current news coverage of this unofficial announcement, GitHub said it was because of Satya.

&gt; GitHub preferred selling the company to going public and chose Microsoft partially because it was impressed by Chief Executive Officer Satya Nadella... ([source](https://www.bloomberg.com/news/articles/2018-06-03/microsoft-is-said-to-have-agreed-to-acquire-coding-site-github))

If leadership at GitHub thinks that this would be a wise move for the future of GitHub and all that it is, and all the responsibility it has to open source, I want to try and see the bright side.

I&#x27;m just here to share our coverage from over the years to give you more perspective to consider as this story unfolds.
---
> from: [**golimpio**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394242561) on: **2018-6-4**

At least nowadays we have some alternatives to GitHub, so it&#x27;s not a big deal for some of us to move our code out of GitHub, in case Microsoft screw up with this service. 

MS is usually good in things like IDE&#x27;s and programming languages, where they succeeded in the open source universe. But they still have a recent record of screwing up with some services and companies (perhaps Skype and the old Nokia).

I&#x27;m not really confident that they won&#x27;t mess up with GitHub if their agenda is purely Azure integration and focus on enterprise repositories. 

This looks like the end of an era for GitHub and the new one is not so exciting.
---
> from: [**vladiibine**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394281423) on: **2018-6-4**

Step 1: Microsoft has its own technologies to promote, because its business relies on it
Step 2: There are other companies who have their own technologies to promote because their businesses rely on them
Step 3: It is within Microsoft&#x27;s interest that the technologies of its competitors see less adoption than its own.

These are the forces that are at play here.

Conclusion: If Microsoft owns Github, it&#x27;s only a matter of time until some technologies (Microsoft&#x27;s) get promoted more than others (their competitors&#x27;). It&#x27;s a conflict of interest.

No technology company should own a platform where all technologies are meant to have the right to free speech.
---
> from: [**gilday**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394308455) on: **2018-6-4**

&gt; This is not all about Microsoft. 

Maybe that was not the intention, but this does feel like a referendum on Microsoft&#x27;s stewardship of the open source community. If centralized power over the open source community is the problem, as #1 points out, that was already the case before the acquisition.

&gt; It&#x27;s about the independence of the de-facto home of the #opensource community. It shouldn&#x27;t be owned by any company with any agenda other than than hosting that home and taking care of that community.

The acquisition doesn&#x27;t feel like a fundamental change to me. GitHub is a business that has always needed to find revenue outside of their mission to host the open source community by selling proprietary software, support, and services (and I don&#x27;t fault them for it).
---
> from: [**HKochniss**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394310523) on: **2018-6-4**

Just my 2 cents: I think the &quot;new&quot; Microsoft is based on different values, I am a bit deeper in the MS ecosystem than most here (e.g. watching weekly asp.net show from the asp.net team itself), quite exposed to their info channels. The people there do excellent work. The engineers really &quot;get&quot; OS and would be good stewards. 

Problem is: it&#x27;s not the engineers making tha calls in such a big company in the long run. They certainly have more influence in engineering-focused orgs (like .NET group) and MS changed dramatically when they fired a whole lot of PMs (basically introduced the &quot;PMs have to be able to code&quot; rule )

But there is still the &quot;business&quot; arm of MS org (obviously, money has to flow), and unfortunately over time there will be an Azure focus for github... I think MS does itself a disservice buying Github, as there will be trust issues. I don&#x27;t want to see a fragmented OS community  - until now it was basically a no-brainer to host your code on Github. I quite like the productivity gain by Github, as well as the emerging 3rd party ecosystem on top of it.

I think it&#x27;s the wrong time. It would be better to wait at least 2 more years. But well.. 


---
> from: [**bashrc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394312822) on: **2018-6-4**

Heh.

Dramatic shift.

Microsoft couldn&#x27;t give a rats ass for software freedom. They despise people like me. All their ventures into FOSS, including funding the Linux Foundation, are narrowly self-serving. They continue to extort FOSS developers with software patents.

Microsoft are about as ethical as a drone strike on a wedding party. I&#x27;ve been around in software for a long time, so have seen their manoevrings. They boasted about the extensiveness of their cooperation with the NSA and bugged Skype after they bought it (read the Snowden docs).

I expect Microsoft to destroy Github as a FOSS platform but retain those aspects which contribute to their own systems and platforms (Visual Studio, etc). After a while Github will probably be highly integrated with VS.
---
> from: [**paulsapps**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394325948) on: **2018-6-4**

&quot;After a while Github will probably be highly integrated with VS.&quot;

It already is, and why do you care? That is a good thing.

---
> from: [**nawwa**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394327475) on: **2018-6-4**

Ppl are afraid when things change, that&#x27;s human dw 
---
> from: [**darkpixel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394336857) on: **2018-6-4**

@nawwa Microsoft has *repeatedly* screwed over developers, open source, and Linux in general.  While they may be turning over a new leaf with the new CEO, there are some of us who have been burned so many times it&#x27;ll take years and years for them to prove they aren&#x27;t up to something malicious.

&quot;Ppl are afraid when things change&quot; is not the reason we have a quote &quot;once bitten, twice shy&quot;.

https://dictionary.cambridge.org/us/dictionary/english/once-bitten-twice-shy
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394337666) on: **2018-6-4**

@nawwa 

&gt; Ppl are afraid when things change, that&#x27;s human dw

That&#x27;s a pretty condescending thing to say to people who see things differently. Feel free to stay on GitHub. But what&#x27;s your point of coming here and saying such things? Do you think you are going to convert people? Or are you just trolling, grandstanding, or bullying?
---
> from: [**paulsapps**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394338881) on: **2018-6-4**

It seems to me is he is correct. If MS change nothing most people here are saying they would leave the service anyway. What I would do is jump ship if something changed that I didn&#x27;t like.

For instance I am not a fan of Facebook, but if they bought GitHub I wouldn&#x27;t instantly leave. I would leave if they added a &quot;friends list&quot; linked to FB or other such features that I am not a fan of.

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394340304) on: **2018-6-4**

Principles matter to some people. as well as honesty. and betrayal.

But please, it really doesn&#x27;t make sense for you to come here and try to convert people. This isn&#x27;t a forum for debate. This is a space for people who feel a certain way. Please respect our difference of opinion. You&#x27;re welcome to post your counterpoints to upend&#x27;s twitter (link in the repo README).
---
> from: [**vladiibine**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394362758) on: **2018-6-4**

Oh yes, while some people might have biases against Microsoft, I&#x27;d be against any purchase of github by any technology company who could have a conflict of interests.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394392303) on: **2018-6-4**

Ok gentle people, this is a place for people who wanted an independent GitHub, and will leave if it isn&#x27;t. It&#x27;s not a forum for debate, or a place for you to convince them otherwise. You don&#x27;t go into some Node project&#x27;s repo, for example, and start making comments on issues arguing that they should switch to Ruby.

There are places for that. One the the Hacker News discussions, for example. On reddit. On Twitter. Not here.

Please be respectful of this.
---
> from: [**Omeryl**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/12#issuecomment-394508946) on: **2018-6-4**

What I don&#x27;t get here, @vassudanagunta, is that you claim you want an independent open community, where everyone can be heard yeah? Then why shut down all opposing, and very detailed, opinions? This is a trending repo, hearing both sides of the story is extremely beneficial to everyone. 
