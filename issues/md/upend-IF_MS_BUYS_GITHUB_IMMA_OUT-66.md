# [Alternate git implementation?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/66)

> state: **closed** opened by: **sam-irl** on: **2018-6-5**

Hey, all!

I&#x27;ve been doing a lot of thinking as to what the next steps should be, and how we can prevent any potential successor to GitHub from going the same way. So, stop me if you&#x27;ve heard this one before: _blockchain._

Commit data could be stored in blocks! I&#x27;ve mocked up a blockchain [here](https://github.com/sam-irl/gitchain.git), and if anyone wants to chip in, that&#x27;d be wonderful!

So the idea is that there is precedent to implement git in JavaScript. If repositories and commit data could be stored in blocks on the blockchain, then there would still be a centralized location for git repos, but no one company could buy it out (if they tried, they wouldn&#x27;t be able to get the 51%.)

I have a simple proof-of-work right now - if anyone has any other ideas for proof-of-stake methods that would be appreciated too.

I&#x27;d love to hear thoughts on this.

### Comments

---
> from: [**nkoehring**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/66#issuecomment-394836741) on: **2018-6-5**

The thumb-downs are pretty clear already but because you asked opinions, I might as well write some words:
An SCM on a blockchain is, just as many other blockchain ideas, not a good idea. Blockchains are not the solution to all our decentralisation problems. Blockchains are too slow, heavy, complicated and not flexible enough.
---
> from: [**remram44**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/66#issuecomment-394838160) on: **2018-6-5**

A blockchains stores everyone&#x27;s data in a single data structure. You want everyone to have a copy of every commit ever on their machine?

If you&#x27;re thinking &quot;we could have a different blockchain per project/fork&quot; then you are thinking about Git.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/66#issuecomment-394842869) on: **2018-6-5**

The problem we need to address is not how Git works but how Git content is hosted. This is off topic.
---
> from: [**sam-irl**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/66#issuecomment-394845092) on: **2018-6-5**

closed as i’m awful?
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/66#issuecomment-394845881) on: **2018-6-5**

Please handle your emotions and actually read what&#x27;s been said a few posts above.
