# [troll](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/68)

> state: **closed** opened by: **gramster** on: **2018-6-5**

i am a troll.

### Comments

---
> from: [**komali2**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/68#issuecomment-394814813) on: **2018-6-5**

Where&#x27;s the law that says github repos have to be cluttered with a bunch of people saying &quot;no?&quot;

This isn&#x27;t a political debate floor. Obviously the owner of the repo is not going to consider throwing his arms up and say &quot;you&#x27;ve convinced me, this thing I care about is useless!&quot;
---
> from: [**Omeryl**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/68#issuecomment-394816541) on: **2018-6-5**

@komali2 At the same time, closing every issue (even those with information that could be useful to some considering this path) is a little annoying. 
---
> from: [**Omeryl**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/68#issuecomment-394818721) on: **2018-6-5**

@vassudanagunta Wow .. that&#x27;s extremely childish. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/68#issuecomment-395581645) on: **2018-6-8**

On the question of censorship, please read #114.
