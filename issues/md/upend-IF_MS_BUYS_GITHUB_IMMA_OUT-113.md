# [sorry](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/113)

> state: **closed** opened by: **ahouse101** on: **2018-6-7**



### Comments

---
> from: [**yegortimoshenko**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/113#issuecomment-395563560) on: **2018-6-7**

@vassudanagunta I think this moderation style only makes people that are comfortable with GitHub acquisition more pissed off. Could you just close those as off-topic without replacing title and description? That would be reasonable and should reduce the tension.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/113#issuecomment-395575061) on: **2018-6-7**

I totally hear you @yegortimoshenko. Thank you for saying it with kindness. Here&#x27;s how I&#x27;ve decided to resolve this: #114.
