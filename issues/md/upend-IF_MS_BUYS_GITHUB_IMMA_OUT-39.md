# [Looks like Gitea is a good option?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39)

> state: **open** opened by: **jac1013** on: **2018-6-4**

https://gitea.io

What are your thoughts around this one guys?

We started using in some weeks ago and so far so good, there is a lot of things that need improvement and we were thinking (before the news) to start collaborating in this project.

Let&#x27;s see where it takes us.

### Comments

---
> from: [**unixfox**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-394505184) on: **2018-6-4**

Gitea is great because the interface is similar to Github and the API is almost exactly the same as Github too. Moreover Gitea is fast and light compared to Gitlab.
Gitea can be easily hosted on a free hosting plan without any issues, for example on Heroku.
---
> from: [**ozanonurtek**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-394533869) on: **2018-6-5**

Hello,
We create a tutorial and a script about Gitea and migration process :)
Checkout : [Your Own Git](https://github.com/librenotes/your_own_git)
---
> from: [**aolko**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-394595970) on: **2018-6-5**

Better than gitlab
---
> from: [**Tvde1**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-394607376) on: **2018-6-5**

@aolko why?
---
> from: [**aolko**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-394610892) on: **2018-6-5**

@Tvde1 [scroll up](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-394505184)
---
> from: [**Depado**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-394708739) on: **2018-6-5**

I also recommend gitea. It&#x27;s easy to setup, it&#x27;s fast, doesn&#x27;t require a lot of resources (can run on a raspberry), and is really easy to maintain.

I&#x27;ve been mirroring my repositories on my gitea instance for years. Works like a charm. 

Also, for TLS, I&#x27;d recommend using [Caddy](https://caddyserver.com/). Way simpler than an nginx with certbot or whatever. 
---
> from: [**taurauko**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-394920056) on: **2018-6-6**

Gitea is a fork of Gogs ( https://github.com/gogs/gogs ). Both are tiny and fast as expected from Go projects, but they&#x27;re of course not as feature-rich as these big proprietary platforms.
---
> from: [**Depado**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-394993796) on: **2018-6-6**

Depends what you&#x27;re looking for.

Gitea is actually just a Github like, with the exact same features. I think the only thing truly missing right now is the &quot;project management&quot; part (with the board and stuff). If you indeed want your integrated CI or something like that, then you should go with Gitlab. But just FYI, [Drone CI](https://drone.io/) is a neat self-hosted CI/CD platform. 

Also, yes, it&#x27;s a fork of Gogs. This fork happened for some reasons that are explained [here](https://blog.gitea.io/2016/12/welcome-to-gitea/). 
---
> from: [**dpedu**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-395009600) on: **2018-6-6**

Well, that&#x27;s certainly not the only thing missing. When compared to GitHub or Gitlab, the Gitea UI is a bit lacking. Specifically, I miss the &quot;ajax&quot; repo file search feature (on GitHub, press &quot;t&quot; when viewing a repo) and other similar interactions. I use Gitea myself and it&#x27;s very capable but let&#x27;s not gloss over this.
---
> from: [**Depado**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-395030407) on: **2018-6-6**

Some &quot;nice things&quot; are missing, but the core functionalities are almost all here, that&#x27;s all I say :+1: 
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-395191451) on: **2018-6-6**

Please consider completing the Gitea section in the [comparison spreadsheet](https://docs.google.com/spreadsheets/d/1LZryaOx4VYHQ_oWLtRjvFrr4RhVjR484eFe7du3Usa4) as per #25.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/39#issuecomment-395578322) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
