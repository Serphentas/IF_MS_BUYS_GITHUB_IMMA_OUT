# [seems MS has acquired github](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/8)

> state: **closed** opened by: **franklingu** on: **2018-6-4**

https://www.theverge.com/2018/6/3/17422752/microsoft-github-acquisition-rumors

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/8#issuecomment-394209227) on: **2018-6-4**

Yes. The number of people staring this repo has accelerated since that update.

If GitHub does in fact lose its independence, what I&#x27;d like to do is turn this repo into a place where the community can discuss options, including alternatives to GitHub.

I&#x27;ll also like to gather like-minded people to help me launch [upend.org](http://upend.org) with the goal of promoting an internet not dominated by corporate interests, but rather people&#x27;s interests.
---
> from: [**tay10r**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/8#issuecomment-394210927) on: **2018-6-4**

@vassudanagunta It&#x27;s acquired, not aquired.
---
> from: [**MaxShalom**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/8#issuecomment-394223695) on: **2018-6-4**

Seems official.
[https://www.bloomberg.com/news/articles/2018-06-03/microsoft-is-said-to-have-agreed-to-acquire-coding-site-github](https://www.bloomberg.com/news/articles/2018-06-03/microsoft-is-said-to-have-agreed-to-acquire-coding-site-github)
---
> from: [**franklingu**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/8#issuecomment-394226300) on: **2018-6-4**

I think, not sure though, the share price of MSFT will go up this Monday.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/8#issuecomment-394433903) on: **2018-6-4**

The README was updated this morning. 
