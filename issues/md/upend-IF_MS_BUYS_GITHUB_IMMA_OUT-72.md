# [GitCenter - A Decentralized Git Repository Platform](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72)

> state: **open** opened by: **ferittuncer** on: **2018-6-5**

I was thinking about a solution powered by [ZeroNet](https://zeronet.io/) then @krixano pointed out on #44  that there is already one: [GitCenter](https://github.com/imachug/gitcenter)

It is decentralized, every client is also a server, like BitTorrent. So there is no hosting cost and application is available as long as one peer is online. This also makes it censorship resistant because there is no centralized server to block.

In addition, this is a platform like GitHub, not like a self-hosted git server. So people can browse others&#x27; repositories.

I just tried it myself, seems like works well. It is developed by @imachug who is also [very active in ZeroNet development](https://github.com/HelloZeroNet/ZeroNet/graphs/contributors).

------

**Steps to start using it:**

1. Install [ZeroNet](https://zeronet.io/)
2. Go to http://127.0.0.1:43110/gitcenter.bit

-----
[ZeroNet Presentation](https://docs.google.com/presentation/d/1_2qK1IuOKJ51pgBvllZ9Yu7Au2l551t3XBgyTSvilew/pub?start&#x3D;false&amp;loop&#x3D;false&amp;delayms&#x3D;3000#slide&#x3D;id.g9a1cce9ee_0_9)

### Comments

---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-394856943) on: **2018-6-5**

I&#x27;ve also created a post about how to migrate from GitHub to GitCenter using git cli on ZeroTalk (Don&#x27;t have the link right now).
---
> from: [**ozanonurtek**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-394869093) on: **2018-6-5**

It sounds good actually but the horrible part is, if no peer &#x3D; no git. :rofl:  
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-394872608) on: **2018-6-5**

If no peer, no Bitcoin also, how about that? :D Yet it works like a charm.

Another example: Even if GitHub is centralized if they wouldn&#x27;t have any user they would shut it down, why would they keep it up?

No peer scenario is not a serious concern. As long as it is valuable to the community, there will be peers.
---
> from: [**cooldesigns**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-394900981) on: **2018-6-6**

We can&#x27;t browse the zeronet on the www. It has no index repos on google. It is hosts own code on github. comic

It have to make a tunnel to www. 

Also I saw the &#x27;porn&#x27; section on the zeronet index. I do not consider this a serious project

We must build a project which is central and have google index, and we must connect peer-to-peer network like spotify engineering
---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-394905351) on: **2018-6-6**

@ozanonurtek 
&gt; It sounds good actually but the horrible part is, if no peer &#x3D; no git. 🤣

This isn&#x27;t exactly true, but I really don&#x27;t want to get into that right now. Maybe I will later.

@cooldesigns 
&gt; We can&#x27;t browse the zeronet on the www. It has no index repos on google. It is hosts own code on github. comic

You&#x27;re wanting a proxy. There are some ZeroNet proxies - search for them.

Of course it hosts the code on GitHub, because that was the number one place to get that (and still is so far). It was literally only *just* announced that Microsoft bought GitHub - give @HelloZeroNet some time.
Btw, it *is* on GitLab **and** on GitCenter also.

&gt; Also I saw the &#x27;porn&#x27; section on the zeronet index. I do not consider this a serious project

By ZeroNet index, I&#x27;m guessing you mean ZeroSites? Btw, all the sites on there are user-added, not official, so of course there is going to be porn, duh! Furthermore, ZeroNet is actively developed by many people, whether that be through improving the client or protocol, etc., or through making zites. Currently, we have:

* GitCenter - GitHub alternative
* ZeroMedium - Medium alternative
* ZeroMe - Twitter alternative
* ZeroTalk and ZeroVoat - BBS and Voat alternatives
* KopyKate - Video Sharing platform
* Important Zites, ZeroSites - Users add zites to these. Important Zites can also search other zites like ZeroMe, ZeroTalk, etc.
* Zirch, EulerFinder, Kaffiene - Search Engines. A client goes through some zites looking for links to add to their index.
* ZeroLSTN - Google Play Music alternative
* Mixtape - Music Playlist sharing.
* Thunderwave - Chat zite. Not instant, but at most 30 second delay.
* ZeroMail - Email in ZeroNet
* ZeroUp - File Streaming and Sharing
* Kiwipedia - Wikipedia alternative
* ZeroExchange - StackExchange alternative
* and many more!

Stuff being worked on:
* PeerMessage plugin to allow *instant* communication and broadcasting of messages throughout the network.
* IRC and Discord alternatives that utilize the PeerMessage plugin for instant chatting.
* KxoNetwork [no details released yet, still in development (by me)]

Furthermore, ZeroNet has already been blocked a couple of times by China, etc. And @nofish has fixed this a couple of times. This shows that China is taking ZeroNet at least somewhat seriously, especially since ZeroNet is one of the places a lot of Chinese people go.

So, *yes*, it most certainly *is* a serious project that is being actively developed by many people.
---
> from: [**shubidubapp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-395056350) on: **2018-6-6**

So the idea is that connected git services serves other connected git services to users right?

 For example if I have 1 project in the git service I have created for my team although my intention was to make 1 project available to everyone, now I have to serve other services&#x27;  projects too. Those projects can become really large files too. 

The worst case is yes my first intention of making our teams&#x27; projects available to everyone is succeeded but my servers bandwith is fully used all the time which means I will either need to limit the bandwith/computation used on the service or I will need a new one. 

I don&#x27;t really think under these circumstances an decentralized git project like this could become popular enough to be worth it.
---
> from: [**imachug**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-395087300) on: **2018-6-6**

Git Center owner here.

You don&#x27;t host what you don&#x27;t want to. E.g. if you only want to use your own repos, you&#x27;ll only host them. Feel free to ask me and @krixano (he is an active ZeroNet developer) 
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-395152842) on: **2018-6-6**

I&#x27;m glad you joined the discussion @imachug 

As far as I know, GitCenter suits best our needs among the alternatives. But you know much better than me as you are the owner of GitCenter and a top contributor of ZeroNet. Therefore I would be glad if you share your thoughts about GitCenter as a decentralized alternative to GitHub. What are the advantages and disadvantages? 


---
> from: [**cooldesigns**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-395154024) on: **2018-6-6**

@krixano Yes I mean ZeroSites. But zerosites showing in left sidebar. It should be removed. 🗡 
---
> from: [**imachug**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-395293764) on: **2018-6-7**

@ferittuncer Git Center, in fact, is a bit different from GitHub. The fact that it&#x27;s decentalized makes it free. E.g. we have private repositories - you just don&#x27;t publish the repo to index, but it can still be downloaded by address (address looks like BitCoin public address, but ZeroNet doesn&#x27;t use blockchain in any way). This address is very big and can&#x27;t be bruteforced, so it&#x27;s secure. This is 1 advantage.

However, sometimes there is a problem. ZeroNet has some problems sharing sites with no peers - so it may be a bit difficult to share a repo you just created with someone else.

However, this problem is solvable - so I think Git Center can be used.
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-395339366) on: **2018-6-7**

@imachug @krixano One lmportant issue is I think identity management. What is the best practice to manage private keys/identities? I would love to hear that there is support for hardware keys such as Yubikey and Ledger. 
---
> from: [**imachug**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-395467939) on: **2018-6-7**

Ok guys - though I don&#x27;t have much time, I&#x27;ll try to add more features as soon as possible, because currently Git Center sounds like a very good choice. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/72#issuecomment-395580507) on: **2018-6-8**

Hey all. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
