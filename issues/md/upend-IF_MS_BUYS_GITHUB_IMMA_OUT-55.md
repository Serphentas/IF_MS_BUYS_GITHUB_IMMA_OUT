# [Standardized data interfaces for issues, pull requests, source database, etc](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/55)

> state: **open** opened by: **xloem** on: **2018-6-5**

Github provides a number of really valuable services such as pull requests.

I few projects have copied some of this functionality, but each project does it differently.

I feel norma should be chosen to ease interoperability as people choose different successors to github.  For example, if we had a standard data representation of issues and pull requests as files in a git branch, we could write software to import and export this important data for existing solutions, or clients that were agnostic.


### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/55#issuecomment-395576782) on: **2018-6-7**

Hey @xloem. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
