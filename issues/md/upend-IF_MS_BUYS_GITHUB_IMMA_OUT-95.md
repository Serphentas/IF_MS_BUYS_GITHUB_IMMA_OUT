# [Replacing Github-specific code in software repositories and build trees](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/95)

> state: **open** opened by: **DavidGriffith** on: **2018-6-6**

According to https://twitter.com/lattera/status/1003729667256397824, FreeBSD has problems with pulling code from any site other than Github,  There are probably other projects with this kind of deficiency.

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/95#issuecomment-395577726) on: **2018-6-7**

Hey @DavidGriffith. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
