# [Did everyone forget about Bitbucket or...?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40)

> state: **open** opened by: **derekbtw** on: **2018-6-5**

Just throwing this out there. This is Bitbucket&#x27;s landing page today...

&lt;a href&#x3D;&quot;https://bitbucket.org/&quot; target&#x3D;&quot;_blank&quot;&gt;&lt;img src&#x3D;&quot;https://user-images.githubusercontent.com/3450398/40944348-b1ed35cc-6811-11e8-8787-9d0eddf7d3fe.PNG&quot;&gt;&lt;/a&gt;

### Comments

---
> from: [**daveloyall**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394523505) on: **2018-6-5**

Aren&#x27;t they dramatically out of touch with the free software community?
---
> from: [**sinri**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394562612) on: **2018-6-5**

Compared with GitHub, Bitbucket is more possibly to be &#x60;firewall&#x60;ed in China.
Not a good news anyway. 
---
> from: [**austinhuang0131**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394570596) on: **2018-6-5**

GitHub is blocked in China (and is apparently [reported so](https://github.com/programthink/zhao/issues/91)) because there are people who literally use the platform to talk politics. Also, there were tons of proxy software on GitHub which were used to bypass GFW.

See also https://www.techdirt.com/articles/20160623/07384934793/rather-than-launch-massive-ddos-attack-this-time-china-just-asks-github-to-take-down-page-it-doesnt-like.shtml

BitBucket is not.
---
> from: [**holzit**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394591834) on: **2018-6-5**

It comes down to how much you like Atlassians stack in my opinion. If you like it go ahead, otherwise look into GitLab. 
---
> from: [**covrom**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394628080) on: **2018-6-5**

gogs.io is good alternative
---
> from: [**muppeth**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394635262) on: **2018-6-5**

Atlassians just like github is not open source, so basically you throw your eggs from one smelly pot to another. Not really solving the situation imo.
---
> from: [**avasconcelos114**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394640904) on: **2018-6-5**

Having been using Bitbucket for side projects every once in a while, I find that they ignore a fair bit of popular requests from the community.

Case in point being [this thread asking for custom PR templates](https://bitbucket.org/site/master/issues/11571/custom-pull-request-description-template) to function like they do on Github
---
> from: [**zafan**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394649797) on: **2018-6-5**

Also, it&#x27;s possible to host your own Bitbucket server but in that case, your problem might be a high price for more users.
I usually use Gitea.io, for a huge mass of people I consider gitlab as a good alternative.
---
> from: [**revelt**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394650732) on: **2018-6-5**

&gt; I consider gitlab as a good alternative.

BitBucket is cheaper and just as good — if you&#x27;ll ever want to switch to commercial development, it will be cheaper than GitLab
---
> from: [**lanodan**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394669188) on: **2018-6-5**

→ Repeating the same thing and expecting different results

Bitbucket is non-libre and centralized, also owned by a corporation.

---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-394778689) on: **2018-6-5**

Anyway, I would prefer Atlassian over Microsoft. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/40#issuecomment-395576954) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
