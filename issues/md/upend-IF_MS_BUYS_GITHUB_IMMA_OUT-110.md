# [Maybe havent being censored?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/110)

> state: **closed** opened by: **milewski** on: **2018-6-7**

about the:

&gt; BREAKING NEWS: Microsoft GitHub just censored this project from its Trending page

i am subscribed to github newsletter and i still got it on my emails for like 2 days in a row:

![image](https://user-images.githubusercontent.com/2874967/41092995-cb19bc74-6a7c-11e8-8744-dc92beee6b00.png)


### Comments

---
> from: [**moonwater**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/110#issuecomment-395374278) on: **2018-6-7**

no doubt.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/110#issuecomment-395450123) on: **2018-6-7**

that&#x27;s clearly from before we were censored. We had over 3,000 stars the last time I saw us on the list.  We have over 4,100 stars now, though it would be over 5,000 if not over 6,000 if they hadn&#x27;t buried us like this.
