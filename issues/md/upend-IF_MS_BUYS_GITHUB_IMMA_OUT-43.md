# [Peers community and notabug](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/43)

> state: **open** opened by: **rdococ** on: **2018-6-5**

Just found this:
https://peers.community/

They have https://notabug.org/ - which appears to be based on the MIT Gogs collaboration platform and appears to be fully dedicated to open-source software.

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/43#issuecomment-395576862) on: **2018-6-7**

Hey @rdococ. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
