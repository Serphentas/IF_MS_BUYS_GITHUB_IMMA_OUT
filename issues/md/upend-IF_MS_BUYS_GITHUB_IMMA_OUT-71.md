# [Fixed Typo in the readme.md file](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71)

> state: **closed** opened by: **NeumimTo** on: **2018-6-5**

Fixed Typo in the readme.md file

### Comments

---
> from: [**changyuheng**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394840965) on: **2018-6-5**

Why is it a typo?
---
> from: [**NeumimTo**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394841167) on: **2018-6-5**

It is.
---
> from: [**sci4me**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394841374) on: **2018-6-5**

No, it&#x27;s supposed to be &quot;your&quot;; learn grammar. Jesus.
---
> from: [**r00ster91**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394847130) on: **2018-6-5**

Thats not a typo.
---
> from: [**NeumimTo**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394847301) on: **2018-6-5**

It is
---
> from: [**r00ster91**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394848062) on: **2018-6-5**

*&quot;In the coming days we will collaboratively put together resources and guides on where best to move you and **you are** projects and how.&quot;* ... ?
---
> from: [**AkgunFatih**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394850602) on: **2018-6-5**

wtf bro are you high or something ?
---
> from: [**sci4me**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394851062) on: **2018-6-5**

He&#x27;s probably another troll.
---
> from: [**amwmedia**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394852083) on: **2018-6-5**

Can we please change the wording from &quot;you&quot; and &quot;your&quot; to &quot;us&quot; and &quot;our&quot; to be more inclusive to the wider community?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394854608) on: **2018-6-5**

@amwmedia yes! can you do a pull request?
---
> from: [**amwmedia**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/71#issuecomment-394857101) on: **2018-6-5**

my comment was total sarcasm (in case anyone wasn&#x27;t sure), but it actually WOULD read nicely with that verbiage change 😂
