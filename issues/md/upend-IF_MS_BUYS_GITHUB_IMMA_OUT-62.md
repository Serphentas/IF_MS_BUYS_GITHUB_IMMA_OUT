# [Possible alternative: GitLab [please close this, duplicate of #9]](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/62)

> state: **closed** opened by: **Ettapp** on: **2018-6-5**

# GitLab
I don&#x27;t know anything about the team / corp behind it, but I&#x27;m realy happy with [Gitlab](https://gitlab.com/) ^^

Spoil: [you can self-host it freely very easily](https://about.gitlab.com/team/) ;-)

EDIT: [some data](https://about.gitlab.com/team/) [about them](https://about.gitlab.com/about/)

### Comments

---
> from: [**licaon-kter**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/62#issuecomment-394796073) on: **2018-6-5**

#9

/Close this
---
> from: [**Ettapp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/62#issuecomment-394796572) on: **2018-6-5**

Oops, you&#x27;re right
---
> from: [**remram44**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/62#issuecomment-394837570) on: **2018-6-5**

@Ettapp you can close this yourself, click on the button.
---
> from: [**Ettapp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/62#issuecomment-394884314) on: **2018-6-6**

God I&#x27;m so hopeless, thank you both x)
