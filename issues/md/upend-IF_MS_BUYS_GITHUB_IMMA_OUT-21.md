# [New list of app/services destroyed by Microsoft acquisition](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/21)

> state: **closed** opened by: **savo92** on: **2018-6-4**



### Comments

---
> from: [**darkpixel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/21#issuecomment-394394551) on: **2018-6-4**

I miss Sunrise calendar.  It was such a handy tool...
---
> from: [**savo92**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/21#issuecomment-394396682) on: **2018-6-4**

I really miss it too. It was the only calendar app in the history that didn&#x27;t make me crazy
---
> from: [**SFoskitt**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/21#issuecomment-394400925) on: **2018-6-4**

Skype
Wunderlist
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/21#issuecomment-394405763) on: **2018-6-4**

I hear you. I have strong feelings about Microsoft too, having lived through the 90s. But this isn&#x27;t about Microsoft. 

[As I tweeted](https://twitter.com/UpEnd_org/status/1003475525619404801), and probably should put in the README:
&gt; It&#x27;s not about Microsoft. It&#x27;s about the independence of the de-facto home of the #opensource community. It shouldn&#x27;t be owned by any company with ANY agenda OTHER THAN servicing this community.

Or if you look at the photo on the README. The problem in *this* picture isn&#x27;t Microsoft, it&#x27;s the guy, GitHub.  [As one person on Twitter put it](https://twitter.com/CWDillon/status/1003614146989043717):

&gt; Yah, it&#x27;s like GitHub just asked for a 3some with Microsoft and expects it not to affect our relationship. 
It&#x27;s one or the other and you&#x27;ve already declared your preference.

So I&#x27;m not going to merge this. But keep the info handy. As we spin up UpEnd.org and talk about what&#x27;s wrong with so few controlling so much of the internet, it may go there. 
---
> from: [**darkpixel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/21#issuecomment-394407888) on: **2018-6-4**

I expect companies to have an agenda.  You aren&#x27;t a business if you don&#x27;t bring in a profit and keep the shareholders/investors happy.  That should be the primary agenda.  It&#x27;s tightly coupled with &quot;provide a valuable service&quot; and &quot;convince people to pay for the service&quot;.

You haven&#x27;t clearly defined what Microsoft&#x27;s agenda is or how it conflicts with some nebulous &#x27;open source&#x27; agenda.

I&#x27;m sure I speak for a lot of people.  It *is* simply about Microsoft.

