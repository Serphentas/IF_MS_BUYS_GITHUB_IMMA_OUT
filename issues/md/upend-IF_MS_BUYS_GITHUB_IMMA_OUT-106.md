# [Code](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/106)

> state: **open** opened by: **dotmilk** on: **2018-6-7**

I saw like most apologists, lots of &#x27;no code here&#x27; arguments about being taken down as trending, are there some code snippets etc in other issues, like some bash stuff etc for migrating to a different place or fetching your issues, that we could compile into some .sh etc code to place in the repository.

TL/DR: Can we compile / combine github migration related code and place it into this repo?



### Comments

---
> from: [**bakkerme**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/106#issuecomment-395307519) on: **2018-6-7**

There&#x27;s no requirement for code being in a repo, I mean look at danistefanovic/build-your-own-x, currently in the trending list. We were taken down purely because it&#x27;s inconvenient.
---
> from: [**dotmilk**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/106#issuecomment-395326840) on: **2018-6-7**

I agree it was taken down as it was inconvenient to their image. And I&#x27;m aware of the fact that code is not needed, it&#x27;s just another resource (which is what this repo is about):

&#x27;A space to learn or share escape routes — In the coming days we will collaboratively put together resources and guides on where best to move you and your projects and how.&#x27;

Has an added benefit of shutting down certain talking points.

I am also wondering if code related to migration is added, will it possibly trigger a larger (worse in the long run for their image) response from this soulless entity?
---
> from: [**bakkerme**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/106#issuecomment-395353066) on: **2018-6-7**

&gt; I am also wondering if code related to migration is added, will it possibly trigger a larger (worse in the long run for their image) response from this soulless entity?

There&#x27;s too much risk in damaging what little good will is left. Microsoft won&#x27;t officially be in charge til later this year, and by more blatantly censoring inconvenient information, they risk an even greater backlash.
There&#x27;s a great PR up with a solid guide and code to assist users to migrate to GitLab that should make for a good starting point and a template to work off for over platforms. Over the weekend I&#x27;m going to look at compiling some general resources about moving to other git hosting providers. This repo has too much attention to be wasted on bickering in the issues.

---
> from: [**dotmilk**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/106#issuecomment-395385303) on: **2018-6-7**

What bickering?
---
> from: [**bakkerme**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/106#issuecomment-395393728) on: **2018-6-7**

Well, what I will say is that there&#x27;s some less than productive conversation in a few of these issues.

I think the best way forward here is to focus on getting resources in the hands of the community. The high tensions of the acquisition will cool down in time and vassudanagunta can finally get some sleep :stuck_out_tongue_winking_eye:.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/106#issuecomment-395579644) on: **2018-6-8**

Hey guys. Thanks so much for your work. I&#x27;ve decide on a way forward. See Issue 114.
