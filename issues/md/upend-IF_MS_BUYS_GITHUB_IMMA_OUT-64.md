# [Keybase and encrypted git](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/64)

> state: **open** opened by: **mehlert** on: **2018-6-5**

https://keybase.io/blog/encrypted-git-for-everyone

### Comments

---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/64#issuecomment-394853664) on: **2018-6-5**

But this is not a platform where people can browse others&#x27; repositories. Or I&#x27;m not aware?
---
> from: [**Riamse**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/64#issuecomment-394892644) on: **2018-6-6**

So we centralised our code and communication to a single company who sold out, and the alternative proposal is to centralise our code and communication to a different single company who may or may not sell out depending on the circumstances? Just because it&#x27;s encrypted doesn&#x27;t mean they won&#x27;t still have power over us - the WhatsApp controversy comes to mind. The real solution to power is decentralisation, federation, free software, and yes, encryption too.
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/64#issuecomment-394933046) on: **2018-6-6**

For a decentralized solution please check #72 @Riamse 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/64#issuecomment-395577206) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
