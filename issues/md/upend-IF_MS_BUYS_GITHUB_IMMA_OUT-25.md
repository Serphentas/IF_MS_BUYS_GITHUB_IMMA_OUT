# [project - build a new Hub](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25)

> state: **open** opened by: **oltdaniel** on: **2018-6-4**

The new hub is an independent project developed by an community, for a community. It will be a decentralized network of self hosted servers that store the data across the network. Each server will grant access to the global API for third party services like deployment platforms.

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394416861) on: **2018-6-4**

I believe there are a number of existing projects that do that. Can you research and catalog them?
---
> from: [**oltdaniel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394417279) on: **2018-6-4**

If there would be a really good alternative we would probably know. But I
can search for some.

&gt; I believe there are a number of existing projects that do that. Can you
&gt; research and catalog them?
&gt;
&gt; —
&gt; You are receiving this because you authored the thread.
&gt; Reply to this email directly, view it on GitHub
&gt; &lt;https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394416861&gt;,
&gt; or mute the thread
&gt; &lt;https://github.com/notifications/unsubscribe-auth/AiJ_c_B_ncTskak8_UdUJDhY4m6reQBWks5t5WAwgaJpZM4UZW1O&gt;
&gt; .
&gt;

---
> from: [**marcus-sa**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394473420) on: **2018-6-4**

I would be willing to contribute as long as it&#x27;ll be a decentralized platform/network which is possible.

I&#x27;d suggest the next would be come up with a list of technology we should use for the new git &quot;platform&quot;?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394479774) on: **2018-6-4**

@marcus-sa some people want a centralized platform, and some people are like you. Rather than get into a heated religious war, how about you write or help write up the distributed option, both the pros and cons, the real world options that exist or that we&#x27;d have to build, being honest rather than trying to beat out the centralized/GitLab folks. I&#x27;l ask the same of them. 

In the end maybe we publish both, or maybe we pick one as the &quot;more recommended&quot;. 
---
> from: [**cooldesigns**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394487902) on: **2018-6-4**

We can find a method for share hosting costs: Example ideas:

- Calculating user&#x27;s hosting cost and invoicing unless the cost will be bigger than 1$
- Decentralize hosting. Using serverless. Like spotify.
- Donation on open collective
- The super idea: Make a feature: Allow users to donate to open source software and get commission for hosting costs.
Software is basic for us. We can develop gags, and we can make it&#x27;s features more social. It&#x27;s already ready and stable.
---
> from: [**duncup**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394558471) on: **2018-6-5**

Maybe we can use blockchain to integrate with the new distributed hub, similar to getting a certain token for the server, and the user pays by paying the token. Just an idea.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394566276) on: **2018-6-5**

@oltdaniel I&#x27;m not an expert in this area... Ideally a team of such experts or willing-to-become-experts forms to figure this out. If we can come up with a list of priority tasks for the group, I can post them on the README to attract help.  I&#x27;ve sleep 10-13 hours since Friday morning (none last night). 
---
> from: [**marcus-sa**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394609886) on: **2018-6-5**

As @duncup mentioned, using a blockchain would be a great idea.
What I had in mind would be using https://ipfs.io with https://filecoin.io and something along the lines of https://www.bigchaindb.com

But anyhow this discussion fits better in https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44
---
> from: [**Salakar**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394615199) on: **2018-6-5**

Whilst I like the idea of decentralisation it unfortunately brings a ton of added complexity. 

I don&#x27;t see centralised being a problem if it&#x27;s foundation based for example; the Mozilla Foundation or the Wikimedia Foundation. Easier for everyone to get on board that way.

That&#x27;s my 2 cents anyway 😅
---
> from: [**oltdaniel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394658278) on: **2018-6-5**

@Salakar Join the Slack Group to help starting a discussion
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394736644) on: **2018-6-5**

https://zeronet.io/ can help. Helps to decentralize web apps. FOSS.

Also, I&#x27;m really excited about a decentralized GitHub. But not an expert on web applications. Would like to help though.
---
> from: [**marcus-sa**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394739163) on: **2018-6-5**

@ferittuncer nice suggestion, but unfortunately ZeroNet wouldn&#x27;t be the greatest approach for this, since it requires an application and an underlying network through Tor.

That **.bit** TLD looks cool tho
---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394799074) on: **2018-6-5**

While it does require an application, it does *not* require Tor. Furthermore, everything needed is bundled up into a zip (including python) or an exe for Windows, or you can use an installer I created for ZeroNet.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394814050) on: **2018-6-5**

For those who don&#x27;t have Slack, see [this issue](https://github.com/bderiso/Microsoft-Github-acquisition/issues/7) to discuss the various existing alternatives.
---
> from: [**oltdaniel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394842057) on: **2018-6-5**

Let&#x27;s see what will happen. GitHub is and will be unbeaten for years.
---
> from: [**RyanGannon**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394868786) on: **2018-6-5**

https://gitgud.io/users/sign_in is a hosted GitLab instance.
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394874143) on: **2018-6-5**

Please see #72  @marcus-sa 

We found a decentralized platform for hosting Git repositories, working already.
---
> from: [**marcus-sa**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394954592) on: **2018-6-6**

@oltdaniel what happened to the Slack server and Docs document?
---
> from: [**oltdaniel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394956289) on: **2018-6-6**

@marcus-sa Let us see what will happen to GitHub
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-394968051) on: **2018-6-6**

@marcus-sa It&#x27;s still up and running. Is it down for you ?
---
> from: [**marcus-sa**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-395004519) on: **2018-6-6**

@Serphentas it&#x27;s nowhere to be found and for me it says the workspace has been deleted.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-395043248) on: **2018-6-6**

The [GitHub alternatives](https://docs.google.com/spreadsheets/d/1LZryaOx4VYHQ_oWLtRjvFrr4RhVjR484eFe7du3Usa4) is up, but indeed the Slack _shortlink_ is dead. Perhaps someone can provide a direct link to the board.
---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-395087543) on: **2018-6-6**

@Serphentas Don&#x27;t know who manages the spreadsheet, but I can give the information for GitCenter. Hope this helps!

Repo hosting - Yes
Public repositories - Yes? This means repositories that can be seen in public right? Because Bitbucket also has this feature, I think, but it&#x27;s marked as not having it.
PR / Code reviewing - Yes
Issues / Project management - Yes
Collaboration / Teams - Must use forks and PR&#x27;s for this (due to certain limitations that will be worked on in the future)
Community / Social network - Not yet. No social features aside from issues and comments (and a way to see issues in a newsfeed thing listed on what is basically ZeroNet&#x27;s homepage within the network).
Documentation - Not yet. No wiki, etc.
Open source - Yes. Source code hosted on GitHub, GitLab, and GitCenter itself. Also, all zite&#x27;s (websites on ZeroNet) are pretty much open source because you are downloading all of the code of the zite.
Decentralized ownership - Yes
Extensibility API - Kinda. ZeroNet sites that act as alternative interfaces can utilize the same repositories, and other ZeroNet sites can easily look at and interact with the information of the repositories (with client&#x27;s permission).
Federation - No
Mattermost/Matrix/Riot integration - No
Available online instance - ??
Self-hosting (own premises) - Yes, in the sense of you are one peer out of potentially many.
CI/CD - Not yet.
Project website hosting - Not yet.
Free of charge - Yes
Development done by the community - Yes
Scalable - Yes
EDIT: High availability - In the sense of you must download ZeroNet to see repos (*unless* you use a ZeroNet proxy), then not really. In the sense of percent of time it&#x27;s accessible within ZeroNet, then pretty high (as long as there&#x27;s one peer).

Regarding a few misconceptions:
* You do not need peers all of the time in order to use a repo, because they are downloaded. You only need at least one peer at the same time you are also on in order to get updates for the repo, but you can still use the repo if you are offline, it just *may* not be the latest until you go back online.
* You are not required to host/seed all repos/ZeroNet sites (unlike one other decentralized internet project, I think Freenet?). You only host/seed what you want to host/seed.
* Having more peers reduces the amount that you are uploading to other people (outgoing bandwidth) because other peers can also upload pieces to people.
* There *are* ZeroNet proxies that allow you to view ZeroNet websites (zites) without having to download a client, just not many of them currently.
* Tor is not required to use the network.
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-395115283) on: **2018-6-6**

Thanks for listing those @krixano 

I think high availability stands for percent of time the service is available. So as it is peer-to-peer I would say it is highly available because it won&#x27;t be down ever as long as one peer exists.

---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-395137639) on: **2018-6-6**

@krixano I manage this spreadsheet. If you want write access, feel free to ask. I&#x27;ll try to put some of your points over there (don&#x27;t have much free time these days).

@ferittuncer Having &gt;1 peer online at any time surely guarantees that the whole service stays up, but does every one host have the ability to host _all_ the content (i.e. repositories) ?
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-395139551) on: **2018-6-6**

@marcus-sa The [board](https://pakethub.slack.com/?redir&#x3D;%2Fmessages%2FCB18GEDJQ) has been deleted.
---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-395139854) on: **2018-6-6**

@Serphentas Ok. I did the Edit Access Request with my work email (forgot I was signed into that). It is christian.seibold.ws9@gmail.com

About your question for @ferittuncer, you can seed/host all of the repositories if you have enough disk space, you want to, and the repository owners have made the link to it public. Furthermore, you do not have to host *all* ZeroNet websites (including repos, since they are basically their own zites), only the ones you want to.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-395140719) on: **2018-6-6**

@krixano Request accepted.

EDIT: forgot to answer your second comment.
So naturally comes the issue of making sure there is &gt;1 copy of any repository at any time. How can this be handled ?
---
> from: [**oltdaniel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-395144759) on: **2018-6-6**

I created a new Slack Team. If anyone is interested in planning nor building a new decentralized hub, join here.

Slack: [Join](https://join.slack.com/t/paket-hub/shared_invite/enQtMzc1OTY5NzAyNTc2LWEzNTkyMTE3NjQzOWJlNzZjZGJhNmE5NTVlOWRhMGJmYzhkYWU0ZGNmOTg3M2Y1MGFkNDNiMGE5OGRlMmVmMWI)

Github Space: [paket-hub](https://github.com/paket-hub)
---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25#issuecomment-395146059) on: **2018-6-6**

@Serphentas If you want a server to seed all repos (or even just certain specific repos, etc.), all you really need is a computer with internet access and the ZeroNet client downloaded on it. You may also need to open a port (which is done, if possible, automatically by the client using UPnP - or you can manually do this of course).

However, you&#x27;d probably need to setup a simple script that will auto-download new repositories when they are published to the Git Center index. But this shouldn&#x27;t be too hard.

Based on what we are currently seeing, if people consider your repo useful, they will seed it. Currently, many people are seeding the ZeroNet and Git Center repos. There are also many people seeding my repos as well. Also, if you are active on ZeroNet and have the client running at least most of the time (like I do), then you&#x27;d be at least one peer, and that&#x27;d be enough for other people to download the repo if they so desire.
