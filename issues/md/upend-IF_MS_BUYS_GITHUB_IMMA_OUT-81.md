# [Move to Mercurial](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/81)

> state: **closed** opened by: **fn-main** on: **2018-6-6**


I recently migrated from GitHub to Mercurial, and I&#x27;m liking what I&#x27;m seeing. Sourcetree support, a basic TUI interface, self-hosted but very minimal on resources... If anyone else agrees let me know, but this is the path I have taken.

### Comments

---
> from: [**cooldesigns**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/81#issuecomment-394904235) on: **2018-6-6**

It is awesome.

We can build a portal for mercurial projects which replaces github pages, or readme section. And supply index on world wide web. 
---
> from: [**TRManderson**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/81#issuecomment-394906185) on: **2018-6-6**

Mercurial is slower than git and has a very different branching model
---
> from: [**lil5**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/81#issuecomment-394937959) on: **2018-6-6**

Git is a revision control system, a tool to manage your source code history.

GitHub is a hosting service for Git repositories.

So they are not the same thing: Git is the tool, GitHub is the service for projects that use Git.

https://stackoverflow.com/a/13321586 
---
> from: [**lieff**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/81#issuecomment-394992974) on: **2018-6-6**

@fn-main I&#x27;ve look at mercurial too, but I do not find &#x60;--depth&#x3D;1&#x60; git alternative. Do you know how to get only latest versions of files in mercurial?
---
> from: [**stevenvachon**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/81#issuecomment-395049036) on: **2018-6-6**

Sourcetree supports Git.
---
> from: [**lil5**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/81#issuecomment-395194072) on: **2018-6-6**

@stevenvachon So does GitLab, BitBucket, Sourceforge, and all the other Git hosters out there.

Here&#x27;s a list for choice:
https://git.wiki.kernel.org/index.php/GitHosting
---
> from: [**stevenvachon**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/81#issuecomment-395201394) on: **2018-6-6**

I was referring to the original comment where Sourcetree was listed as a reason to move to Mercurial.
---
> from: [**fn-main**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/81#issuecomment-395276323) on: **2018-6-7**

Sourcetree has support for mercurial too
