# [Create GITLAB_MIGRATION_GUIDE.MD](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/86)

> state: **open** opened by: **XakepSDK** on: **2018-6-6**

Migration guide to gitlab

### Comments

---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/86#issuecomment-395141915) on: **2018-6-6**

LGTM
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/86#issuecomment-395595741) on: **2018-6-8**

This is great work. But it needs to be published somewhere else (probably on GitLab given what you&#x27;re implicitly recommending).

The reason I can&#x27;t post it here is because:
1. See Issue 114
2. It&#x27;s recommending GitLab, and not everyone agrees that&#x27;s what should be recommended. 

What I can do is link to it, with the text, &quot;If you choose to migrate to GitLab, this guide will make it easier&quot;. 

But question, what does this provide on top of what I&#x27;m sure GitLab itself is already providing evacuees?
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/86#issuecomment-395790422) on: **2018-6-8**

Given that we discuss the alternatives [here](https://github.com/bderiso/Microsoft-Github-acquisition/issues), it would probably be a good idea to compile various guides over there for those who wish to continue with GitLab until the community decides what to use next.

What do you think ?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/86#issuecomment-395911457) on: **2018-6-9**

reminder this forum is shutting down in 5 hours 20 minutes. 
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/86#issuecomment-395917048) on: **2018-6-9**

I&#x27;ve migrated the current guide [here](https://github.com/bderiso/Microsoft-Github-acquisition/blob/master/migration_guide_gitlab.md).
