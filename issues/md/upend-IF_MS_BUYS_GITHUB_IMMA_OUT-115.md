# [Savannah/Savane](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/115)

> state: **open** opened by: **zafan** on: **2018-6-8**

There might be another option to migrate to.
**GNU Savannah/Savane** it supports:

* GIT
* Mercurial
* Bazaar
* Subversion
* CVS

It&#x27;s FOSS and actually can be modified (e.g. facelift)

&#x60;&#x60;&#x60;&quot;Savane is a Web-based Libre Software hosting system. It includes issue tracking (bugs, tasks, support, news and documentation), project member management by roles and individual account maintenance. &quot;&#x60;&#x60;&#x60; 
-- from [gnu savannah](https://savannah.nongnu.org/p/savane)

I didn&#x27;t know where to add it - the spreadsheet is not writable. 

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/115#issuecomment-395636130) on: **2018-6-8**

@Serphentas, can you help @zafan with getting it on the spreadsheet? Do you own it? 
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/115#issuecomment-395789399) on: **2018-6-8**

@vassudanagunta Sure thing.

@zafan Please type your gmail address in a comment on the spreadsheet. I will grant you write access afterwards.
