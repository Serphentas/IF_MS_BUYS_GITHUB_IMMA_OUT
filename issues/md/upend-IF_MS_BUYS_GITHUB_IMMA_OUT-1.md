# [GitHub is already a centralised non-free thing](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1)

> state: **open** opened by: **luke-jr** on: **2018-6-3**

The reasons to not use GitHub already exist. Therefore, it&#x27;s better for people to stop using it. Therefore, it&#x27;s better for Microsoft to acquire them...

### Comments

---
> from: [**luke-jr**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394121015) on: **2018-6-3**

Also, the more people who leave, the better the eventual replacement will be :)
---
> from: [**sugaaloop**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394124115) on: **2018-6-3**

Care to expand for the uninformed?
---
> from: [**luke-jr**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394124525) on: **2018-6-3**

Microsoft buys GitHub -&gt; leads to mass abandonment of GitHub -&gt; some % of those people write better open source replacement -&gt; open source GitHub replacement -&gt; net gain for everyone.
---
> from: [**awilfox**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394126231) on: **2018-6-3**

May I recommend that anyone interested in making a free, decentralised GitHub look in to contributing to projects like:

* [Pagure](https://pagure.io/pagure) which needs help migrating to Python 3
* [Gogs](https://github.com/gogs/gogs) which has some UI issues that could be closed
* [GitLab](https://gitlab.com/gitlab-org/gitlab-ce/) which has some features that could be added or improved
---
> from: [**Giancarlos**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394127526) on: **2018-6-3**

Plot Twist: Microsoft buys GitHub and open sources it.
---
> from: [**JapaneseSquid**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394131309) on: **2018-6-3**

Plot twist: GitHub buys Microsoft 
---
> from: [**RobRoseKnows**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394137024) on: **2018-6-3**

The ideal IMO is Microsoft giving GitHub and endowment to form a Mozilla-like foundation.
---
> from: [**CharlesGueunet**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394140331) on: **2018-6-3**

Do not forget Microsoft already is second open source contributor on GitHub. They may need to open source more of their products for me to like them but they have shown they are not against open source.
---
> from: [**charjac**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394150251) on: **2018-6-3**

decentralized github alternative:

https://git.scuttlebot.io/%25n92DiQh7ietE%2BR%2BX%2FI403LQoyf2DtR3WQfCkDKlheQU%3D.sha256
---
> from: [**Dog2puppy**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394169257) on: **2018-6-3**

I’m moving to GitLab. At least they give free private repos. 
---
> from: [**ghost**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394172203) on: **2018-6-3**

&gt; Plot twist: GitHub buys Microsoft

Plot twist of plot twist: Github buys Microsoft and makes it free software
---
> from: [**ghost**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394172852) on: **2018-6-3**

&gt; but they have shown they are not against open source.

They were [against](https://www.linuxinsider.com/story/34292.html) more than you could think they were. They just couldn&#x27;t fight it.

Anyway, we will probably know microsoft has bought github when they delete my comment
---
> from: [**CharlesGueunet**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394174503) on: **2018-6-3**

On Sun, Jun 3, 2018, 6:13 PM TheCodeRunsMe &lt;notifications@github.com&gt; wrote:

&gt; but they have shown they are not against open source.
&gt;
&gt; They were against &lt;https://www.linuxinsider.com/story/34292.html&gt; more
&gt; than you could think they were. They just couldn&#x27;t fight it.
&gt;

That is an interesting read, but do not forget the GPL is not the only open
source license.
Moreover and more importantly, this article from 2004. Things may change in
14 years, like for example I did not even find the main website they are
talking about in the article.

&gt; Anyway, we will probably know microsoft has bought github when they delete
&gt; my comment
&gt;
&gt; —
&gt; You are receiving this because you commented.
&gt; Reply to this email directly, view it on GitHub
&gt; &lt;https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394172852&gt;,
&gt; or mute the thread
&gt; &lt;https://github.com/notifications/unsubscribe-auth/AFKW4xqDBDRk95eBSrCccWdJl2dubhD8ks5t5AsogaJpZM4UX3KC&gt;
&gt; .
&gt;

---
> from: [**stereobooster**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394182205) on: **2018-6-3**

&lt;blockquote class&#x3D;&quot;twitter-tweet&quot; data-lang&#x3D;&quot;en&quot;&gt;&lt;p lang&#x3D;&quot;en&quot; dir&#x3D;&quot;ltr&quot;&gt;If Microsoft actually buys GitHub. &lt;a href&#x3D;&quot;https://t.co/GcicxrJVYW&quot;&gt;
&lt;img src&#x3D;&quot;https://user-images.githubusercontent.com/179534/40889895-68698624-676e-11e8-91db-fd0182107a38.jpg&quot; /&gt;&lt;/a&gt;
&lt;/p&gt;&amp;mdash; Daryl Ginn (@darylginn) &lt;a href&#x3D;&quot;https://twitter.com/darylginn/status/1002879517751414784?ref_src&#x3D;twsrc%5Etfw&quot;&gt;June 2, 2018&lt;/a&gt;&lt;/blockquote&gt;

&lt;blockquote class&#x3D;&quot;twitter-tweet&quot; data-lang&#x3D;&quot;en&quot;&gt;&lt;p lang&#x3D;&quot;en&quot; dir&#x3D;&quot;ltr&quot;&gt;I think it&amp;#39;s time I publicly shared about how Microsoft stole my code and then spit on it.&lt;br&gt;&lt;br&gt;I&amp;#39;d been waiting for them to do something about it, but that is clearly never happening. &lt;a href&#x3D;&quot;https://t.co/yCui1neBZz&quot;&gt;https://t.co/yCui1neBZz&lt;/a&gt;&lt;/p&gt;&amp;mdash; puppies, daddies, and hot butts (@jamiebuilds) &lt;a href&#x3D;&quot;https://twitter.com/jamiebuilds/status/1002696910266773505?ref_src&#x3D;twsrc%5Etfw&quot;&gt;June 1, 2018&lt;/a&gt;&lt;/blockquote&gt;

---
> from: [**tay10r**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394195975) on: **2018-6-4**

According to a report by Bloomberg, they&#x27;ve already been acquired and it might be announced Monday. I&#x27;m going to wait for the announcement to be fair and probably research the alternatives in the meantime
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394196328) on: **2018-6-4**

@tay10r, I heard 😢

But no reason to stop. We have to keep building a movement against such concentration of power... Microsoft, Facebook, Google... Note a backlash recently forced Google to back out of a partnership with the US Military. Can&#x27;t stop. The free world depends on free people speaking up. 
---
> from: [**tay10r**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394197071) on: **2018-6-4**

@vassundanagunta ahem, the military needs Google too!

All jokes aside, Microsoft has been open sourcing their projects. Maybe they&#x27;ll open source GitHub? Perhaps this is wishful thinking, but I&#x27;d like to see what their plans are before I do anything.
---
> from: [**gajus**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394198492) on: **2018-6-4**

![dey74y_uwaaucbj](https://user-images.githubusercontent.com/973543/40892098-ceda7c40-6789-11e8-87ac-7a4496e8e3fc.jpg)

source: https://twitter.com/hdevalence/status/1003383500974665728
---
> from: [**brunoczim**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394212454) on: **2018-6-4**

About Microsoft and Open Source: Embrace, Extend, Extinguish...
---
> from: [**chuojiang**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394225271) on: **2018-6-4**

Maybe we can launch a distributed GitHub alternative based on blockchain.
---
> from: [**tay10r**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394225719) on: **2018-6-4**

@chuojiang isn&#x27;t that what [git ssb](https://git.scuttlebot.io/%25n92DiQh7ietE%2BR%2BX%2FI403LQoyf2DtR3WQfCkDKlheQU%3D.sha256) is? I haven&#x27;t used it yet, but someone on this issue brought it up.
---
> from: [**luke-jr**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394227368) on: **2018-6-4**

git itself is already as &quot;blockchain&quot;-based as would make sense for something like this.
---
> from: [**chuojiang**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394248906) on: **2018-6-4**

@tay10r I just looked at git-ssb, which is quite simple. We use GitHub because it&#x27;s not just a git hosting provider, it&#x27;s also a community ecosystem, with all kinds of features:

* A powerful search engine that allows us to find specific project or code;
* Feed stream and notification system;
* Visualize project creation and permissions settings;
* Visualize watch, add stars, fork, and pull requests;
* Issues and wiki modules which deeply integrate with git;
* Rank system, including explore and trending;
* Developer-friendly API.

Perhaps we can build distributed GitHub alternative based on EOS or other blockchain systems.
---
> from: [**awilfox**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394301394) on: **2018-6-4**

GitHub, a private for-profit corporation, should not be a &quot;community ecosystem&quot; for libre software; we already have most of those things in freer alternatives:

* [Fossies](https://fossies.org/) provides a similarly-scoped search engine.
* Release tracking and &quot;trending software&quot; can be done with something like [FreshCode](http://freshcode.club/).
* GitLab, Gogs, and Pagure all have pull requests, and the original PR system uses mailing lists and &#x60;git send-email&#x60;; public mailing lists are by nature decentralised and allow almost anyone with an internet connection to contribute **_without_** needing to sign up for another account with a corporation or organisation they may not trust.
* GitLab, Gogs/gitea, and Pagure all have wikis that are Git repositories, and Pagure even lets you access project metadata in a Git repo so it can be exported to another forge if you change your mind.
* The &quot;friendliness&quot; of GitHub&#x27;s API is personal taste (I find it dreadful), but since it is ubiquitous, I&#x27;ve already written a small wrapper to let you use the commit hook [from any Git repository, hosted anywhere in the world](https://github.com/awilfox/real-git-rcmp).  This could be easily extended to other features if anyone wants to pick it up (or just take the idea and implement it in $language_of_the_week).
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394305126) on: **2018-6-4**

Well yeah we should pack up and move, there are better alternatives. 
---
> from: [**bashrc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394310406) on: **2018-6-4**

In a way Github being bought actually makes things a lot clearer for me. Github always was a proprietary silo that I initially onto got onto in order to be able to contribute to some other projects. Things just kinda snowballed. Microsoft buying it turns it from unpleasant to radioactive.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394434737) on: **2018-6-4**

@luke-jr looks like you got your wish.

I was about to close this since you did. But looks like you&#x27;re the man to write the case for options that don&#x27;t involve a centralized solution, i.e. not GitLab.  Can you take that on? 
---
> from: [**Dog2puppy**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394486342) on: **2018-6-4**

Kinda happy that GitHub was bought now. I was wanting to move to GitLab entirely before this but I didn’t really want to because GitHub was more popular. 
---
> from: [**sebastianbarfurth**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-394529143) on: **2018-6-5**

@Dog2puppy That argument doesn&#x27;t work I think, it will very certainly remain the most popular option for at least another couple of years. Either you move because you like GitLab and don&#x27;t like Microsoft or you don&#x27;t - popularity really is a thing they will keep going for them.
---
> from: [**jorgebucaran**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/1#issuecomment-395015219) on: **2018-6-6**

Is anyone keeping a list of projects or users that already left or are actually leaving GitHub? I&#x27;d be curious to see who&#x27;s on that list. I am talking about notable open source projects, not people that haven&#x27;t really invested much in the site.
