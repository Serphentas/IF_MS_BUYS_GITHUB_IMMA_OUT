# [Moving on, the importance, and the difficulties](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/17)

> state: **open** opened by: **bashrc** on: **2018-6-4**

I don&#x27;t think it can be stopped.

Frankly, I&#x27;ve been saying I was going to get out of Github for the last three years, but somehow never got around to it. The convenience of being able to search code and projects was attractive.

I&#x27;m a free software developer so there&#x27;s no way I&#x27;m going to stay on a platform controlled by Microsoft. It doesn&#x27;t matter how much they claim to have changed or how much they fund the Linux Foundation. I know what Microsoft does when it takes over companies. Things won&#x27;t change immediately, but over time it will be extend and extinguish for anything not directly contributing to Microsoft&#x27;s bottom line.

I seem to have accumulated a lot of repos. I&#x27;ll back them up, move them to a different system and then I&#x27;m outta here. Without an account it will be more difficult to contribute patches to projects which remain here, and that kind of disruption is exactly what Microsoft wants. Break up FOSS. Control the parts of it which directly support their business operations.

### Comments

---
> from: [**darkpixel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/17#issuecomment-394373927) on: **2018-6-4**

LOL: https://monitor.gitlab.net/dashboard/db/github-importer
---
> from: [**darkpixel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/17#issuecomment-394374329) on: **2018-6-4**

I think a new bug needs to be opened &quot;Switching to GitLab&quot; and then developers can reference that bug in commits for things like updating the README, etc...
---
> from: [**bashrc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/17#issuecomment-394431226) on: **2018-6-4**

In my case I&#x27;m not switching to Gitlab. It&#x27;s too big and high maintenance for me to self-host, so I&#x27;ll be using Gogs. I&#x27;ll be asking around to see if I can also mirror to other people&#x27;s servers as a fallback.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/17#issuecomment-394440685) on: **2018-6-4**

Subsumed by #32. This thread will be used a a resource. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/17#issuecomment-395576473) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
