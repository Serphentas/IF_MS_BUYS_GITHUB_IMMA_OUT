# [create our personal peer to peer github over google drive](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54)

> state: **open** opened by: **MHTaleb** on: **2018-6-5**

Hi this is may be stupid but is my personal Idea that I want to share

  1- develop a client WEB-Mobile-Desktop app that have all similarities of github
  2- this app uses google drive as way of sharing
  3- this app can encrypt or let it open
  4- share the drive link repository

What do you think ?


### Comments

---
> from: [**loo41**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-394695098) on: **2018-6-5**

I don&#x27;t think it&#x27;s feasible because it&#x27;s hard to produce such an excellent community within a certain period of time
---
> from: [**BangL**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-394696117) on: **2018-6-5**

you&#x27;re kinda trying to replace a car with a bicycle ...which has no wheels.
git already is a decentralized version control, no need to replace it ...especially not by regular file structure.
git is not gitHUB, git is not bought by MS, and there is no reason to move away from git at all.
we only need a replacement for gitHUB, which uses git and extends it by it&#x27;s web interface, pull requests, easy forking, comparing across forks, integrated wiki and so on.
and there are some, like gitlab or gitea.. gitea is self hosted, while gitlab can be self-hosted but it can also be used like github at gitlab.com, it lacks some features like comparing across forks yet though.
---
> from: [**Leodmanx2**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-394699943) on: **2018-6-5**

So you don&#x27;t want Microsoft to host your projects but Google is okay?
---
> from: [**MHTaleb**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-394744556) on: **2018-6-5**

@Leodmanx2  that means that I work for google 
---
> from: [**checkraiser**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-394804978) on: **2018-6-5**

We must have truely decentralized storage first.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-394813832) on: **2018-6-5**

This suggestion makes no sense with respect to what we&#x27;re trying to achieve (distance ourselves from big for-profit companies &amp; issues that come along). Close this already.
---
> from: [**Rafagd**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-394828637) on: **2018-6-5**

The problem with your suggestion is that Version Control Systems (VCS) like cvs, svn, hg and git were created exactly to avoid these types of solutions.

Also, I&#x27;d rather stay with M$ if the option is moving to Google. 
---
> from: [**angela-d**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-394874457) on: **2018-6-6**

&gt; Also, I&#x27;d rather stay with M$ if the option is moving to Google.

Worth mentioning my post in this thread, about Gitlab (which is where most are fleeing to): https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-394876421) on: **2018-6-6**

Please see #72 for a decentralized platform solution @checkraiser 
---
> from: [**Rafagd**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-395055492) on: **2018-6-6**

&gt; Worth mentioning my post in this thread, about Gitlab (which is where most are fleeing to): #9

Well, there&#x27;s that too. But I mean using Google (drive in this example, but not restricted to it) or M$-owned GitHub in a more direct way, since if you want to &quot;outsource&quot; your server-related worries out, you&#x27;ll need to deal with one of the big bad corps anyway (unless you know some smaller, less evil cloud service you trust).
---
> from: [**angela-d**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-395180126) on: **2018-6-6**

@Rafagd There&#x27;s also independent code hosts like [Not a Bug](https://notabug.org/)

Running away from Microsoft into a Google property is like cutting off your nose to spite your face.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/54#issuecomment-395578185) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
