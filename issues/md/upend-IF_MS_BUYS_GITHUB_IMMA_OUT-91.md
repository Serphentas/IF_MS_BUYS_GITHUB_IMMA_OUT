# [Should this project be a total free speech zone?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91)

> state: **closed** opened by: **vassudanagunta** on: **2018-6-6**

I&#x27;ve been censoring trolls, people who come here specifically to undermine this project, and even people who support the project but keep challenging my use of censorship. Whether or not you agree with my method, I feel I have no choice lest this project, with over 3,600 supporters and counting, will, as another contributor put it, &quot;turn into a pile of shit very fast.&quot;

This Issue is a space **for people who support** this project but not my censorship to express their opinions. Anywhere else in this repo such debates are subject to being deleted. 

**I will delete comments here from non-supporters of this project.** 

# why i censor

*I wrote this very fast. I will update it when I get some more time (and sleep).*

This org isn&#x27;t an open-ended, neutral forum. It is one with a specific mission, a specific agenda. People who disagree with it have plenty of places to voice their opinion. They don&#x27;t get to derail this org&#x27;s work or jam our signal with a bunch of noise.

The ACLU fights for people&#x27;s civil liberties, and will even defend the right of the KKK to speak and assemble (peacefully). I applaud that. But does the ACLU let KKK members into its meetings and allow them to criticize the ACLU&#x27;s work for black civil rights right there, in those meetings? Of course not.

Would it have been a contradiction or hypocrisy for Martin Luther King, John Lewis and other leaders of the non-violent civil rights movement of the 60s to disallow the participation of the KKK (white supremacists) or the Black Panthers or the Nation of Islam (black rights groups advocating violence) in their meetings and protests? Of course not.

Debates about which GitHub alternative should be recommended, or about [whether an image I posted on behalf of this group was insensitive to women](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58) are on topic and very welcome. Posts by people opposed to this group or its aims don&#x27;t belong here. I made that clear in the README as well as in the Issue Template. 

I&#x27;ve only had 3 hours of sleep for four nights in a row, and zero so far tonight (5:55am). I&#x27;m doing the best I can to keep this project on track. I&#x27;ll write more on this when I get the chance. 

### Comments

---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395012889) on: **2018-6-6**

I think no, it shouldn&#x27;t. Common ground should be &quot;if ms buys gh imma out&quot;. Other discussions are just noises for our signal. 
---
> from: [**jorgebucaran**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395013487) on: **2018-6-6**

&gt;  Should this project be a total free speech zone? #91

**Yes, it should**. By censoring [critical-constructive](https://en.wikipedia.org/wiki/Varieties_of_criticism#Constructive_criticism) comments or issues you are only likely to disgust people and spoil any legitimate interest in your project. You are more likely to gain supporters by encouraging critique and discussion. In the end, they may even change your mind or you may change their mind.



---
> from: [**MoonlightOwl**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395015160) on: **2018-6-6**

This repo is a representation of very hot and opinionated question right now.
Some &#x27;fluctiations&#x27; from the main topic are inevitable, and must not be censored.
But some kind of moderation is strictly necessary for keeping all in order.
---
> from: [**dou4cc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395015537) on: **2018-6-6**

Anyway, censor the comments by editing them into blank comments rather than just delete them, which leaves a possiblity to see the origin threads.

#best_practise
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395016637) on: **2018-6-6**

But this is not a Microsoft discussion forum and if we start off topic discussions we will be lost in noise. People are discussing this for years, nothing new. I have friends who are totally okay with Microsoft and I have friends who are disgusted like me. Nobody could change each other&#x27;s mind for years.

This repo is for like minded people to find a GitHub alternative. If you are OK with MS, go back to your life. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395022108) on: **2018-6-6**

## I will delete comments here from non-supporters of this project.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395022348) on: **2018-6-6**

@dou4cc I didn&#x27;t realize I could do that on regular comments. I will do that going forward. 

Wait... No. That leaves an incentive to keep violating the rules of this forum. No, I will out-right delete. Sorry. 
---
> from: [**dou4cc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395030776) on: **2018-6-6**

@vassudanagunta 

&gt; Wait... No. That leaves an incentive to keep violating the rules of this forum. No, I will out-right delete. Sorry.

You can just block those keeping violating the rules using the org account. BUT PLEASE DO NOT DIRECTLY DELETE COMMENTS ANY MORE.
---
> from: [**karlisk**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395031137) on: **2018-6-6**

The main point of people against this movement that are showing up in this repo is to shout: &quot;if you don&#x27;t like what happened, no one&#x27;s keeping you here&quot; it is indeed frankly just trolling... I mean - **that&#x27;s the point nimrod** everyone gathering here is not doing it to demand immediate shutdown of GitHub but to move out to a GitHub alternative. In fact - no one here is forcing anyone to leave so anyone who has a problem with this repo can just ignore it and move on with their day. 

Anyone, feel free to correct me if I&#x27;m wrong but as far as I got the idea, this is not a petition or a protest movement to destroy GitHub. Anyone against leaving can just do the opposite - not show any support, stay at GitHub and generally be a happier person instead of being a snarky, negative and often nasty douche. If anyone has a problem with others leaving GitHub then it&#x27;s their own damn problem. If this is causing workflow interruptions because their tem is moving out of GitHub - they should bring this problem up with **their team**, **not here**.  
---
> from: [**dou4cc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395032474) on: **2018-6-6**

Take a good sleep and think twice.
---
> from: [**AnaRobynn**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395043368) on: **2018-6-6**

Ok, lets slow down a bit.
It&#x27;s perfectly fine to create a repository to look for alternative solutions, find migration paths etc. But it has 4 days and the only thing I see in the issues is people who like the repo bashing microsoft and people who disagree bashing the repo. 

It has been 4 days and there is no significant results in how to progress and take the next steps. Where are the migration guide in the repo, where is are the alternatives listed, ...?
To me this repo, when you first land on it really feels like it&#x27;s just about bashing Microsoft without actually providing solutions. 

The fact that you complain about Microsoft censorship (which is not true, github&#x27;s explore/trending does weird shit sometimes) and then edit issues or comments, because you don&#x27;t agree with them is two-faced. I was genuinly interested in this movement, but the fact that I saw edited and deleted comments spoiled my interest in the whole concept, it&#x27;s so disgusting. 
You could have just commented on it that they don&#x27;t help with the discussion and close the issue.

It&#x27;s sad to see that there is such a strong reaction behind the movement, which yields so little results. If I don&#x27;t start seeing alternatives options, migration paths, etc in the repositiory I think that the repository is just about bashing Microsoft, rather than actually finding a solution. 

Inb4 deleted comment.
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395047786) on: **2018-6-6**

@AnaRobynn Bullshit. Everyone was busy with enumerating options until MS lovers came and started to cry.

This repo is not a court and we don&#x27;t wanna discuss if this acquisition is good or bad. This is a place for who believes it&#x27;s bad. 

We are not interested in bashing MS, if you leave us please we are busy with deciding the best alternative.

Thanks. 

---
> from: [**emanuil-tolev**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395050637) on: **2018-6-6**

&gt; I&#x27;ve only had 3 hours of sleep for four nights in a row, and zero so far tonight (5:55am). I&#x27;m doing the best I can to keep this project on track. I&#x27;ll write more on this when I get the chance.

I don&#x27;t have an opinion on what you do with comments on these issues but please, please go to sleep (and continue explicitly tracking how you take care of yourself after - water, food, sleep). This isn&#x27;t worth your mental and physical health.
---
> from: [**mcmonkey4eva**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395058745) on: **2018-6-6**

As a general note that belongs here more than elsewhere probably: You can believe that MS is evil or believe it&#x27;ll all be fine, but either way: Please ensure your opinion is informed. Don&#x27;t go blindly, make sure you know the facts and your choose was made reasonably.

As an example of what I mean:
It seems rather odd that the readme file would contain &#x60;BREAKING NEWS: Microsoft GitHub just censored this project from its Trending page&#x60;
My issue with that isn&#x27;t regarding whether censorship was intentional or not on the part of GitHub... it&#x27;s the &quot;Microsoft GitHub&quot; phrasing.
The reason I take issue with that:
Microsoft does not own GitHub.

Microsoft announced on June 4th that they have made a deal to acquire GitHub.
They have not acquired it. They&#x27;ve made a deal to acquire it.
The acquisition will occur later this year (loosely estimated to be done by the end of calendar year 2018).
They have several layers of government regulation and other validation before they own GitHub.

If Microsoft was caught taking controlling actions over GitHub at this stage (such as ordering censorship of a repo from trending list), that would be a serious risk for them, as government regulators may take that as a sign that the acquisition is not being done in good faith.

If you want to leave GitHub before MS takes control, sure. But don&#x27;t leave it immediately under the assumption that MS already owns it - they don&#x27;t.
---
> from: [**ChrisCallanJr**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395077005) on: **2018-6-6**

@mcmonkey4eva I agree its likely not Microsoft pulling the strings on this one, *however* it is common in business acquisitions for the business that is to be acquired to conform and act in the interests of the buyer once a deal has been made public, in this case, pulling down a repo that is literally strategizing to pull users away from the site.
---
> from: [**douglascamata**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395166445) on: **2018-6-6**

If you censor a group of people (the ones who doesn&#x27;t support this) you&#x27;re violating point 2.C of GitHub ToS, exactly where it says:

&#x60;&#x60;&#x60;
2. Content Restrictions
You agree that you will not under any circumstances upload, post, host, or transmit any content that:

* is discriminatory or abusive toward any individual or group;
&#x60;&#x60;&#x60;

By doing this the repo will be subject to deletion and you become not any better than Microsoft ;)

---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395169833) on: **2018-6-6**

@douglascamata Except that they aren&#x27;t being deleted based on discrimination, but on the fact that they are off-topic/unrelated to the purpose of the repo, which is clearly stated in the README. Sorry, nice try though!
---
> from: [**douglascamata**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395180278) on: **2018-6-6**

Censorship in any way is bad, UNLESS people are being offensive, harassing others, etc. Just because you state in the README &quot;this repo is about X&quot; you cannot censor people based on this and think you are less tyrant that anybody else that censors others. You are protesting here against MS buying github, some people might protest against your arguments or might want do discuss. With a healthy discussion and arguments you could even win more supporters. 
---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395183039) on: **2018-6-6**

Sure, but my point was that technically it&#x27;s not discrimination and therefore there has been no violation of the GitHub ToS.

Furthermore, this repo isn&#x27;t about protesting against / bashing MS, and perhaps all comments that do this should also be deleted.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395250362) on: **2018-6-7**

@douglascamata Please specifically address my points at the top of this issue. 

In your world The ACLU or a civil rights group or even GitHub or Microsoft can&#x27;t have a meeting about a project that is closed to people opposed to that project?

Copied from another comment of mine:

&gt; I have ZERO power to censor people&#x27;s opinions. I am just saying they don&#x27;t get to state those opinions here. **This IS NOT an open forum.**

&gt; Read my points at the top of #91. Tell my how you would run the ACLU? Because you are fighting for the KKK&#x27;s free speech rights, does that mean you would allow them into your ACLU planning and organizing meetings? What if they wanted to add anti-Semitic or racist items to the agenda? What if when you said no, they kept interrupting the meeting with complaints that you were censoring them?

&gt; Have you ever led a group or a meeting of a large number of people not of the same mind? 


Please answer my specific points, not just repeat &quot;censorship is wrong&quot; again.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395250989) on: **2018-6-7**

### But anyway, this is not going to be an issue anymore very soon. 

I&#x27;m going to be making an announcement and a change to to this repo soon that will resolve this conundrum I&#x27;ve been facing. Those of you who want a space free of censorship, or my decisions, can have that (though you&#x27;ve always had that option).
---
> from: [**evyatarmeged**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395301207) on: **2018-6-7**

@AnaRobynn pretty much summed it up:
**&quot;I was genuinely interested in this movement, but the fact that I saw edited and deleted comments spoiled my interest in the whole concept, it&#x27;s so disgusting.&quot;**
I think stuff like this drives people away after they initially showed some interest.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395578830) on: **2018-6-7**

Hey all. Please read #114.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395589425) on: **2018-6-8**

@AnaRobynn How can you say no work has been done in finding the alternatives ? [This spreadsheet](https://docs.google.com/spreadsheets/d/1LZryaOx4VYHQ_oWLtRjvFrr4RhVjR484eFe7du3Usa4) has been getting constant update over the last few days, and [there](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/25) [are](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44) [multiple](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63) threads (just to list some) in which we debate and discuss way we can go away from GitHub.

Also, how do you want us to write any migration guide when we don&#x27;t even know what service we&#x27;re going to use next ? It make absolutely zero (0) sense.

Just because you see some shitposters does **not** mean that this movement is shit or that people taking part of it are shit.

Please have some respect for what&#x27;s been done until now. Moving out of GitHub is not a mere task, and surely trolls and shitposters will be present. After all, we&#x27;re on the Internet, what did you expect ?

Now that [this repo is about to die](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114), I think people really interested in this migration should continue their discussions [over here](https://github.com/bderiso/Microsoft-Github-acquisition/issues) (at least the technical part) since that&#x27;s where most of the comparison is done regarding the alternatives.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395590317) on: **2018-6-8**

@Serphentas It&#x27;s not going to die. It&#x27;s just not going to try to host N teams with X philosophies, to be all things to all people. I&#x27;ve worked in a lot of organizations, from tiny startups to big corps to activist movements, and decentralization is best. 
---
> from: [**danilw**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395600578) on: **2018-6-8**

best project ever

keep it up :)
---
> from: [**AnaRobynn**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395662991) on: **2018-6-8**

@Serphentas
Sure there is lots of discussions in the issues, but people don&#x27;t see any progress in the repo itself. 
We could/should have an alternatives.md file (or something) where we continuously update the alternatives in stead of keeping in the discussions. (for clarity)
I know that it&#x27;s being discussed and progress is made in the issues, but it would be more transparent if it also translate to repo files.

Why? The first thing that people see is &quot;OMG Microsoft blocked the repo&quot; and check out the repository for options, but don&#x27;t actually see alternatives &#x3D; bad.
As soon as they go to the issues they notice people crying and comments issues being censored &#x3D; bad. 

Why wouldn&#x27;t you be able to write a migration guide? Do you want to force people into the same platform?
Why not offer multiple alternative so people can actually choose a platform that suits their needs?

What ruined it to me or not the shitposters, but the fact the repo owner changed title and description and closed them. Just close them. It really felt like a hostile place.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395672150) on: **2018-6-8**

@AnaRobynn 

&gt; We could/should have an alternatives.md file (or something) where we continuously update the alternatives in stead of keeping in the discussions. (for clarity)

Dude, that&#x27;s a judgement call. The README says that a list will come. I&#x27;m not going to prematurely publish a list of recommendations without actually having some depth of analysis -- otherwise they are not recommendations but nothing more than a prettied up [search result for &quot;github alternatives&quot;](https://duckduckgo.com/?q&#x3D;github+alternatives&amp;t&#x3D;osx&amp;ia&#x3D;web). Sorry, no.

&gt; I know that it&#x27;s being discussed and progress is made in the issues, but it would be more transparent if it also translate to repo files.

Sorry, that&#x27;s less transparent. Presenting something just for appearances and to cover up the fact that we don&#x27;t know yet (because that is exactly what you are saying) is about the least transparent thing I can think of. 

&gt; As soon as they go to the issues they notice people crying and comments issues being censored &#x3D; bad.

If I didn&#x27;t censor like I did, then as soon as they&#x27;d go to the issues they&#x27;d see flamewars and debates and the reasons why we are idiots (because you have me to leave those comments in place, no deletions). The subscribers to this repo **don&#x27;t want to see that shit**. My intent was that visiting this repo would be like visiting the EFF.org website, a site built by people of like-mind on the site&#x27;s raison d&#x27;être. You wouldn&#x27;t call the EFF hostile for refusing to publish opposing viewpoints, would you? But there was my mistake... EFF.org doesn&#x27;t have a comments feature on its articles for precisely this reason. They &quot;censor&quot; (to use your attitude) _all_ outside voices, both those who disagree with them as well as their equivalent of you.

&gt; What ruined it to me or not the shitposters, but the fact the repo owner changed title and description and closed them. Just close them. It really felt like a hostile place.

I really don&#x27;t give a damn about your opinion on this (or all the thumbs down I&#x27;m going to get on this comment). Hostile? Yeah. Look at your speech here. It&#x27;s all hostile  backseat driver judgement. I deleted exactly those posts that violated the rules. Not any other dissent. Not even the one that (politely I might add) called me out for posting something sexist. 

Between the choice of how deleting those comments looked, and how leaving them there would have looked, I have no regrets. But my mistake was thinking those were my only two choices. [I&#x27;ve realized that and it will be rectified shortly](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114). My regret is having opened up this forum in the first place, both because of those &quot;shitposters&quot; and, frankly, people like you. You have neither helped the cause of this repo, nor have helped the cause of free speech. You&#x27;re just a grandstanding sophist. 

As @Serphentas asked, &quot;Please have some respect for what&#x27;s been done until now. Moving out of GitHub is not a mere task&quot;
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395838280) on: **2018-6-8**

#116, especially the second panel, nails why I deleted rather than taking lighter steps.
