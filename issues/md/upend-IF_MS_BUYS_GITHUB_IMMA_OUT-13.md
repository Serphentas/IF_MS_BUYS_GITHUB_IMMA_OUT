# [The readme needs to articulate why](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13)

> state: **open** opened by: **differentsmoke** on: **2018-6-4**

The readme needs an articulate, explicit description of your point, why you think it is bad, what the problems will be, etc. So far, it just seems like an aversion to Microsoft, which ultimately I don&#x27;t believe is your main point.

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394237312) on: **2018-6-4**

Thanks for your thoughtful and polite reply, @differentsmoke. But, respectfully, I think that 500 stars in 48 hours says otherwise. This repo is for people who don&#x27;t need to be convinced. For those who get the open source ethos. For those who instinctively get that the concentration of power is bad for an open and free internet just as it is bad for societies, governments and economies. I&#x27;m preaching to the choir.

But yes, now that the acquisition seems a done deal, it&#x27;s time to turn this into a platform for next steps. I&#x27;ll be working on that. Wanna help?

btw, [upend.org](http://www.upend.org) will be about articulation of the problems I (and I think you) hint at above. Let me know if you want to help with that. Or watch this repo as I&#x27;ll be updating. 


---
> from: [**emmekappa**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394289891) on: **2018-6-4**

So it&#x27;s ok for GitHub Company to _concentrate the power_ but not for Microsoft Corp. ?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394296327) on: **2018-6-4**

You may be right, that a solution that is entirely decentralized would be better for open source. Many people believe that. But there is also an argument that any community needs a shared town square, a community hall, a place to gather and see what everyone else is up to. If so, such a place needs to be independent. Such a place should only have one agenda: to host and service this community. Maybe relying on a for-profit company such as GitHub to be that steward isn&#x27;t ideal (And some believe that this betrayal of the community is proof that we were stupid), but at least if its profit was dependent entirely on doing this right, it could work, far better than if it had other agendas, other masters. 

GitHub&#x27;s concentration of power was organic, created by and existing for the open source ecosystem. 

Microsoft does not exist for the open source community. It has its own agenda. For it, this is just a play. A chess move. we are the pawns. Acquisitions are not an organic growths in power. They are about domination. For example, [American tech giants are making life tough for startups](https://www.economist.com/business/2018/06/02/american-tech-giants-are-making-life-tough-for-startups).

I&#x27;m curious why you are eager to defend a $750 billion company and distrust the instincts of people who believe in open source, in doing things for the world not-for-profit but just to do things for the world, who believe in independence, who instinctively distrust concentration of power, _especially_ inorganic ones like this. 
---
> from: [**prestonvanloon**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394336943) on: **2018-6-4**

@vassudanagunta, I still think your motivation is an aversion to Microsoft, after reading your comments.

Microsoft is one of the biggest contributors to open source community. They have created/supported over 7000 open source projects including VS Code, Typescript, and RxJS. Microsoft also has the infrastructure and resources to scale and grow GitHub.

I’m not defending the Microsoft, but I don’t see why this acquisition is bad. Why should anyone exert any amount of energy by being upset, moving off GitHub, or creating repos like this one?

If/when GitHub starts to turn to shit, then I’ll leave. Otherwise who cares? Microsoft might make this a better place and you haven’t even given them a chance. 
---
> from: [**immexerxez**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394341504) on: **2018-6-4**

I don&#x27;t think most of you are really understanding why this is a problem. There is a huge conflict of interest with Microsoft owning Github vs Github owning itself. Microsoft is a **software** company Github is a **hosting** company. Microsoft has huge incentives to take Github and not only push their own products but push traffic away from their competitors or in the extreme push them off the site. With Github being owned by itself it actually has gigantic incentives to **NOT** do this. 
---
> from: [**differentsmoke**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394351074) on: **2018-6-4**

That is, indeed, a potential conflict of interests but, is there an actual incentive for Microsoft to do this? 
1. They have gotten in trouble in the past (for practices that Apple currently engages in without much resistance, btw).
2. The FLOSS community has punished misbehaving platforms in the past to a large degree of effectiveness (remember Sourceforge?).
3. Microsoft (like Google and Amazon) is moving towards SaaS/PaaS,and infrastructure business, and it is in their best interest to foster all software development.
4. It is hard to see how, in this rather open environment, they could &quot;pull a fast one&quot; on the community without everyone being aware. 
    - I think it would be more effective to lay out what you believe are unacceptable developments and react to those if and when they happen, than to simply assume they will.
5. Someone has mentioned that GitHub preferred this over going public. Have we compared the two alternatives and what they may entail?
---
> from: [**immexerxez**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394353444) on: **2018-6-4**

Yes as I stated they have competitors that in itself is an incentive. Also comments for your points they really have never been punished and I think it&#x27;s pretty off base to think the free software community has any capability to &quot;punish&quot; Microsoft. Your third point in itself **IS** the conflict Microsoft could very easily make it more difficult to use github w/ aws, google cloud etc. or could &quot;accidently&quot; delete their repos what have you. 
---
> from: [**immexerxez**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394357450) on: **2018-6-4**

What draws people to Github is/was the belief that they&#x27;re impartial and it&#x27;s 100% in their interest to remain as impartial as they possibly can. Github has become more than just open source software it&#x27;s become the place to share all sorts of information a great example I&#x27;d say is how the Chinese community has developed even though they&#x27;d be censored in their own county. Microsoft fundamentally will never have this level of impartiality because in no circumstance would that make sense in their primary business. They could very well operate Github completely impartially but there will always be some form of mistrust within the user base because at the end of the day they are a business that needs to make money w/ competitors. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394364814) on: **2018-6-4**

I&#x27;m not going to tolerate rudeness or trolling here. This is not a forum for debate. This is a space for GitHub Refugees. Please respect us and take your arguments elsewhere. If you want to argue, feel free to make your case on UpEnd&#x27;s twitter (link in the repo README).
---
> from: [**emmekappa**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394367745) on: **2018-6-4**

the conference call is live right now https://edge.media-server.com/m6/p/eudfciq3/dmediaset/audio
---
> from: [**immexerxez**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394382259) on: **2018-6-4**

I think it does more harm than good to delete comments. All opinions here are valid.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394383404) on: **2018-6-4**

@immexerxez yes, all opinions are valid. But this is not a forum for debate. This is a place for people who wanted an independent GitHub, and will leave if it isn&#x27;t. You don&#x27;t go into some Node project&#x27;s repo, for example, and start making comments on issues arguing that they should switch to Ruby.

The Hacker News discussions are, for example, a place to debate this. Or on Twitter. Not here.

Please be respectful of this.
---
> from: [**immexerxez**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394384483) on: **2018-6-4**

@differentsmoke they already did pull a fast one on VS Code users after the update I believe last month (anyone feel free to correct that) they automatically set 

&#x60;&#x60;&#x60;js
    &quot;telemetry.enableCrashReporter&quot;: true,
    &quot;telemetry.enableTelemetry&quot;: true
&#x60;&#x60;&#x60;
did not warn users and expected you to find/disable. 

As for 5. that is really interesting and I&#x27;d love to hear some peoples take on it. In that sense it really feels like stuck between a rock and a hard place and honestly if it came to it I&#x27;d rather them be acquired even by microsoft than have to fend for themselves w/ shareholders to appease every quarter. 
---
> from: [**GitSquared**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394450006) on: **2018-6-4**

If i may add my two cents to this comment section:

As @vassudanagunta stated in another comment, the problem with MSFT acquiring GitHub is mainly about the concentration of power.

I&#x27;m sure Microsoft can do great things with GitHub. Satya Nadella (MSFT&#x27;s CEO) has some history of willing to open up the company&#x27;s massive codebase and promote open-source projects. But he&#x27;s just a temporary commander of a massive organization that could abuse the enormous control they have over what we as developers do online.

Just think about it : **Microsoft now owns** (or at least will in a couple of months) **both GitHub and LinkedIn**. That&#x27;s the services most recruiters use to find developers (particularly freelance).

I don&#x27;t know how this will turn out but if you really believe in the whole open-source spirit and concept, you *should* move out just for the sake of staying away from the GAFAMs.
---
> from: [**quantumpacket**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394455636) on: **2018-6-4**

https://jacquesmattheij.com/what-is-wrong-with-microsoft-buying-github
---
> from: [**cooldesigns**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394490046) on: **2018-6-4**

Also you must add to readme like that &quot;Join discussions on issues&quot;
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394584560) on: **2018-6-5**

I cleaned up the README considerably. The opening expresses the gist of why without diving into an essay. I think that is sufficient for now. The purpose of this repo is not to debate whether the acquisition is good or bad... it&#x27;s for people who already think it&#x27;s bad who want to know what to do next. Any debate should be on that latter point.
---
> from: [**cooldesigns**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394770947) on: **2018-6-5**

@vassudanagunta I&#x27;m talking about the debate about what the solution will be. For those who think the sale is bad. But if we do not tell them we are discussing here, they will just read star and go
---
> from: [**komali2**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394810813) on: **2018-6-5**

@vassudanagunta your first reply is Argumentum ad populum and is a [logical fallacy](https://en.wikipedia.org/wiki/Argumentum_ad_populum)

10,000,000 Americans believing in intelligent design doesn&#x27;t add any weight to the intelligent design argument. 
---
> from: [**komali2**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394813291) on: **2018-6-5**

@immexerxez  point that the existence of a single incentive outweighs all other incentives. 

The listed incentives to make github bad are

1. because microsoft could push out all other competitors 

The listed incentives to make github not-bad are

1. any malpractice by microsoft will erode years of open-source evangelizing in an microsecond

2. Everyone is watching all good and bad actions right now

3. making github bad will push people to other platforms and reduce microsoft developer audience (could be stated as &quot;making github good will increase developer wellbeing and platform adoption&quot;)

Given that if they do incentive 1 for make-github-bad, the very existence of incentive 3 (other people can just go to other platforms) makes this argument perfectly untenable. The reason is they will be exchanging a tiny amount of platform control / profit for an incalculable amount of bad press, and that platform control will then immediately vanish. 

This is like speculating that Oscar Meyer might publicly use dog meat in their hot dogs to save a buck. It doesn&#x27;t make sense in the context (a planet with internet access and the means to tell eachother that oscar meyer is making hot dogs out of dog meat). 
---
> from: [**quantumpacket**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-394847882) on: **2018-6-5**

@komali2 I&#x27;m more concerned about all the private repositories for developers, startups, etc that are now accessible by a company that has a track record of stealing code and ideas and pushing it as something innovative they did. That&#x27;s monopolization on steroids when you now can access the proprietary code of potential future software competitors.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/13#issuecomment-395577068) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
