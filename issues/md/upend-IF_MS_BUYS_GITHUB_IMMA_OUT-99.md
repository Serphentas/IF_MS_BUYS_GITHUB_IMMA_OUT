# [troll](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/99)

> state: **closed** opened by: **lolnogenerics** on: **2018-6-6**

i am a troll.

### Comments

---
> from: [**Flarp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/99#issuecomment-395096487) on: **2018-6-6**

It&#x27;s not the acquisition itself, but the acquirer people have problems with. Microsoft has had a history of unethically exploiting and demeaning open source in the name of profit
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/99#issuecomment-395099352) on: **2018-6-6**

violating the house rules: don&#x27;t post here if you&#x27;re not a supporter of the project. 
---
> from: [**timsueberkrueb**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/99#issuecomment-395186315) on: **2018-6-6**

You complain that GitHub censors this repository from its trend side, while you censor the criticism in issues like this one yourself? I&#x27;m sorry, but this is ridiculous.
---
> from: [**SirJosh3917**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/99#issuecomment-395187731) on: **2018-6-6**

@timsueberkrueb he doesn&#x27;t censor them entirely - there&#x27;s a dedicated page if you&#x27;d like to speak out against him over [here](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91)
---
> from: [**timsueberkrueb**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/99#issuecomment-395189822) on: **2018-6-6**

@SirJosh3917 There&#x27;s not so much a problem in closing those issues, but editing the comments and subjects and replacing them with &quot;troll&quot; and &quot;i am a troll&quot; is unnecessary and makes you look ridiculous. 
---
> from: [**leokhachatorians**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/99#issuecomment-395214820) on: **2018-6-6**

this entire freak out is ridiculous.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/99#issuecomment-395215824) on: **2018-6-6**

@timsueberkrueb 

&gt; You complain that GitHub censors this repository from its trend side, while you censor the criticism in issues like this one yourself? I&#x27;m sorry, but this is ridiculous.

Apples and Oranges. See my post on #91
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/99#issuecomment-395581449) on: **2018-6-8**

On the question of censorship, please read #114.
