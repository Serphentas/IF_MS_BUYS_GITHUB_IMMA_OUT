# [Channels to indicate lack of support?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/53)

> state: **closed** opened by: **jorgebucaran** on: **2018-6-5**

Hi @vassudanagunta, interesting initiative! I just want to point that there is currently no way to indicate _lack_ of support other than not starring the repo, but we can&#x27;t really count how many people refrained from starring the repo intentionally. Maybe someone can create a new repo expressing opposite views and exhort folks to star it to indicate their support (or like this issue?)

An example of this is what happened with https://github.com/dear-github/dear-github and https://github.com/thank-you-github/thank-you-github back back in the day (+2 years ago).

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/53#issuecomment-394750807) on: **2018-6-5**

@jorgebucaran when I get time I&#x27;ll write a longer reply, but I don&#x27;t right now so this will have to do in the meantime:

You can vote opposed by simply staying on GitHub. Sadly, I&#x27;m pretty sure that the majority will. Also, you just received $7,500,000,000 votes in your favor. Isn&#x27;t that enough. Please leave us be. Please do not demand *every* project support and reply to every person who doesn&#x27;t like it. It&#x27;s really not fair. You have plenty of places to exercise free speech. 
---
> from: [**jorgebucaran**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/53#issuecomment-395007131) on: **2018-6-6**

Thanks, looking forward to your longer reply, I guess.
---
> from: [**MarounMaroun**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/53#issuecomment-395015518) on: **2018-6-6**

I don&#x27;t understand why this issue is closed. It&#x27;s obviously not against this repo, it only suggests creating a new one to indicate the *lack* of support.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/53#issuecomment-395023682) on: **2018-6-6**

So does the ACLU have to allot a room in its offices for people who don&#x27;t support the ACLU ideals and mission?
---
> from: [**MarounMaroun**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/53#issuecomment-395340037) on: **2018-6-7**

I understand you, but I think that:

&gt; We were the #1 project and the #4 &quot;developer&quot; before they deleted us. Maybe it was too embarrassing?
&gt; 
&gt; We have not violated any terms of service.
&gt; 
&gt; Is this Microsoft GitHub&#x27;s commitment to an open platform for all projects? Will this repo be deleted too? Will other projects that compete with Microsoft products or challenge its dominance be on the chopping block next?

seems to be conflicting a bit with what you&#x27;re trying to say.

Anyway, I do respect your repo, and wish us the best of luck.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/53#issuecomment-395580783) on: **2018-6-8**

On the issue of censorship, please read Issue 114.
