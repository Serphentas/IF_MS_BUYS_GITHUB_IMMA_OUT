# [Possible alternative, GitGud](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/69)

> state: **closed** opened by: **RyanGannon** on: **2018-6-5**

https://gitgud.io/users/sign_in is a hosted GitLab, their philosophy seems to be around open-source. 
&gt; Sapphire runs GitGud.io along with many other services. We are a user-supported open source company. Our company is designed in such a way that users are our only source of income, so we only serve them, their needs, and no other 3rd parties.
This could mean that it wouldn&#x27;t scale well but worth looking into.

### Comments

---
> from: [**Adelyne**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/69#issuecomment-394829574) on: **2018-6-5**

This is a [weird spin-off of the GamerGate, movement](http://thisisvideogames.com/gamergatewiki/index.php/GitGud) because Github was judged too &quot;SJW&quot;.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/69#issuecomment-394842330) on: **2018-6-5**

Please see #25 to suggest this service, although this is a GitLab instance so it should be listed as a GitLab 3rd-party instance. Consider closing this issue once that&#x27;s done to keep the issues list tidy.
---
> from: [**AkgunFatih**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/69#issuecomment-394851204) on: **2018-6-5**

since it&#x27;s powered by [Gitlab](https://gitlab.com/) we should use [Gitlab](https://gitlab.com/) instead
---
> from: [**RyanGannon**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/69#issuecomment-394868308) on: **2018-6-5**

I see what you mean @Serphentas, will do.
---
> from: [**TeoTwawki**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/69#issuecomment-394870057) on: **2018-6-5**

~~A thing I like about gitgud: they will not bow to pressure to censor projects no matter who shows up demanding your lead dev be canned over some argument on twitter or w/e.~~

~~[This](https://www.techdirt.com/articles/20150802/20330431831/github-nukes-repository-over-use-word-retard.shtml) will never happen at gitgud.~~ nvm just realize this sort of thing isn&#x27;t *welcome* here.
