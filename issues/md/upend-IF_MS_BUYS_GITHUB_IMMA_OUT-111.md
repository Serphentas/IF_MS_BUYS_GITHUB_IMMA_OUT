# [Update Licensing for MSFT](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111)

> state: **open** opened by: **ChrisCates** on: **2018-6-7**

It&#x27;s clear that a lot of people are upset about the MSFT acquisition.
For good reason too... Considering it is not uncommon for Facebook or Google to buy companies to kill them. I have reason to believe that Microsoft is no different in this approach, but presents it differently.

However, it&#x27;s clear that we want some actionable methods to prevent such things... To simply disband Github is a bit absurd for your public projects, for now at least... It is a big community with lots of people.

I think it would be a great idea to just update the licensing in your Github repos with additional clauses that specify that it is illegal for Microsoft to use it.

Perhaps a modified version of GPLV3. Maybe we could potentially modify MIT to work effectively this way. I think this should be a clear message of disdain to Microsoft that we as developers, want and crave creative freedom. And to not be restricted by temper tantrum throwing babies like Bill Gates.

### Comments

---
> from: [**ChrisCates**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395368515) on: **2018-6-7**

Also, for further context: https://www.gnu.org/philosophy/microsoft-verdict.en.html
And, also unrelated, but ever so important: When I see that Bill Gates has attempted to sue people for creating free software early in his career, and, then later in his career after he is filthy rich, whistleblows that there could be a potential virus pandemic... One could only assume his malicious intents.
---
> from: [**examachine**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395385743) on: **2018-6-7**

It&#x27;s not a bad idea. I thought it was bad because how about all those private crypto projects which they can now just view? Maybe, they badly wanted to plagiarize some critical software, how would we even know? The issue here is that Microsoft is not a company we can trust.
---
> from: [**ChrisCates**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395388319) on: **2018-6-7**

@examachine, I think it&#x27;s clear that any critical software should move off Github.

Anyone who has observed Microsoft&#x27;s track record and behavior (largely influenced by Bill) should know better than to play the greater fool.
---
> from: [**Marinofull**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395443881) on: **2018-6-7**

Hi guys I&#x27;m really considering to create a storage of modified Licenses after the idea from @ChrisCates 
---
> from: [**Marinofull**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395456805) on: **2018-6-7**

well, I started doing a repo with a modified version of MIT license. I&#x27;m not so good in &quot;legal language&quot; so it may have some breaches, I don&#x27;t know. I also worried if we can mention the EvilCorp name in License docs without do some infringement

https://github.com/Marinofull/licenses-anti-ms-pourposes
---
> from: [**mcmonkey4eva**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395515305) on: **2018-6-7**

You definitely cannot get away with writing a license that arbitrarily excludes a specific legal entity while being otherwise permissive.
The closest you can get is to form a default exclusion with certain (generalized!) inclusions of permission.
There is no way to form a valid legal license that will exclude Microsoft without excluding everyone in general.
You might set up a license that requires the original license be kept alongside any data duplicated from a project, including access to the source data of the original data, any modifications to it, and any code that depends on it (ie: The project trying to make use of your project).
But... that doesn&#x27;t stop Microsoft from anything other than using your project in their proprietary work - they can still use it in GitHub-related ways freely, and use it in their open source work freely.
Also: Any and all permissions that apply to GitHub, further apply to Microsoft after Microsoft has completed the acquisition of GitHub.

The only case when a specific legal entity is named in a license, is in an exclusive grant of licensing rights to a specific entity. (used, for example, if a specific legal entity has purchased access to a private codebase - you would name them in a specific grant-of-license).

---
> from: [**iwanttobethelittlegirl**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395551007) on: **2018-6-7**

That violates freedom 0.
---
> from: [**bakkerme**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395563778) on: **2018-6-7**

I&#x27;d be watching for any Terms of Service for changes when MS takes over that forces you to give them a license to your code, regardless of your license. This line in particular:

&gt; This license does not grant GitHub the right to sell Your Content or otherwise distribute or use it outside of our provision of the Service.

It does seem very unlikely to me that they would change this, however. Unless they are to have a distinct Terms Of Service for enterprise users, it would effectively kill any trust that corporations have in GitHub and tank the business.

Regardless, you should consider AGPL as an alternative to any modified licenses. AGPL is much like the GPL, but with an extra stipulation that anyone making modifications to the licensed software and making it available over a network must also share that modified source. Contrast this to the GPL, where you only have to share the source if you are distributing the resulting binary.

This is an especially dangerous license to these massive corporations that provide online services, since it means they can&#x27;t make any changes to the code without having to publish those changes. Consider the legal hurdles that they have to jump through in order to do that, like getting the code signed off for not containing company secrets, and the various bureaucratic processes that corporations need to put together. They basically found a way around the spirit of free software, and AGPL is their kryponite. 

We already know [Google is extremely opposed to the license](https://opensource.google.com/docs/using/agpl-policy/), even having [AGPL code banned from Google Code](https://www.cnet.com/news/googles-festering-problem-with-the-agpl/) in the past. I&#x27;m not aware of Microsoft ever saying anything about it, but it&#x27;s fair that they would also have similar problem.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395575824) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
---
> from: [**ChrisCates**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395595528) on: **2018-6-8**

@iwanttobethelittlegirl It&#x27;s ironic how we are talking about violations of freedoms against corporations when we are discussing an oligarch known for suing people for exercising basic human rights.

Interesting isn&#x27;t it.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/111#issuecomment-395911672) on: **2018-6-9**

reminder this forum is shutting down in 5 hours 20 minutes. 
