# [It&#x27;s not embrace-extend-extinguish, it&#x27;s embrace-extend-monopolize-monetize](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/80)

> state: **open** opened by: **qwertychouskie** on: **2018-6-6**

People keep saying &quot;embrace-extend-extinguish&quot; in regards to the buyout, but I believe that is not right.  Why would they extinguish a platform they just payed $7.5billion for when they have no directly competing platform?  Look at e.g. Minecraft:

- Embrace:  MS buys Mojang
- Extend:  The &quot;Better Together Update&quot;, which lets e.g. console players play with Windows 10 edition players (see https://www.pcgamesn.com/minecraft/microsoft-minecraft-java-edition)
- Monopolize:  Sorry, the Java edition doesn&#x27;t get the update, use the Windows 10 edition instead.
- Monetize:  e.g. the &quot;Minecraft Marketplace&quot; (https://techcrunch.com/2017/04/10/minecraft-is-getting-a-marketplace-where-creators-can-monetize-their-creations/)

It&#x27;s embrace-extend-monopolize-monetize.  It&#x27;s just a matter of time before it happens to Github.

P.S.  Maybe an edited version of this should be added to the readme?

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/80#issuecomment-394902136) on: **2018-6-6**

i agree with your assessment. but let’s focus on providing options for people who want to leave. living our convictions is the best way to promote change. 
