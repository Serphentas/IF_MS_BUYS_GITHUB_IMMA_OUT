# [ replace machist meme with a feminist meme](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/51)

> state: **closed** opened by: **albjeremias** on: **2018-6-5**

yeah.. that.

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/51#issuecomment-394640679) on: **2018-6-5**

1. Nice try, buddy, trying to hijack the image link. &quot;society.oftrolls&quot;  Yes. Indeed.
2. &quot;feminist&quot;. BS. Most of my friends are women and feminist, and they&#x27;ll call bullshit on your simplistic patriarchal idea of &quot;feminist&quot;.

---
> from: [**albjeremias**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/51#issuecomment-394642880) on: **2018-6-5**

lol.. i have feminist friends.. but i am a machist... great...

whats the problem with society.oftrolls ? we are ethical trolls... that is a bullshit image...

you could at least request to change the hosting of the image... what the hell? o.O
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/51#issuecomment-394645653) on: **2018-6-5**

You were hijacking the link. In a pull request about replacing an image. Not cool. 

This is a serious project. Can you contribute something that advances it? We need the ethics without the trolling.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/51#issuecomment-394822416) on: **2018-6-5**

@albjeremias on the subject of sexism: [it turns out I was blind](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58#issuecomment-394811185) to the insensitivity if not sexism in that meme. I&#x27;m glad @mtwentyman called it out. I apologize for not seeing the truth in that part of your comment. 

But your pull request is still a hijack, and i&#x27;m not sure your replacement image helps or hurts fight sexism and male insensitive to women&#x27;s issues. I also don&#x27;t want to make this effort to be advertising for GitLab. Not everyone agrees it is the right alternative. As part of this project, we&#x27;ll discuss that and come up with some kind of recommendation.
---
> from: [**albjeremias**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/51#issuecomment-394962716) on: **2018-6-6**

ok, i apologize for taking over the link.. but it was the source... :o you had the source of the image in there before... i didn&#x27;t meant it...
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/51#issuecomment-394964733) on: **2018-6-6**

ok no worries. anyway, we&#x27;re not doing any meme any more. 
