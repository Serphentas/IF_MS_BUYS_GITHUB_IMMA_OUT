# [Long-term financial solvency ](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/6)

> state: **closed** opened by: **josephofiowa** on: **2018-6-4**

Allegedly, GitHub lost $66M in 2016. Business model changes for orgs and private repos since then imply trouble hitting profitability.

Moreover, [Developers, Developers, Developers](https://www.youtube.com/watch?v&#x3D;Vhh_GeBPOhs) 

### Comments

---
> from: [**vsbatista**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/6#issuecomment-394204500) on: **2018-6-4**

I’m not a ms diehard fan, but I think they did some nice moves recently. Xamarim, which was an expensive tool was acquired by them, and now is free for all. Dotnet is open source...Maybe I’m naive, but I believe Gh is going to have more ways (money) to be even more disruptive. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/6#issuecomment-394391293) on: **2018-6-4**

But this is not a forum for debate. This is a place for people who wanted an independent GitHub, and will leave if it isn&#x27;t. You don&#x27;t go into some Node project&#x27;s repo, for example, and start making comments on issues arguing that they should switch to Ruby.

The Hacker News discussions are, for example, a place to debate this. Or on Twitter. Not here.

Please be respectful of this.
---
> from: [**josephofiowa**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/6#issuecomment-394469965) on: **2018-6-4**

@vassudanagunta Makes sense. Thanks for alternative discussion recommendations.
