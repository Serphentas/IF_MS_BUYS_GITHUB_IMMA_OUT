# [Adding this image to explain our situation right now](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/103)

> state: **closed** opened by: **engyasin** on: **2018-6-6**

I think this image well convey an idea of the situation right now, so may add it to README (instead of the previous one)
actually I opened an account in GitLab and moved my repos there, but there&#x27;s still some work for me here in github..

![34448051_1833306990069957_4926335739144175616_n](https://user-images.githubusercontent.com/15736151/41064030-e57282b2-69c9-11e8-8bcf-3e79f5231d48.jpg)



### Comments

---
> from: [**unixfox**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/103#issuecomment-395223072) on: **2018-6-6**

Please consider posting this here: https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/38 instead of creating a new issue.
