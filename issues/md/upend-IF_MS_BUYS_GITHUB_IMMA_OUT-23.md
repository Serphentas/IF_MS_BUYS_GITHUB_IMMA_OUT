# [Provide best practices for a &quot;placeholder&quot; Github presence with forwarding address](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/23)

> state: **open** opened by: **vassudanagunta** on: **2018-6-4**

People who&#x27;ve been on GitHub for a long time may have a lot of external links to their GitHub profile and repos. 

For repos, for example, is there a way to delete all the code in the repo, replacing it with a forwarding address in the README, and do it in a way that doesn&#x27;t blow away downstream clones? 

### Comments

---
> from: [**rdebath**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/23#issuecomment-394621546) on: **2018-6-5**

Gitlab has &quot;push&quot; and &quot;pull&quot; from remote repositories. If your host doesn&#x27;t have that facility a &#x60;cron&#x60; script can do it too, with the advantage that you control how complete or out of date it is.

This allows you to continue to use github as a pure distribution system and funnel into your preferred host. With the automatic script allowing for trivial modification of the repository to, for instance, make the default branch on github &quot;special&quot; or only push maintenance branches to github.

Though, just changing the default branch on github would allow you to make the repo look like it just contains a README in the guthub GUI. eg: https://github.com/rdebath/test 

&#x60;&#x60;&#x60; sh
#!/bin/sh
set -e
cd /home/github/putty
git fetch origin
git push -q -f git@github.com:rdebath/PuTTY.git origin/master:refs/heads/master
&#x60;&#x60;&#x60;

Note: This script does not need a remote for github, the remote is being specified explicitly in the command line.

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/23#issuecomment-395576490) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
