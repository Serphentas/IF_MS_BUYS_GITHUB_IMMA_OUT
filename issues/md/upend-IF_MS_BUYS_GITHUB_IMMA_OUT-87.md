# [Github with focus to research and dev/sci community collaboration](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/87)

> state: **open** opened by: **bregydoc** on: **2018-6-6**

I think we need a powerful tool for democratizing our research and development work. Now is a good time to make on a community-based platform where we can collaborate on the scientific publication. I think we need to try to change the current mentality about platforms like Elsevier and  ScienceDirect.


### Comments

---
> from: [**cben**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/87#issuecomment-394985768) on: **2018-6-6**

Some existing links that come to mind:

- [Open Science Framework](https://osf.io/) are building a lot of tools for science collaboration.

- Math support:
  - [x] GitLab already supports math in Markdown and Asciidoc :clap:
  - [x] Bitbucket renders math in reStructuredText :clap: but [not in markdown](https://bitbucket.org/site/master/issues/7908/enable-mathjax-in-markdown-bb-9086)
  - [ ] GitHub [doesn&#x27;t render math](https://github.com/github/markup/issues/897) in text files nor comments. 
  On GitHub Pages, kramdown does recognize math syntax and it&#x27;s easy to add mathjax or katex.
- Jupyter notebooks (including math inside, of course):
  - [x] Rendered in GitLab
  - [x] Rendered in GitHub

- Open source LaTeX collaborative editing platforms:
  - ShareLaTeX was [mostly FOSS](https://github.com/sharelatex) though github sync part wasn&#x27;t.  Team joined Overleaf, they&#x27;re working on [joint v2 product](https://www.overleaf.com/blog/641-try-out-overleaf-v2) that I hope will be FOSS but not fully sure.  Note git sync in v2 is planned to only sync to GitHub (?)
  - [CoCalc](https://github.com/sagemathinc/cocalc) — does tons of things, including collaborative LaTeX editor, markdown with math editor, collaborative jupyter notebooks and more...
  - [BlueLaTeX](https://github.com/gnieh/bluelatex/issues/276)
---
> from: [**bregydoc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/87#issuecomment-395118020) on: **2018-6-6**

I didn&#x27;t know Open Science Framework, that&#x27;s great!. Thanks for shared it.

---
> from: [**tycho01**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/87#issuecomment-395191285) on: **2018-6-6**

&gt; scientific publication

publish to arxiv, browse with arxiv-sanity / arxiv-vanity / scirate?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/87#issuecomment-395578293) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
