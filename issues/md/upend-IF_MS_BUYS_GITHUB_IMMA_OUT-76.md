# [Updates are available: go to Windows Update to install the updates now.](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/76)

> state: **closed** opened by: **DylanLukes** on: **2018-6-6**



### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/76#issuecomment-394893477) on: **2018-6-6**

Haha. This is a serious mission we&#x27;re on, and as I&#x27;ve said on Twitter: 

&gt; This is not all about Microsoft. This is about the independence of what has become the de-facto home of open source. It shouldn&#x27;t be owned by any company that has any agenda other than host that home.

So let&#x27;s stick to solutions that help us find a new home or homes. 

But feel free to post your image on #38, which I consider a place to vent. There&#x27;s a lot of frustration so a space for that and some humor is needed. 
