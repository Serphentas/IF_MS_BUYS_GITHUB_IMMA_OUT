# [a heads up about project wikis for those headed to gitlab, and some thoughts about the situation](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/57)

> state: **open** opened by: **TeoTwawki** on: **2018-6-5**

I think gitlabs importer is not copying wiki&#x27;s at this time. I just had to manually clone and push a github wiki to a gitlab wiki. It may be this feature is missing or they may be under so much load that the importer missed it, but either way those of you with project wiki&#x27;s will want to make sure you didn&#x27;t forget your wiki before nuking your project off github.


Other thoughts: the users mass fleeing may kill github before MS ever gets the chance to even do anything bad. for anyone not familiar with the track record MS has with these things, I have an info-meme for you:

![ms knocks on githubs door](https://user-images.githubusercontent.com/6871475/40979694-0a725394-68c6-11e8-88ea-759a222306ce.png)

edit: I don&#x27;t know who the image originated with, I just captioned it.


### Comments

---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/57#issuecomment-394714649) on: **2018-6-5**

The second logo belongs to?

---
> from: [**MarounMaroun**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/57#issuecomment-394721317) on: **2018-6-5**

@ferittuncer [Mojang](https://mojang.com/).
---
> from: [**SamanthaJoy**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/57#issuecomment-394882240) on: **2018-6-6**

How was Mojang killed?
---
> from: [**TeoTwawki**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/57#issuecomment-394882552) on: **2018-6-6**

I&#x27;d hafta ask the guy who made the graphic, I wouldn&#x27;t call it dead but a lot of users think its damaged somehow, not a minecraft player couldn&#x27;t say.
---
> from: [**jowolf**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/57#issuecomment-395277391) on: **2018-6-7**

The row of doors isn&#x27;t nearly long enough.  This has been going on for 4 decades. 

A few notable examples from memory, without having to research or refer to the many books about Microsoft and/or Bill Gates:

## From the eighties: 

**Quarterdeck systems** - QEMM - (Quarterdeck Extended Memory Manager) a utility which made better / efficient use of DOS &quot;High&quot; memory above 640K. Microsoft cloned a vastly inferior version of the product, and bundled it for free with a newer version of DOS, thus killing the market for the product.

## From the nineties:

**Digital Research** - DR Dos - a superior DOS clone. Microsoft introduced a vaguely-worded dialog box, in it&#x27;s apps (eg, early MS Word) which it displayed when it figured out it was running on top of DR dos, which used simple FUD (without any actual faults) to scare people away from DR DOS.

The code to detect DR DOS, was encrypted and hidden, and was obviously written to do nothing but target a competitor - this was called the infamous AARD code, written about by Andrew Shulman and covered in countless publications including Dr Dobbs, etc

Yet no DOJ inquiry or any other action was taken, despite obviously breaking several laws against anticompetitive behavior, and then hiding it (shows intent).

## From the 90s /  2000s:

**Novell**, **Word Perfect** (Maybe MS was just after Provo, UTAH? JK) - Both these stories are more well known - 

**WordPerfect** was the only real competitor to the mediocre-at-best MS line of productivity apps (no incentive not to be mediocre, that way they can sell you a new copy every year or two of the product you already bought, and call it an &quot;Upgrade&quot;) - and MS killed the superior WordPerfect Write and Quattro Spreadsheet products with a weak &quot;Look-and-feel&quot; lawsuit - which they LOST, but not until the parent company was destroyed and unable to recover.  

**Novell** is a more involved story, but suffice it to say that they again, like QEMM, built a successful product on DOS&#x27;s shortcomings - which then simply acted like product feature vetting for MS, so they could build / buy or copy the solutions that Novell introduced, thus rendering Novell redundant (in the British sense).

## From the 2000s / 2010s:

**Borland** - Despite a vastly superior suite of developer products, MS poached it&#x27;s head developer and architect, Anders Heijlsberg, to be their head architect for .NET. His first task was to specifically copy several key innovations from Borland, which supposedly (according to MS) didn&#x27;t violate any copyright or IP agreements from the departing Borland - although this was controversial, again, lawsuits or the threat thereof were used effectively by MS to prevail.


## This Rant / Diatribe

Sorry, I got carried away - :-)

This was written on impulse, as a reaction to the door photo, completely off the top of my head in about 20 minutes, without so much as a quick Google to check dates, so forgive me if the above has minor issues to correct - I will update if I need to make corrections, but I do know from memory, that it&#x27;s essentially correct.

I remember these events over 3-4 decades, because of the MS&#x27;s consistently appalling and astounding behavior, that has continued unchecked, and remains to this day still unpunished and unaddressed, despite a slap on the wrist from our country&#x27;s DOJ, and from Europe, who&#x27;s various efforts to stop MS&#x27;s anticompetitive behavior were ineffective at best, and simply encouraged them to play brinkmanship at worst and improve their knowledge of the line, so they could push it.

I write this because MS&#x27;s behavior, as I slowly learned by experience of living though the above events, and the light slowly dawning on me that this was a pattern, and not just one or two coincidental events, is what motivated me to start my company, and try and have a higher bar for ethics and how to treat one&#x27;s competitors - and to encourage, foster, and sponsor more egalitarian software products and superior technical solutions in the process.

## Republish

Feel free to republish this, along with the reaper image (it doesn&#x27;t make sense without it), wherever you see fit.

Sincerely,
Joseph Wolff
Founder and CEO  
eRacks Open Source Systems
Founded 1999
eracks.com

We have our code on Github too. We&#x27;re looking for where we&#x27;re going to move.

Maybe we&#x27;ll self-host, or use our colo and hosting resources to host a new Git-like startup for Open Source?


---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/57#issuecomment-395578556) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
