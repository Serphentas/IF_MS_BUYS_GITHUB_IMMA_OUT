# [This is all my mistake. Time to reset.](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114)

> state: **open** opened by: **vassudanagunta** on: **2018-6-7**

I inadvertently embarked on [a fool&#x27;s errand](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395339838). @AnaRobynn&#x27;s [comment](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395043368) captures the conundrum perfectly. He wants (demands) all of the following:

- **results** *(YES!)*
- **no more Microsoft bashing** *(YES! I wanted to shut [this bashing](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/38) down but it would have required more censorship on my part, the very thing he opposes.)*
- **a free-speech forum**, allowing anyone to disrupt, jam with noise, or otherwise undermine the work, or to usurp this online meeting place for their own agenda *(NOT POSSIBLE if you want results and no bashing).* 
- **results within 4 days** *(even more NOT POSSIBLE if you want all of the above).*

Ironically @AnaRobynn&#x27;s participation exemplifies this. He contributed [8 words](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/44#issuecomment-395046092) towards &quot;significant results&quot; and [243 words](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/91#issuecomment-395043368) towards &quot;bashing&quot; this effort. 

As @bakkerme [wrote](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/106#issuecomment-395353066) (my emphasis):

&gt; Over the weekend I&#x27;m going to look at compiling some general resources about moving to other git hosting providers. **This repo has too much attention to be wasted on bickering in the issues.**

But again, this is all my fault. I should have known better. Instead I tried to compensate for the above contradictions by employing a heavy-hand on the delete and edit buttons. It was a misguided effort to nip in the bud disruptions and noise, to not let this turn into a platform for detractors, saboteurs, shills, to not let such behavior go without consequence, to not reward such behavior by letting their posts stay visible on a highly visible repo. Misguided because of appearance, not because it was wrong — the second panel of this XKCD (thanks @michaelpittino) nails it:

![Free Speech](https://imgs.xkcd.com/comics/free_speech.png)

I didn&#x27;t like doing it, but felt I had no choice, that it was the right thing. 

But I did have another choice. And I&#x27;m going to make it now. 

### the original intent 

I created this repo when the acquisition was only a rumor for people and projects who count on an independent GitHub (It&#x27;s right there in the repo name), to make them aware that the independence was under threat, and to enable them to voice their position via the star button in the slim hope that we might influence the decision.  

When rumor became reality, but the number of new stars kept accelerating, using the repo to host a solutions effort seemed to make sense. It all happened so fast, and I felt I had to make a quick decision, without doing the things I should have done first (like making the rules very clear).

**It was never meant to be a public forum** to debate the acquisition. But some people assume that just because there&#x27;s a text box with a &#x60;post&#x60; button, it implies a free speech public forum, regardless of what&#x27;s stated in the README or [in the Issue Template](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/blob/master/.github/ISSUE_TEMPLATE.md). 

I regret my quick decision now.

### git reset HEAD^

To get things back on track, I&#x27;ve decided to restore this repo back to that original intent. 

1. **Instead of hosting projects here, it will link to them.** 
   - **You all can self-organize** and create the kind of project or forum you want, with whatever rules you want *(You&#x27;ve always had this freedom)*.
   - **To those of you who have criticized how I&#x27;ve run things**: This is your chance to show me. I&#x27;ll expect you to accept all comers, including me, to let all posts stand undeleted, or whatever your expectations of me were. I can link to your project with the words &quot;Open forum, all opinions, including pro-Microsoft, welcome.&quot;
2. **Issues will be turned off June 8, 9pm Pacific Time, about 30 hours from this posting.** At that point they and comments will no longer be visible.
   - This should give you enough time to share contact information with those you want to team up with, and to export the work you have done here.
   - If anyone has good reason for needing more time, email me and we&#x27;ll work something out.
3. **I&#x27;ll post your project&#x27;s updates here**. Just email me. Or:
4. **I&#x27;ll accept patches to this repo itself** the pure git way. Without reliance on GitHub. The GitHub version of the repo will simply be a mirror of a repo not dependent on GitHub. This is also in keeping with our goals.

## why this is best

1. It is in keeping with the goals: Get off GitHub.
2. It aligns with a good message: Decentralize.
3. It eliminates the risk of deletion by Microsoft GitHub (You can&#x27;t &#x60;git pull origin&#x60; the issues database). The README will eventually get moved to upend.org as well.
4. Many of us had hopes that people would coalesce into teams and we’d rapidly have something to offer. This is the best way to achieve that.
5. Everyone gets what they want, though they don&#x27;t get it for free anymore. People get the freedom to work on a team with operating principles aligned with their desires (e.g. total free speech vs tight ship)
6. No more dictators (e.g. me). At least not a single dictator.
7. I get to stop deleting posts.
8. I get a break and a decent amount of sleep.

I&#x27;ll answer any questions or entertain other ideas you may have below. But it is unlikely I&#x27;ll change my mind.

If you want to debate free speech versus effective organization, I&#x27;ll be happy to do that. But not here. Feel free to invite me wherever you want to have it.





### Comments

---
> from: [**evyatarmeged**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395584130) on: **2018-6-8**

Seems legit. Let&#x27;s see where this goes.
---
> from: [**bakkerme**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395587330) on: **2018-6-8**

Wow, wasn&#x27;t expecting it to go nuclear. Sounds like a good chance to learn how to create and apply patch files. I&#x27;ll still be continuing with my plans, and I hope others who want to help out do so too.
---
> from: [**xloem**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395587982) on: **2018-6-8**

I’ve noticed that https://scuttlebutt.nz/ can be good for these situations.  In scuttlebutt, you inherit the streams of the people you follow, so noisemakers fade fast because nobody chooses to follow them.  git-ssb has been mentioned elsewhere.

Another option would be to invite trustworthy passionate community members to aid in managing the noise.  The most effective leaders are those who teach more teachers; they have a light workload but a large impact.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395590255) on: **2018-6-8**

Since the comparison of alternatives is being done [ħere](https://github.com/bderiso/Microsoft-Github-acquisition) and [here](https://docs.google.com/spreadsheets/d/1LZryaOx4VYHQ_oWLtRjvFrr4RhVjR484eFe7du3Usa4), I would suggest that people really interested in this movement to continue their discussions and debates over there (at least the technical part).

It would be too sad and shameful to lose all what&#x27;s been done up until now.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395591084) on: **2018-6-8**

@Serphentas Why would anything get lost? I&#x27;ll post links from here to individual efforts. Best of both worlds: You get to manage it how you want AND the visitors to this repo will be informed about it.

It will actually be better. Instead having to go to the Issues tab, visitors to this repo will see a list of projects with link, a description of the effort&#x27;s purpose, and its status (if you keep me updated). That is far better than visitors having to slog through all the noise in the Issues database. 
---
> from: [**xloem**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395593349) on: **2018-6-8**

I have created a post-github discussion room for networking at https://matrix.to/#/%23postgithub:disroot.org .

Projects like this one generally do get lost.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395630549) on: **2018-6-8**

Just learned we were [mentioned and linked to in a Wired article](https://www.wired.com/story/microsoft-github-code-moderation/). 
---
> from: [**neko**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395636840) on: **2018-6-8**

Thank you. This is the mature route.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395637343) on: **2018-6-8**

&gt; This is the mature route.

That&#x27;s a bit condescending @neko (You weren&#x27;t in my shoes. And do you know how old I am and how many projects I&#x27;ve run?), but that don&#x27;t matter. Show me what you accomplish in the project you lead 😉 
---
> from: [**Krotera**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395638723) on: **2018-6-8**

&gt;That&#x27;s a bit condescending @neko (You weren&#x27;t in my shoes. And do you know how old I am and how many projects I&#x27;ve run?), but that don&#x27;t matter. Show me what you accomplish in the project you lead wink

Thank you. This is the mature route.
---
> from: [**AnaRobynn**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395669074) on: **2018-6-8**

1) results (yes)
2) no more Microsoft bashing (yes/no)
The repo could have taken a more neutral stance on the subject, in stead of trying to create hype by spreading lies.
If people want to bash the repo in the issues go ahead. If people want to bash microsoft go ahead. Issues will be closed since they aren&#x27;t relevant.
3) See above, just close issues.
4) Not possible to have progress in 4 days? 
Yes is is possible and people have done it already: https://github.com/bderiso/Microsoft-Github-acquisition

5) I want to help the cause succeed, but I lost interest because how things are run here. So yes, I didn&#x27;t help the cause itself, but rather found it more valuable to give a perspective on why people don&#x27;t like the movement or lose interest in joining the discussions.

As a general tip, really take note of https://github.com/bderiso/Microsoft-Github-acquisition.
Their tone is a lot more nuanced and for someone having years of experience working in teams you really lack proper communication skills. 

You could have written this entire post in a less &quot;I know I&#x27;m right, but do whatever you like and you&#x27;ll see after that I was right along&quot;-tone. It really doesn&#x27;t help with the credibility.

Anyway, I&#x27;m glad, even though I kinda feels like a shitpost towards me, that my comment moved something and that the repo will become a haven for other repositories (since this one already has the exposure)
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395677048) on: **2018-6-8**

Your comment moved something? Wow, some ego. I made these changes because of feedback from other people, and because we all are not of one mind, and that&#x27;s far more damaging to the cause than my deleting the comments of &quot;shitposters&quot; to use your favorite term. 

I used your comment because it expressed the contradictions in the demands being made on me perfectly. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395677735) on: **2018-6-8**

And yeah, to all of you giving me the thumbs down all over this repo and no doubt on this comment too, I&#x27;m tired and worn out, because all I&#x27;ve been getting is shit for trying my best. 

This is from the project @AnaRobynn praises:

&gt; Examples of behavior that contributes to creating a positive environment include:
&gt; - Showing empathy towards other community members

&gt; Examples of unacceptable behavior by participants include:
&gt; - Trolling, insulting/derogatory comments, and personal or political attacks

&gt; Project maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, or to ban temporarily or permanently any contributor for other behaviors that they deem inappropriate, threatening, offensive, or harmful.


The only reason I haven&#x27;t deleted this repo is because i feel responsibility towards the 4000+ people who&#x27;ve starred it, and the thousands of visits it continues to get every day.

If you guys share any of that sense of responsibility, you&#x27;ll continue your work regardless of how much you hate me, and I&#x27;ll post links to your work regardless of how much I feel shit on.

---
> from: [**bakkerme**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395682030) on: **2018-6-8**

Vassudanagunta, you&#x27;re definitely going through a great deal trying to keep this group under control and your effort is appreciated, but please take it easy. Tensions are high from the acquisition, and understandably so.
If you need any help in establishing the future of the project, please let me know and I&#x27;ll do what I can to help keep things going.
---
> from: [**ChrisCates**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395690510) on: **2018-6-8**

@Addvilz, I would be wary of making such an assumption without knowing all of the things Microsoft has been involved in the past, and what they may potentially be involved in now.

To say the least, it is like implying Bill Gates&#x27; humanitarian gimmicks should not be taken facetiously for his techno-feudalist desires.
---
> from: [**Droppers**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395691515) on: **2018-6-8**

Why don&#x27;t you start by documenting alternatives with their pros and cons?
---
> from: [**ChrisCates**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395692005) on: **2018-6-8**

@Droppers the problem does not rely on pros and cons of new solutions.

The problem relies on the fact that millions of developers have trusted their code with Github and now it is the property of Microsoft.

The implications of this are large, for good, or for bad.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395696508) on: **2018-6-8**

Thanks @bakkerme. Very much. You&#x27;ve been kind and supportive. I&#x27;m going to hit you up on that.

One more thing and I&#x27;ll be done with my ranting and raving. On the subject of &quot;Showing empathy towards other community members&quot;, what I care far more about than this GitHub-Microsoft shit are things like racism and sexism. Sexism is especially bad in the tech community because it is so male dominated.

When @mtwentyman pointed out (#58) the possible insensitivity of an image I created to add some humor to this endeavor, I took it seriously (I actually dropped out of tech to work on social justice issues) and asked 9 women I respect what they thought. All but one of them said that it was insensitive to women&#x27;s pain if not sexist. They didn&#x27;t explain, because they know me and knew I knew the answer and just needed to open my eyes more. I opened my heart and then it became clear. So I [posted a mea culpa](https://twitter.com/UpEnd_org/status/1004098207494877185) on Twitter, where the original image has been the most popular thing I&#x27;ve posted and drawn lots of visits to this repo. Guess what. Crickets. The only replies are from men mansplaining why it isn&#x27;t sexist, or posting a gif of Trump saying &quot;Wrong!&quot;. That makes me sad. Far more than sad. And the hearts and the retweets for the original image keep coming in. And look at all the thumbs down @mtwentyman got. He&#x27;s gotten even more thumbs down than me. Look at that whole thread (#58). I&#x27;m pretty disgusted by it. Few came to his defense. Everyone would rather grandstand about free speech than use that free speech against sexism. Maybe getting lots of thumbs down in this community is a good sign. 

I&#x27;d rather go back to working on social issues and working with underserved children than in this entitled self-important, over-privileged, insensitive male dominated tech industry. If I do continue with the UpEnd project, it will be with a team of people who share the values I have, which are harshly critical of the culture that focuses on wealth and turns a blind eye on those left behind, because, you know, America and especially the tech world is a so-called-meritocracy, i.e. if you&#x27;re poor or oppressed it&#x27;s your own fucking fault.

I care about upending all of the over-dominant and oppressive power structures, be it the tech oligarchy of companies like Microsoft, or patriarchy or white supremacy. I just wanted to make that clear. 

Ok, i&#x27;m done. I got it all out. No more from me.
---
> from: [**ChrisCates**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395698423) on: **2018-6-8**

@vassudanagunta These are not relevant topics. We are talking about a techno-feudalist overlord looking to grab as much power as it can and is trying to suffocate free software... again...

Also, despite the irrelevant statements, I will interject... But I suggest keeping things on topic.

In regards to your feelings towards feminism, you are focusing on microaggressions. A lot of the world does not have a basic understanding of proto-feminism. This is a far greater issue than the one you posed. Instead of worrying about people&#x27;s reactions to a meme... Perhaps you should look into reforming countries with Sharia Law. Where it is not uncommon for females to be treated as slaves.
---
> from: [**ChrisCates**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395699075) on: **2018-6-8**

I suggest instead of policing yourself (and others) on such minor incidents to reflect on what is happening at a macro scale. The number of injustices that occur daily is tantamount. To worry about a meme is pitiful when there are others in the world who are being stoned to death and oppressed.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395699102) on: **2018-6-8**

Yes, rather than look in the mirror and correct our own behavior, let&#x27;s deflect by pointing fingers at people that are worse than us. 
---
> from: [**ChrisCates**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395699230) on: **2018-6-8**

You are literally policing yourself (and others) on a meme...
---
> from: [**ChrisCates**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395699660) on: **2018-6-8**

I suggest focusing on getting The Western World caught up to speed on proto-feminism before pursuing such &quot;problems&quot;. Not only is this kind of behavior unproductive, it counters the values you wish to achieve in society.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395699755) on: **2018-6-8**

&gt; You are literally policing yourself 

And that&#x27;s what we should all be doing. People have to live their convictions. Our society has problems because everyone makes excuses. 

And this is on topic. If you read the end of that mea culpa I explain that.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395703165) on: **2018-6-8**

@Addvilz you have no business being here. You said so yourself as you defended sexism in that thread:

&gt; even considering I do not agree with the premise of this repository in general. If you feel personally offended by something, it is your personal responsibility, not anyone elses.

Yeah, women, people of color... they shouldn&#x27;t be offended. It&#x27;s their personal responsibility. The white male bubble of the tech world at its best.  
---
> from: [**ChrisCates**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395704226) on: **2018-6-8**

@vassudanagunta you are literally enforcing postmodern feminist views when a lot of people in Western Society do not have a fundamental understanding of the basic principles of feminism.

You are the same archetype as the females who want to enforce laws where it is illegal to look at other people... It&#x27;s like you&#x27;re okay with Sharia Law veiled in a Post Modern Feminist context.

https://www.youtube.com/watch?v&#x3D;3KSJY0c8QWw

Isn&#x27;t it ironic what happens to countries controlled by oligarchs with Post Modernist views?
You got to love Snopes trying to hide these facts too:

https://www.snopes.com/news/2017/06/19/what-is-sharia-law/

&quot;As with so many aspects of Islam, some non-Muslims criticize &quot;Sharia law&quot; without really knowing the first thing about it.&quot;

https://www.snopes.com/fact-check/crime-sweden-rape-capital-europe/

Yet they claim Sweden is not the rape capital... Hmm... Interesting considering their narrative on Sharia Law isn&#x27;t it. What were to happen if Snopes emails were to be leaked? Interesting to say the least wouldn&#x27;t it be, right?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395714932) on: **2018-6-8**

I&#x27;m hiding the totally off topic and ridiculous exchange between me and @ChrisCates, to spare you all. I&#x27;m keeping my rant because it&#x27;s relevant to the philosophical misalignment behind a lot of our strife, and frankly, it&#x27;s everyone against me and i&#x27;ve worked so hard on this shit and i&#x27;m just going to grant that pass for me. All these comments are gone in 18 hours anyway. 

One more time: no more from me unless it&#x27;s a question about the OP.
---
> from: [**Giancarlos**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395725793) on: **2018-6-8**

I&#x27;m under the heavy impression the &quot;trending&quot; repositories and developers are based on traffic at the time, which explains why the CEO of Microsoft was trending with 0 repositories or any user content on his profile. People are just looking up everyone involved with Microsoft. I don&#x27;t think it&#x27;s censorship, lots of people came here from HN / 4chan / clown porn sites / whatever and then after a day that attention died down. Which makes the most sense to me. I highly doubt GitHub cares to do censorship on this, it&#x27;s never truly done that before. Some repositories are just only popular at the moment they receive a ton of attention.

If there was more to do here than just laugh and comment, and people kept coming back daily, I guarantee it will trend for a week or longer. But that&#x27;s not the purpose of this repository (to be active on GitHub) it&#x27;s to be inactive / deactivate your GitHub. I honestly sorta wish a GitHub staff member would confirm this weird behaviour.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395818874) on: **2018-6-8**

@Giancarlos feel free to repost your theory under #88, but only after you explain the data I posted there.
---
> from: [**williamknauss**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/114#issuecomment-395882336) on: **2018-6-8**

Hi @vassudanagunta - Hey mate, I would appreciate a link to my open forum https://github.com/WateringBooth/Refreshment which has a goal to educate people on the many options, and availabilities of git. 
