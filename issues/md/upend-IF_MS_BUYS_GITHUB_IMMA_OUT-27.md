# [OUT](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/27)

> state: **closed** opened by: **Frangte** on: **2018-6-4**

The purpose of this repo is to give a voice to those who wanted an independent GitHub, not one owned by any company with any business or agenda outside of serving the open source community. It is a place for such people to collaborate on options, given that GitHub is no longer independent.

If you are not such a person, this place isn&#x27;t a forum for you.

It is NOT a place to debate the acquisition or the opinions of those opposed to it. It is not a place for you to try to convert people. It is certainly not a place to patronize, troll or otherwise be rude. I will enforce this without hesitation.

Please respect our wishes. You are welcome to make whatever argument you want on UpEnd&#x27;s twitter (link in the README), if you really need to.


### Comments

---
> from: [**bashrc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/27#issuecomment-394420967) on: **2018-6-4**

Microsoft has shills. It&#x27;s a long and dishonorable tradition at that company.
