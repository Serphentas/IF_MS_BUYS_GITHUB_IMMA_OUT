# [Sending the right message](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/24)

> state: **closed** opened by: **immexerxez** on: **2018-6-4**

At first when I heard the news I was devastated and figured this would be the end of the Github I know and love but in all reality it isn&#x27;t **yet**. Github&#x27;s strength was never that it was free or that it was the big dog in open source. It was because everyone came here Huge corporations **and** regular people  contributed, and it remained/appeared to be impartial. In my earlier comments on a different issue I said Microsoft can&#x27;t be trusted and we inevitably will lose but that is the **wrong** attitude. **We aren&#x27;t refugees**. You can&#x27;t just flee when you don&#x27;t like something and we don&#x27;t even know yet what the outcome will be. It&#x27;s really easy to throw your hands up and run away. Everyone should stay, make their voices heard and if that doesn&#x27;t work and Microsoft does ruin Github, leave. 


### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/24#issuecomment-394424060) on: **2018-6-4**

You are not a refugee. But I am. 

From the outset I made this about GitHub and it&#x27;s *independence*. I *never* made this about Microsoft, regardless of my opinion of them. 

This is the phrase on the upend GitHub organization page: &#x60;Upend the web oligarchy. #resist&#x60;

This is the text on the upend.org homepage:

&#x60;&#x60;&#x60;
upend.org

a future rebel base of the web resistance.

take back the web. take back our world.

#resist
&#x60;&#x60;&#x60;

[And on Twitter](https://twitter.com/UpEnd_org), through which I&#x27;ve been driving a lot of traffic to this repo (and getting little sleep), I&#x27;ve made the message very clear. Check out the posts there. People have been coming here after reading those posts.

This is about principles. It&#x27;s about the open source ethos. It&#x27;s about the need for independence **especially** for something like the home of the open source community. This is about resisting the concentration of power, as well as money being the arbiter of everything.

I can go on, and I will as I develop upend.org.

So, for people like me, this was my home, and it was just taken over by coup-de-tat. It is no longer my home because of that. So I am a refugee. 

&gt; You can&#x27;t just flee when you don&#x27;t like something and we don&#x27;t even know yet what the outcome will be. 

That&#x27;s like telling someone after some big country has taken over your little one: *You can&#x27;t just flee. We don&#x27;t even know yet the outcome. Maybe they they will be good rulers*. 

You&#x27;re opinion is perfectly valid. But movements have to have some coherency of vision. A vision some one or some few present. And people join based on whether they align with that vision. I&#x27;m sorry if you got this one wrong. Maybe this isn&#x27;t the place for you. I mean that quite genuinely and with all the best intent. Follow your heart. ❤️ 

---
> from: [**bashrc**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/24#issuecomment-394426012) on: **2018-6-4**

I&#x27;m not going to beg Microsoft for anything.

The only question I&#x27;m having right now is whether to close my account now or give it a few days. If you take a look at my profile you&#x27;ll notice that my activity on Github over the last five years has not exactly been trivial. But for me Microsoft is a red line. I had no objection to them being a user on Github like many others, but them being an intermediary between me and my repos ain&#x27;t gonna happen.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/24#issuecomment-395581871) on: **2018-6-8**

Hey all. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
