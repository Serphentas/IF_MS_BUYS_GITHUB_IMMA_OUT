# [Is GitLab a good options?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9)

> state: **open** opened by: **dou4cc** on: **2018-6-4**

https://twitter.com/gitlab/status/1003027830437892096

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394210119) on: **2018-6-4**

Cool. I think it would be for the community to discuss what the best alternative is. Maybe it&#x27;s GitLab, maybe it&#x27;s not. See [this comment](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/8#issuecomment-394209227).
---
> from: [**qeesung**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394211946) on: **2018-6-4**

Gitlab may accidentally delete your repository. 😂 
---
> from: [**fantasticfears**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394303219) on: **2018-6-4**

Gitlab.com is actually on Azure. Go for a traceroute
---
> from: [**austinhuang0131**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394409579) on: **2018-6-4**

[GitLab is moving to Google Cloud?](https://venturebeat.com/2018/04/06/why-and-how-gitlab-abandoned-microsoft-azure-for-google-cloud/)

Also what about BitBucket?
---
> from: [**forforeach**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394412253) on: **2018-6-4**

Very strange to see people bringing private repos as a big pro, when arguing that GitHub should stay independent home for open source
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394417773) on: **2018-6-4**

I added a &quot;?&quot; to the title, as this is a suggestion, not a recommendation with a backing pro-con case. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394440342) on: **2018-6-4**

Subsumed by #32. This thread will be used a a resource. 
---
> from: [**darkpixel**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394442967) on: **2018-6-4**

@forforeach &quot;Very strange to see people bringing private repos as a big pro, when arguing that GitHub should stay independent home for open source&quot;

I use private repos for hosting network documentation for companies and sharing it between contractors.  What&#x27;s wrong with that?
---
> from: [**forforeach**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394444218) on: **2018-6-4**

@darkpixel nothing is wrong. A lot of people use private repos including myself. But it is not an open source code. That&#x27;s it. And here, as I can see, people try to promote independent open source platform and hand by hand mention private repos as a big advantage
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394444656) on: **2018-6-4**

@forforeach, he used the word &quot;too&quot;. There&#x27;s nothing contradictory about doing private stuff and doing open source, and wanting the convenience of doing that in the same place. Me thinks you just want to make your argument no matter what people are actually saying.
---
> from: [**forforeach**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394446434) on: **2018-6-4**

@vassudanagunta don&#x27;t be so aggressive. If you disagree with my opinion, it&#x27;s ok with me. In my opinion it looked strange. I didn&#x27;t try to argue with anyone or convince that I&#x27;m right. He asked, I answered. So blaming me that I&#x27;m trying to make my argument no matter what, is at least wrong

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394567100) on: **2018-6-5**

@forforeach I&#x27;ve slept 11-13 since friday morning, i&#x27;m trying to start a movement, i have to heard cats, i&#x27;ve been pushing the visibility of this repo on twitter, and i have to deal with trolls and people who take offense at this movement and feel entitled to barge in here and mock us, and trying to coax people into being respectful to each other. Your tone came off as disrespectful / dismissive. If I was wrong, sorry. Can you get me some sleep?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394574295) on: **2018-6-5**

Probably some good comments in this thread on GitLab: https://news.ycombinator.com/item?id&#x3D;17229940
---
> from: [**itsnotvalid**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394622349) on: **2018-6-5**

GitLab still opensources its code and it offers exporting the whole project (not just the repos), IMHO this seems to be a good option for self-hosted, free or paid service.
---
> from: [**angela-d**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394847569) on: **2018-6-5**

If Gitlab is moving to Google Cloud, is it really any better?  Google invested $20 mil to Gitlab&#x27;s C funding: https://www.kyivpost.com/technology/google-fund-invests-20-million-gitlab-startup-ukrainian-cofounder.html

Next step is their [IPO](https://about.gitlab.com/strategy/) where one can assume Google will take a huge chunk of ownership.

Currently, Gitlab.com resolves to a Microsoft IP, so it must still be on Azure.
&#x60;&#x60;&#x60;txt
PING gitlab.com (52.167.219.168) 56(84) bytes of data.
...
NetName:        MSFT
NetHandle:      NET-52-145-0-0-1
Parent:         NET52 (NET-52-0-0-0-0)
NetType:        Direct Assignment
OriginAS:       
Organization:   Microsoft Corporation (MSFT)
RegDate:        2015-11-24
Updated:        2015-11-24
Ref:            https://whois.arin.net/rest/net/NET-52-145-0-0-1
...
&#x60;&#x60;&#x60;
---
> from: [**simonschaufi**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-394870228) on: **2018-6-5**

In my opinion the biggest advantage of gitlab are the issue templates. Github only supports 1 while gitlab supports several that you can choose while creating a new issue. The same goes for a merge request templates. ❤️ 

Oh and the integration of docker images for CI is also a BIG plus.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/9#issuecomment-395576643) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
