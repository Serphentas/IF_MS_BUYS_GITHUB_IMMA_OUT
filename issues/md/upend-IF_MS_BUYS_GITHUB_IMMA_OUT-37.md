# [Preserving access to starred repositories](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/37)

> state: **open** opened by: **Serphentas** on: **2018-6-4**

While some repositories/organizations may leave GitHub for some other code hosting platform, some may not necessarily do so. Some of the remaining content may be of great value (ex. [ZoL](https://github.com/zfsonlinux/zfs), [Flask](https://github.com/pallets/flask), [SIDtoday](https://github.com/firstlookmedia/sidtoday), etc.).

How should we handle such cases ? Do we copy the existing repositories in their current state elsewhere or do we keep coming back to GitHub for new commits ?

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/37#issuecomment-394470713) on: **2018-6-4**

This nice thing about Git is that it is a distributed system. You can read (&#x60;git fetch&#x60;) from any GitHub repo without an account anytime. If you need to contribute, you have to make the call. This doesn&#x27;t have to be black and white. People can pull out of GitHub as much as they can. Just that alone will have an impact. As long as people are honest and not lazy. #resistance takes work and sacrifice!

Your point is a very important one that should be addressed by the guides we will write. Thank you? I hope you can help.


---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/37#issuecomment-394648910) on: **2018-6-5**

I am willing to help, indeed, as moving out of GitHub implies handling many details such as this one.

Right now I haven&#x27;t got much time due to work IRL but I&#x27;ll try to make something. Are there any guidelines ?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/37#issuecomment-395576514) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
