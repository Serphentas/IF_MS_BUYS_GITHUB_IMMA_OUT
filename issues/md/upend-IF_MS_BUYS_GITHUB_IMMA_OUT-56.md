# [initial written proposal](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/56)

> state: **closed** opened by: **lzycindy** on: **2018-6-5**

initial written proposal

From the README:

&gt; But some believe deeply that the open source community needs an independent home. Some believe that the concentration of so much power in the hands of so few is antithetical not just to free and open software, but also to a free and just society. We, the contributors and stargazers of this project are such people. This project is for us.

If the above does not describe you, if this project is not for you, or if you disagree with it, please do not post here.

This is NOT a forum to debate the acquisition or the opinions of those opposed to it. It is not a place for you to try to convert people. It is certainly not a place to patronize, troll or otherwise be rude. I will enforce this without hesitation.

Please respect our wishes in our house.



### Comments

