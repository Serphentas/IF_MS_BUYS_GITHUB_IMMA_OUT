# [What if MicroSoft removes this repo ?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/92)

> state: **open** opened by: **AEK-BKF** on: **2018-6-6**

Do we have an other place to meet there and decide what we should do ? 

Let&#x27;s see ...

### Comments

---
> from: [**AnaRobynn**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/92#issuecomment-395044578) on: **2018-6-6**

I think Microsoft is smarter than shutting down this repository. People have already forked as well so, I doubt it.

---
> from: [**limonte**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/92#issuecomment-395045204) on: **2018-6-6**

&gt; I think Microsoft is smarter than shutting down this repository. 

This is a very optimistic assumption. 

&gt; People have already forked as well so, I doubt it.

Forks don&#x27;t have all tickets and conversations, which are the greatest value of this repository.
---
> from: [**moorchegue**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/92#issuecomment-395335636) on: **2018-6-7**

Is IRC / XMPP chatroom considered a good option?
---
> from: [**fn-main**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/92#issuecomment-395465065) on: **2018-6-7**

Possibly Discord?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/92#issuecomment-395580447) on: **2018-6-8**

It&#x27;s a real risk. All the Issues and comments would be gone. See #114 for the solution.
