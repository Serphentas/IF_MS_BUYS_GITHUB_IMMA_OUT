# [Add link to change.org petition](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/3)

> state: **closed** opened by: **xkr47** on: **2018-6-4**

Add link to: https://www.change.org/p/microsoft-stop-microsoft-from-buying-github

### Comments

---
> from: [**tay10r**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/3#issuecomment-394195850) on: **2018-6-4**

Looks like they acquired it already and are making the announcement on Monday.

https://www.bloomberg.com/news/articles/2018-06-03/microsoft-is-said-to-have-agreed-to-acquire-coding-site-github
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/3#issuecomment-394195852) on: **2018-6-4**

Hi @xkr47. I&#x27;d normally say a change.org petition is a good idea. But the best place to sign a petition to send a message to GitHub is right here on GitHub. Each star on this repo represents the signature of actual users on this account. What can be better than that?

I think it&#x27;s better to focus efforts on getting the stars on this into the thousands and thousands. 

The good news is that this repo, created just yesterday, is now a top 10 trending repo on GitHub. Let&#x27;s keep it up!
