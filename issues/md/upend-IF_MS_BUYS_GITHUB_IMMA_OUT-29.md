# [Use #DeleteGithub to Label Your Migrated Projects](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/29)

> state: **open** opened by: **biergaizi** on: **2018-6-4**

As many people have decided to migrate their projects to other platforms, why not to label the abandoned GitHub project as &quot;#DeleteGithub&quot;?

If we use #DeleteGithub tag to label our migrated projects, and encourage others to do the same, we can easily see how many projects have chosen to migrate by searching this tag. [Like this](https://github.com/search?q&#x3D;topic%3Adeletegithub&amp;type&#x3D;Repositories).

We can make the transition as smooth as possible. You don&#x27;t have to actually delete the repo and you don&#x27;t have to break things for others, you can just

    git branch deleted
    git checkout deleted
    git rm -rf *
    echo &#x27;#DeleteGithub&#x27; &gt; README.md
    echo &#x27;&#x3D;&#x3D;&#x3D;&#x3D;&#x3D;&#x27; &gt;&gt; README.md
    echo &#x27;**Deleted!** This project is moved to [notabug.org](https://notabug.org/example/example), welcome to join the free world!&#x27; &gt;&gt; README.md
    git add README.md
    git commit -a -m &quot;Moved, #DeleteGithub!&quot;
    git push --set-upstream origin deleted

Then, click &quot;Settings&quot; -&gt; &quot;Branches&quot; -&gt; make &quot;deleted&quot; as Default branch. Finally add the tag &quot;#deletegithub&quot; to your project.

If you found it&#x27;s a good idea, you may add this suggestion to your repo.
 

### Comments

---
> from: [**lanodan**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/29#issuecomment-394670863) on: **2018-6-5**

&gt; Also, please if you do move your code away from Github:
Be sure that your package maintainers are aware of this change, for example by filling an issue.

Haelwenn /ɛlwən/ — https://social.hacktivis.me/objects/a38b05b2-dc86-4f8d-8664-41d3f2669f7f

This technique of yours doesn’t make a notification, and you’ll likely delete your account soon. Potentially breaking the release checks of package maintainers and without having any kind of information after that if you don’t have a proper homepage or channel.
---
> from: [**biergaizi**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/29#issuecomment-394679481) on: **2018-6-5**

I&#x27;m not deleting my account, because of the potential breakage it may create, and it is why I purposed above procedure for migration - keep the master branch as-is to avoid breaking existing systems and focus on future releases.

You have a good point. Silently breaking automatic release checks relied by package maintainers is a serious issue. I guess for a influential project, converting the GitHub repo to a pure mirror may be a better solution.
---
> from: [**0100001001000010**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/29#issuecomment-394777756) on: **2018-6-5**

I put #DeleteGithub on my profile, but I am not deleting my account in case I find a project I want to contribute to that is still hosted on GitHub.
---
> from: [**biergaizi**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/29#issuecomment-394991048) on: **2018-6-6**

@lanodan Perhaps intentionally creating a non-working version tag may help solving this problem. It&#x27;s not the nicest thing to do, it *almost* sounds like doing something malicious (while it&#x27;s still not the case), but it does trigger a new version alert, and likely to cause the build process to fail, in this process the maintainers are notified.
---
> from: [**lanodan**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/29#issuecomment-395024921) on: **2018-6-6**

@biergaizi 
Makes me think that “destroying” the git repo probably isn’t a good thing either for things based on git (archlinux &#x60;&#x60;${pkgname}-git&#x60;&#x60; and gentoo’s &#x60;&#x60;${pkgname}-…9999&#x60;&#x60; packages).

And yeah, creating a fake release (and so tag) is probably a bad idea for stuff like that too. (I have ones using the nearest tag to the HEAD in the tree because there is too much releases)
---
> from: [**DavidGriffith**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/29#issuecomment-395039153) on: **2018-6-6**

There are a goodly number of repos using this label that are pointing to new repos on Gitlab that aren&#x27;t marked public.  You need to explicitly do that.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/29#issuecomment-395577818) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
