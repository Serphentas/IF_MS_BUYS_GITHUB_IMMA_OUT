# [GitHub alternatives for shared web hosts](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98)

> state: **open** opened by: **Apollia** on: **2018-6-6**

What [free (as in freedom)](https://www.gnu.org/philosophy/free-sw.en.html), [libre](https://en.wikipedia.org/wiki/Free_software), [open source](https://www.gnu.org/philosophy/words-to-avoid.en.html#Open) GitHub alternatives might work best on shared web hosts, or could most easily be modified to work well on a shared web host?

&lt;br&gt;
I haven&#x27;t tried any yet, so I&#x27;m not sure how well these will work, but these 3 seem like possibilities:

&lt;br&gt;* **GitList**, written in PHP: http://gitlist.org/
&lt;br&gt;https://github.com/klaussilveira/gitlist

&lt;br&gt;* **GitPrep**, written in Perl: http://gitprep.yukikimoto.com/
&lt;br&gt;https://github.com/yuki-kimoto/gitprep


&lt;br&gt;* **Phabricator**, written in PHP - https://phacility.com/phabricator/
&lt;br&gt;https://github.com/phacility/phabricator

&lt;br&gt;However, the [Phabricator Installation Guide](https://secure.phabricator.com/book/phabricator/article/installation_guide/) says:

&gt; &quot;A Shared Host: This may work, but is not recommended. Many shared hosting environments have restrictions which prevent some of Phabricator&#x27;s features from working. Consider using a normal computer instead. We do not support shared hosts.&quot;

Still, I&#x27;m mentioning it because perhaps it might be possible to make a version of Phabricator that would definitely work well on a shared web host.

----

I&#x27;m also interested in anything that could possibly be useful in building a GitHub alternative for shared web hosts.  Like this:

&lt;br&gt;* **Gitter**, written in PHP - 
https://github.com/klaussilveira/gitter

Quote:
&gt;&quot;Gitter allows you to interact in an object oriented manner with Git repositories via PHP.&quot;


&lt;br&gt;
Thanks for any ideas!

### Comments

---
> from: [**chrismrutherford**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98#issuecomment-395119617) on: **2018-6-6**

- **GitBlit**, written in Java -
http://gitblit.com 

I have been using this for a while now.   
---
> from: [**pihao**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98#issuecomment-395126021) on: **2018-6-6**

- __Gogs__, written in Go - https://gogs.io
- __Gitea__, written in Go - https://gitea.io
---
> from: [**angela-d**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98#issuecomment-395186738) on: **2018-6-6**

A bit off-topic, but what shared host do you guys use that allow git?
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98#issuecomment-395189212) on: **2018-6-6**

Please consider posting your suggestions in [this spreadsheet](https://docs.google.com/spreadsheets/d/1LZryaOx4VYHQ_oWLtRjvFrr4RhVjR484eFe7du3Usa4) as per [this issue](https://github.com/bderiso/Microsoft-Github-acquisition/issues/7) and [this one too](#25) and close this issue in order to keep things centralized and issues count low. Thanks.
---
> from: [**Apollia**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98#issuecomment-395199570) on: **2018-6-6**

Thanks all!

&lt;br&gt;

&gt; A bit off-topic, 

No problem!

&gt; but what shared host do you guys use that allow git?

I use DreamHost: https://help.dreamhost.com/hc/en-us/articles/216445317-How-do-I-set-up-a-Git-repository-

&lt;br&gt;

&gt; Please consider posting your suggestions in [this spreadsheet](https://docs.google.com/spreadsheets/d/1LZryaOx4VYHQ_oWLtRjvFrr4RhVjR484eFe7du3Usa4)

Thanks for that great spreadsheet!  I&#x27;ll check out all those websites hopefully soon.

I&#x27;m afraid I might accidentally mess up the spreadsheet somehow if I edit it myself, so, anyone who wants to can add my suggestions.

[...]

&gt;and close this issue in order to keep things centralized and issues count low. Thanks.

Please correct me if I&#x27;m wrong, but, I think this issue&#x27;s topic might be a bit different and more narrowly-focused than the spreadsheet.

This issue is about software which you can run on your own shared web host, rather than someone else&#x27;s server which might qualify as a &quot;Service as a Software Substitute&quot; (SaaSS) - https://www.gnu.org/philosophy/who-does-that-server-really-serve.en.html

So, I&#x27;m in favor of leaving this issue open.

Thanks again to you all!
---
> from: [**ernierasta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98#issuecomment-395459118) on: **2018-6-7**

[gitea.io](https://gitea.io) FTW!
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98#issuecomment-395502909) on: **2018-6-7**

@Apollia 

&gt;I&#x27;m afraid I might accidentally mess up the spreadsheet somehow if I edit it myself, so, anyone who wants to can add my suggestions.

You can comment on the spreadsheet if you prefer not to edit directly, so that those with write access can merge your changes.

&gt;Please correct me if I&#x27;m wrong, but, I think this issue&#x27;s topic might be a bit different and more narrowly-focused than the spreadsheet.

It&#x27;s really up to you. If you think a debate has to take place, then sure keep this issue open.

@ernierasta Already referenced in the spreadsheet.

---
> from: [**Apollia**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98#issuecomment-395522594) on: **2018-6-7**

Thanks again all!  Also, repeated suggestions are fine - it helps provide an idea of what software is the most popular.

Quote:

&gt; You can comment on the spreadsheet if you prefer not to edit directly, so that those with write access can merge your changes.

I tried, but Google Docs seems slow and glitchy in my browser.  Sorry!

Quote:

&gt; It&#x27;s really up to you. If you think a debate has to take place, then sure keep this issue open.

I&#x27;m leaving it open, but not because I think a debate has to take place.  I see no reason why the spreadsheet and this issue can&#x27;t coexist in peace.

Thanks again!
---
> from: [**Apollia**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98#issuecomment-395530797) on: **2018-6-7**

Might be a useful component of a GitHub alternative for a shared web host:

&lt;br&gt;* **Gitolite**, written in Perl - http://gitolite.com/

https://github.com/sitaramc/gitolite

&lt;br&gt;Quotes from the [Gitolite Overview](http://gitolite.com/gitolite/overview/) page:

&gt; &quot;Gitolite is an access control layer on top of git.&quot;
&gt; 
&gt; &quot;Can be installed without root access, assuming git and perl are already installed.&quot;
&gt; 
&gt; &quot;Gitolite is useful in any server that is going to host multiple git repositories, each with many developers, where &quot;anyone can do anything to any repo&quot; is not a good idea.&quot;
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/98#issuecomment-395576162) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
