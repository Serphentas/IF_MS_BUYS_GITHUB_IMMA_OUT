# [Update README.md](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/35)

> state: **closed** opened by: **sirjessthebrave** on: **2018-6-4**

Using Outlaw instead of Refugee

Note: the email address github.outlaws@upend.org will have to be created 

### Comments

---
> from: [**ghost**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/35#issuecomment-394458864) on: **2018-6-4**

Outlaw implies outside of the rule of law in some sense.  Which maybe it might come to, but there&#x27;s plenty pro-capitalist, pro-rule of law developers that could be appealed to here, a lot more than refugees probably by not using the world &#x27;outlaw&#x27; here.  What about &quot;victims&quot;?
---
> from: [**sirjessthebrave**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/35#issuecomment-394460649) on: **2018-6-4**

So we can deconstruct the implications of the word outlaw but not refugee? Honestly just pick another one.. rebel, dissident, pirate, defector, renegade, castaway 
---
> from: [**ghost**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/35#issuecomment-394462093) on: **2018-6-4**

I never said we couldn&#x27;t deconstruct the implications of the word refugee.  I would say the implications are the correct implications.  castaway might work better though.

---
> from: [**sirjessthebrave**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/35#issuecomment-394462531) on: **2018-6-4**

Well NM looks like he found a better word: &#x27;Evacuation&#x27;. Closing this PR
