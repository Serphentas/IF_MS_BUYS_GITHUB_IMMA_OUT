# [Paranoia or actual fact?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108)

> state: **closed** opened by: **naholyr** on: **2018-6-7**

Is the algorithm defining &quot;top trending&quot; developers and repos actually known with details? Is the infamous censorship actually provable or is it really paranoia *in fine*?

I think it&#x27;s worth really thinking about this and gathering real proofs like opposing the algorithme and the numbers. This way you can say “it’s supposed to display us if we had more than X likes in the last Y hours, and we did, here are the proofs, so the only explanation is that someone (most supposedly MS) has manually removed us”.

Which is way more believable than &quot;LOOK MA MS CENSORED US&quot;, which could definitely be true, but needs more facts.

### Comments

---
> from: [**rdebath**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395320333) on: **2018-6-7**

The problem I see is that a simple &quot;more than X things in Y hours&quot; is open to abuse. A this point it is, IMO, quite likely that this repo has tripped an anti-spam rule. It&#x27;s likely after all to be a really dumb _marketing_ decision to censor this on purpose, and even the old M$ was never bad at marketing.

---
> from: [**limonte**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395320786) on: **2018-6-7**

&gt; Is the infamous censorship actually provable or is it really paranoia in fine?

How can you prove or disprove something in the proprietary code?
---
> from: [**naholyr**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395324436) on: **2018-6-7**

&gt; How can you prove or disprove something in the proprietary code?

Then if you can&#x27;t prove it, you should not claim it as a fact
---
> from: [**limonte**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395327004) on: **2018-6-7**

&gt; Then if you can&#x27;t prove it, you should not claim it as a fact

Kinda agree with you. But the inability to prove something doesn&#x27;t mean we can&#x27;t make **assumptions**, right? I&#x27;m not the native English speaker and the statement in the readme seems OK to me, but if other people see &quot;100% proved fact&quot; in that sentence, then the sentence should be changed.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395329404) on: **2018-6-7**

The idea that this is some anti-spam mechanism is farcical. Getting 4000 stars is not something you can just fake. There is no army of Russian bots creating GitHub accounts and staring the repo. You are free to click on the stargazers and review their histories. 

Occam&#x27;s razor: Deliberate removal. 

&gt; Then if you can&#x27;t prove it, you should not claim it as a fact

This is not a newspaper. UpEnd is making a charge. 

They&#x27;ve neither shown us the code or answered my charge. So the charge will stand.

UpEnd&#x27;s position is that GitHub has not been honest with it&#x27;s users, because it built its business on an implied promise to open source developers that they were a neutral corporation with no conflict of interest, then built it up enough to sell to a non-neutral corporation with immense conflict of interest for $7,500,000,000.

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395331295) on: **2018-6-7**

I&#x27;ll be making a change to this repo tomorrow, because: 

1. Not everyone agrees with me or the purpose of this repo.
2. This repo was never a public forum, but too many people are treating it as one. 
3. This repo can&#x27;t be a community effort, because we have no consensus community here.

I will ask people to self organize into whatever groups they want wherever they want, and I can link to their efforts from this repo. More later. 
---
> from: [**rdebath**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395331799) on: **2018-6-7**

@vassudanagunta You are in Eastern Europe your timezone is close to UTC (like mine).  Github&#x27;s offices are in America they are mostly asleep and not at work right now. An anti spam rule as to be dumb, it has to fail on the &quot;oops I&#x27;m sorry I&#x27;ll put that back&quot; side rather than the &quot;Oh dear a hate group got on our homepage&quot; side.

Your headline is fine right now; but be prepared to change it when they get into the office.

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395332444) on: **2018-6-7**

I am in New York. We have been off the list for nearly 24 hours. So that cannot be the reason. 

We were taken off in the morning of their California business hours. 
---
> from: [**rdebath**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395334138) on: **2018-6-7**

Ah, sorry, I should have realised you wouldn&#x27;t have any problem touching this repo at 4am. :smile: 
---
> from: [**dispix**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395334851) on: **2018-6-7**

&gt; This repo can&#x27;t be a community effort, because we have no consensus community here.

I think this is a critical point that should be expressed in the README. I guess a lot of people came here precisely to create a community important enough to have an impact on how Github handles this event. The fact that you are now expressing quite the opposite and closing issues/deleting comments left and right, while screaming &quot;CENSORSHIP&quot; when Github supposedly removes this repo from the top trend, has the potential to hurt the credibility of the whole initiative.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/108#issuecomment-395339838) on: **2018-6-7**

@dispix,

First, it is not a contradiction. Read my comments at the top of #91.  This is not a public forum and never was! But GitHub claims to be an open non-discriminating platform, and the trends page implies truth. 

&gt; to create a community

&gt;  to have an impact on how Github handles this event. 

It isn&#x27;t as easy as you think to do the first in a way that makes everyone happy and do the second. They are almost contradictory goals. 

Have you ever tried to create a community? Or led one? Have you ever run a startup, or run a multi-million dollar project in a large company? If you have, you would know that getting shit done and &quot;open ended community that welcomes all people of all opinions and philosophies&quot; is a fools errand. All you have to do is look at any successful open source project and you will see this truth there too. They are all run by a (benevolent) dictator, or a team of people who knew they had like minds before teaming up. Do you think, for example, that the Jekyll project would tolerate people coming in and opening issues rudely mocking their choice of Ruby? 

It was my mistake not to remember this during the rush to respond to the acquisition. 

Anyway, I will correct this problem tomorrow. I need some sleep now. 

