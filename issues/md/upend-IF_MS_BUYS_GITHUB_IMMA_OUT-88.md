# [This repo isn&#x27;t in weekly trends](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88)

> state: **open** opened by: **XakepSDK** on: **2018-6-6**

Just look at this: 3400++ starts in 4 days and this repo now isn&#x27;t in trending list.
![a](https://user-images.githubusercontent.com/4938483/41021429-3b55442c-6988-11e8-90a4-469645c008ba.png)
Micrototalitarism from microsoft.

### Comments

---
> from: [**Bloggerschmidt**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394960179) on: **2018-6-6**

This is called censorship. It isn&#x27;t in daily trends, too.
---
> from: [**zskovacs**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394961416) on: **2018-6-6**

No, this is called paranoia...

![image](https://user-images.githubusercontent.com/20083522/41021688-99164b18-6967-11e8-82e2-37f06f48c1b2.png)

---
> from: [**XakepSDK**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394961748) on: **2018-6-6**

Wtf, i can&#x27;t find it
---
> from: [**XakepSDK**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394962038) on: **2018-6-6**

It is found in https://github.com/explore, but not in https://github.com/trending?since&#x3D;weekly
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394963230) on: **2018-6-6**

They clearly had someone to take it out. [Because this was rather embarrassing](https://twitter.com/UpEnd_org/status/1003936552840515584). 
---
> from: [**Omeryl**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394963497) on: **2018-6-6**

The trending repos work in funny ways, and it&#x27;s been known to drop repos that are consistently popular some times to let others shine. 

EDIT: https://web.archive.org/web/20180606080036/https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394964397) on: **2018-6-6**

@Omeryl That isn&#x27;t likely because &#x60;ry / deno&#x60; has been on that board before this group, and was along side us this entire time.  
---
> from: [**e-kwsm**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394977166) on: **2018-6-6**

You should not censor comments.
---
> from: [**nukeop**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394982829) on: **2018-6-6**

Already on HN: https://news.ycombinator.com/item?id&#x3D;17245018
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394987150) on: **2018-6-6**

This group is like a big meeting. This meeting has a specific agenda. It is not a &quot;free speech&quot; meeting. There are plenty of places for free speech. 

Debates about which GitHub alternative, or about [whether an image I posted was insensitive to women](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/58) are on topic and very welcome. Posts by people opposed to this group or its aims don&#x27;t belong here. Their only purpose is to undermine this group. They can do that anywhere else but here.

I&#x27;ve only had 3 hours of sleep for four nights in a row, and zero so far tonight (4:30am). I&#x27;m doing the best I can to keep this project on track. 
---
> from: [**nukeop**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394989455) on: **2018-6-6**

The &quot;insensitive to women&quot; thing is a classic derailment tactic employed by trolls. You should not have responded to it, or yielded to their demands.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394994338) on: **2018-6-6**

I just updated the homepage (README) with the news about the Microsoft censorship.

---
> from: [**e-kwsm**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394995065) on: **2018-6-6**

You justify your censorship but abuse MS&#x27;s? (I don&#x27;t think MS/GH censors.)
---
> from: [**Droppers**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394996900) on: **2018-6-6**

You are abusing other peoples opinion by editing and removing comments that don&#x27;t fit your opinion.

_&quot;Without any moderation at all, this repo will turn into a pile of shit very fast.&quot;_
This is not moderation, this is censorship.
---
> from: [**MoonlightOwl**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394997497) on: **2018-6-6**

Without any moderation at all, this repo will turn into a pile of shit very fast.

_&gt; This is not moderation, this is censorship._
I think the difference here is very thin.
---
> from: [**RyanGannon**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-394998925) on: **2018-6-6**

@Droppers This repo has a clear purpose and it makes sense to remove threads that aren&#x27;t relevant to that. It doesn&#x27;t stop you from finding an alternative or using both. That is the freedom we have on this platform, to provide an opportunity for all views but requiring all people to accept them.

In short, it cuts both ways.
---
> from: [**Droppers**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395000017) on: **2018-6-6**

@RyanGannon I agree with that, but removing or editing any counter-argument is not helping anyone.

This repository has a bad reputation because of this.
---
> from: [**thrasos**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395002648) on: **2018-6-6**

And there goes Github&#x27;s sense of humour (among other things).
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395012115) on: **2018-6-6**

@Omeryl, @e-kwsm, @Droppers and anyone else who dislikes my censorship. I created a space for you guys to express your disagreement: #91.

**If you guys self-delete your comments about censorship from here and other places** and promise to keep them within #91, then you can have that forum. Deal? That&#x27;s the best I can do.  
---
> from: [**HExiangyu**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395039644) on: **2018-6-6**

前排兜售香烟瓜子火腿肠啤酒饮料
---
> from: [**ajacksified**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395067475) on: **2018-6-6**

I got it in my &quot;trending repos&quot; daily email 🤷‍♂️ 

&lt;img width&#x3D;&quot;604&quot; alt&#x3D;&quot;screen shot 2018-06-06 at 9 25 45 am&quot; src&#x3D;&quot;https://user-images.githubusercontent.com/175515/41040862-9bd370d4-696b-11e8-9244-baf7a673404d.png&quot;&gt;

Careful not to get _too_ knee-jerky.
---
> from: [**Leprecorn**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395086903) on: **2018-6-6**

@ajacksified That email is outdated, its been 2000+ stars for atleast a day
---
> from: [**ajacksified**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395091706) on: **2018-6-6**

It was literally from an hour ago.

On Wed, Jun 6, 2018, 10:23 AM Leprecorn &lt;notifications@github.com&gt; wrote:

&gt; @ajacksified &lt;https://github.com/ajacksified&gt; That email is outdated, its
&gt; been 2000+ stars for atleast a day
&gt;
&gt; —
&gt; You are receiving this because you were mentioned.
&gt; Reply to this email directly, view it on GitHub
&gt; &lt;https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395086903&gt;,
&gt; or mute the thread
&gt; &lt;https://github.com/notifications/unsubscribe-auth/AAKtmxnqLDv4q3ycZgeXaNAPsGauh0yiks5t5-XqgaJpZM4UcFE-&gt;
&gt; .
&gt;

---
> from: [**Flarp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395097000) on: **2018-6-6**

US here, cannot see it anywhere
---
> from: [**quantumpacket**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395112026) on: **2018-6-6**

Not showing up for me anymore. I came and looked just for this thread.
---
> from: [**jorgebucaran**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395120168) on: **2018-6-6**

I am far from an expert, but I think it is very plausible that this project falls in the gray area of GitHub TOS ([see 2 and 3 of acceptable use](https://help.github.com/articles/github-terms-of-service/#c-acceptable-use)). So, wouldn&#x27;t it be reasonable to expect someone at GitHub simply removed it from the github.com/trending as a silent warning?
---
> from: [**kuharan**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395170907) on: **2018-6-6**

Also, check out the developer Ranks. How is Nadela Ranked as No.1?  I mean, WTF?

![image](https://user-images.githubusercontent.com/22185333/41058167-b7c8be68-69e6-11e8-9cdd-61e611cabea9.png)

---
> from: [**jorgebucaran**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395243243) on: **2018-6-7**

@kuharan I think the only reason he&#x27;s showing up there is that a lot of users happen to be following his account. Not at all odd considering recent events. It&#x27;s just an algorithm.
---
> from: [**hktonylee**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395305563) on: **2018-6-7**

This reminds me [https://www.theregister.co.uk/AMP/2016/04/15/microsoft_discontinues_robovm/](https://www.theregister.co.uk/AMP/2016/04/15/microsoft_discontinues_robovm/). The M$ guy who discontinued a rival open source project is exactly the same guy in charge of Github. 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/88#issuecomment-395578920) on: **2018-6-7**

Hey all. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
