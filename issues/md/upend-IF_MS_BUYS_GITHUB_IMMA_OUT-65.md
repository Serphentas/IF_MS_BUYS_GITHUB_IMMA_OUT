# [As alternative, should give Sourceforge a chance again?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65)

> state: **open** opened by: **MarkoCen** on: **2018-6-5**

Here is today&#x27;s Sourceforge main page with this [GitHub importer](https://sourceforge.net/p/forge/documentation/GitHub%20Importer/)
![Sourceforge website screenshot](https://i.imgur.com/HBLpflv.png)

### Comments

---
> from: [**Ominousness**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-394808678) on: **2018-6-5**

Hell no. Malware in installers? - tyvm.
---
> from: [**necrophcodr**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-394818257) on: **2018-6-5**

Sourceforge isn&#x27;t even a free or open source, and it has a history of infrastructure problems, malware injections, and worse.
---
> from: [**Atavic**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-394841219) on: **2018-6-5**

It had a blackout in early 2018. Many projects landed on Github.
---
> from: [**loganabbott**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-394876579) on: **2018-6-6**

Hi all, president of SourceForge here. Just in case people don’t know, a lot has changed at SourceForge since my company acquired them in 2016. We [covered the improvements](https://sourceforge.net/blog/congratulations-github/) again here yesterday, but here is a summary since we took over.

The very first thing we did after acquiring SourceForge two years ago was remove bundled installers from projects, and vow that it&#x27;ll never happen on SourceForge again. Since then, further improvements have included:

- Implemented malware scans with a partnership with Bitdefender for every single project on SourceForge
- Removed ads for developers (if you’re logged in, you’ll never see ads)
- Added HTTPS support for project website hosting and all downloads
- Completely redesigned SourceForge.net from the ground up

We&#x27;d love for people to give SourceForge another shot. We&#x27;re committed to making SourceForge a solid, trustworthy place for FOSS hosting. If you&#x27;re interested in giving us a shot, we have a [GitHub to SourceForge importer here](https://sourceforge.net/p/forge/documentation/GitHub%20Importer/).

To address some of the above comments:

&gt; Sourceforge isn&#x27;t even a free or open source, and it has a history of infrastructure problems, malware injections, and worse.

SourceForge is completely free, and is built with Apache Allura.

&gt; Hell no. Malware in installers? - tyvm.

Malware free since 2016. No adware in any installers, and every project on the SourceForge is scanned for malware by Bitdefender and another security service as well. Literally the first thing we did was eliminate the bundled installers as soon as we finished signing the acquisition agreement. We had nothing to do with those previous decisions and nobody who was involved with those decisions is at SourceForge anymore.

---
> from: [**Bengt**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-394880786) on: **2018-6-6**

Thanks for improving SourceForge. However, by comparison with GitLab&#x27;s monthly (!) releases, these changes look like bringing a knife to a gunfight:

https://about.gitlab.com/2018/03/22/gitlab-10-6-released/
https://about.gitlab.com/2018/04/22/gitlab-10-7-released/
https://about.gitlab.com/2018/05/22/gitlab-10-8-released/
---
> from: [**Revivify**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-394886158) on: **2018-6-6**

Sourceforge is garbage. start a 501c3 to host a community developed alternative.
You&#x27;ll get bought just like GitHub did. It&#x27;s the dream of sillyvalley

---
> from: [**h3**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-394889774) on: **2018-6-6**

@loganabbott It&#x27;s nice to see the new direction SF is taking and the commitment you have for FOSS, however, I fear it&#x27;s a bit late to recover from the damage that has been done over so many years :/

Furthermore, the solution you are up against has too many advantages and benefits over SF to even be a consideration:

1. It&#x27;s FOSS and thus maintained at a pace and scale you probably can&#x27;t ever compete with

2. It&#x27; installable on-premise, no fees or questions asked.

3. It&#x27;s pragmatically FOSS oriented, not business and marketing oriented. So there is no &quot;Deals&quot; section or advertising. Even if your new redesign is miles ahead of what it used to be, it&#x27;s pretty much exactly what people fear Github will look like once Microsoft takes over and ruin it.

4. It&#x27;s already integrated with many FOSS tools or third-party SaaS solution as a first-class citizen just like Github because the integration is often very similar.

5. The UX is reminiscent of the Github philosophy, so the learning curve is *really really* fast

6. In its own right, Gitlab is already a good contender to replace Github, which I believe only latched on due to historical value and inherent developer laziness. Thanks to Microsoft, this is changing fast :)

My intention is not to trash SF, I don&#x27;t think to have the right to criticise anyone trying to contribute to the FOSS universe, on the contrary, I can only applaud them.

However, I think you deserve to understand why your efforts will likely fail and why it&#x27;s probably too late to surf the wave you are going for.
---
> from: [**loganabbott**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-394891259) on: **2018-6-6**

I don&#x27;t look at it as a zero sum game where winner takes all. We have over a million daily users and 500,000 projects hosted with us, so we&#x27;re just focused on improving their experience.

We&#x27;re actually profitable at SourceForge already with no plans to sell. We&#x27;re not located in Silicon Valley, and we have taken no outside funding. That&#x27;s not the case with GitLab and others. They&#x27;re great products too, but I&#x27;d imagine they&#x27;ll end up being bought by a company like Microsoft eventually, or will have to drastically change their business model to be able to exist indefinitely. We&#x27;ve seen a huge influx of imports from GitHub in the past 3 days, so that&#x27;s good.

At the end of the day, the more viable options out there for FOSS, the better. GitLab focuses on developers while SourceForge focuses not only on developers, but also on search and discovery for end-users where they can easily find, download, and install the software they need with the click of a button, whether or not they have any technical or coding ability. It&#x27;s apples and oranges and I think we can both co-exist.
---
> from: [**TwinProduction**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-394895696) on: **2018-6-6**

@Bengt on the other hand, GitLab has the annoying &quot;This site uses cookies&quot; pop-up...
---
> from: [**h3**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-394905248) on: **2018-6-6**

@loganabbott the most important difference to me is that even if Gitlab gets bought or change philosophy significantly, its source code can always be forked and maintained openly. No closed source solution can offer that level of flexibility and long-term security.

Incidentally, Gitlab knows very well that if they take a shit in the punch bowl, someone else is likely to quickly roll-out an alternative solution based on their very own source code.

https://gitlab.com/gitlab-org/gitlab-ce/blob/master/LICENSE
---
> from: [**rubo77**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-395308483) on: **2018-6-7**

@loganabbott: why don&#x27;t you make the Source Code of sourceforge open source as well?

I think with 500,000 projects already hosted , your have nothing to lose.
---
> from: [**loganabbott**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-395442658) on: **2018-6-7**

@rubo77 SourceForge is built with [Apache Allura](https://allura.apache.org/), which is already open source.
---
> from: [**rubo77**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-395497793) on: **2018-6-7**

Are there many modifications from the standard Allura platform in sourceforge?
---
> from: [**loganabbott**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-395499461) on: **2018-6-7**

Yeah there are some, but when we make changes and improvements to the development tools on SourceForge, that code makes it&#x27;s way back into Apache Allura, as our devs are the biggest contributors to the Allura project.
---
> from: [**williamknauss**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-395534029) on: **2018-6-7**

I&#x27;m going to weight in here. I don&#x27;t believe Github is going down the drain (in regards to MS acquiring them). However in the spirit of open source, collaborating with others, and knowledge sharing I&#x27;d be willing to try Source Forge a try again. @loganabbott I&#x27;m willing to give SF a try again. BTW got any PHP jobs open there? :smiley:
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/65#issuecomment-395576099) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
