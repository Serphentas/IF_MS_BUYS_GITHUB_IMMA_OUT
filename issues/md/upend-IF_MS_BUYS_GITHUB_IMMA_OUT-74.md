# [Create our own FreeHUB](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74)

> state: **open** opened by: **AEK-BKF** on: **2018-6-6**

Hi developers !

I think it&#x27;s a good idea to create our own FreeHub, we collect in it everything is open source.
The FreeHub won&#x27;t belong to one or two Founders, but thousands founders, we can do it !

What&#x27;s your opinion ?


### Comments

---
> from: [**TwinProduction**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-394893402) on: **2018-6-6**

http://freehub.com is already taken, though. :(

**UPDATE:** I bought [gitbye.com](http://gitbye.com), which is a pretty cute alternative
---
> from: [**AEK-BKF**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-394893832) on: **2018-6-6**

@TwinProduction it&#x27;s just a name but what I mean is the idea : create a free git host instead of this one Github !!

---
> from: [**TwinProduction**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-394894615) on: **2018-6-6**

I don&#x27;t think that the issue is that we have nowhere to go. There are already existing alternatives (e.g. BitBucket, GitLab).

The thing is that GitHub somewhat ditched us.

On the other hand, GitHub was evaluated at $2B in 2015. The fact that MS is willing to take out $7.5B 3 years later means that they were dead set on buying it.

Not to mention that if everything is free, how will we afford the servers?
---
> from: [**AEK-BKF**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-394895787) on: **2018-6-6**

Who said that BitBucket or GitLab ... wont sell themselves as GitHub did ? 
If we want we can and we will do it ! it&#x27;s so easy to create new Git Host belongs to thousands developers.
Try to live as a lion even it walks alone better than a sheep which follows others to live ...
---
> from: [**fn-main**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-394900188) on: **2018-6-6**

@TwinProduction , we could host the servers with Docker perhaps?
---
> from: [**fn-main**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-395274549) on: **2018-6-7**

If you guys decide to, I will help out with making a Git based alternative. I know Rust, Python 2.7, Go, JS, CSS, and HTML. If you need me, just let me know.
---
> from: [**prenex**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-395299843) on: **2018-6-7**

It is interesting that we use a distributed version control system, but we use a centralized point for storing stuff - like github. Wouldn&#x27;t it be much better if we would make a system that only centralize the community of &quot;gitea-like servers&quot;?

My idea is to create as many gitea servers as we want ourselves, but create a single community point that unifies the login, search and ranking only. Think about it like this: you install your gitea to your server, raspberry pi, whatever, give it a UUID - or hopefully a name like prenex_stuff.git - and by default you are having your &quot;private repositories&quot;. By non-default you can share a pointer to your repositories publicly through &quot;gitstack&quot; (I just named teh community service as this for whatever) and you get all the benefits of starring stuff, searching and whatever - but the main central repo will stay thin with a small database preferably runnable on a single machine and easily mirrored for backup by anyone.

I would add a system that you can share 32 + as many puclic repositories as you have &quot;mirrored&quot; from other authors servers to keep data safe. I am not sure about the ranking system, but you get the idea that there must be some driving force besides liking stuff to mirror them on your machine but on the other hand we better ot mirror everything just as-is. Maybe you can actually ask people to mirror your stuff and thus it could be a much better community with more communication after all. For example: &quot;look at this awsome algorithm I wrote for computer vision - do you want to help me keep it alive by mirroring?&quot;

In the above mentioned system, mirroring things should be as much of a top priority as adding new code.

Is this a bad idea? I am thinking about maybe doing this, but I guess my time is too limited and I am already struggling with anough home projects haha so at least I wanted to share the idea ;-)
---
> from: [**fn-main**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-395445519) on: **2018-6-7**

If someone else wants to I would be happy to work on something like this. How about for the name, something like mirror.io?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-395580081) on: **2018-6-8**

Hey all. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-395595149) on: **2018-6-8**

@fn-main Why not extend already working Git Center? 
---
> from: [**prenex**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-395746756) on: **2018-6-8**

@ferittuncer I like zeronet, but there are people who do not prefer things like that. The thing proposed is more traditional in its architecture as it is a community of repo-hosting devices with mirroring done more visibly. I think it is a question of simplicity to do this well so that people do not see the system as a not understandable black box. Also I don&#x27;t know if it is good to complicate both projects instead of concentrating of only one thing. I don&#x27;t want both of them to lose in this...

@fn-main Maybe &quot;sucklesshub.io&quot;? If we can keep the architecture simple and fast it would be a good name for three reason:
1.) It is not already taken like previous choices here
2.) It has a riot-mentality deep in its name against the happenings with github
3.) It is a bit suckless.org simplicity by not using very complicated storage all around the cloud but using git as it should be: for distributed storage and simple open usage.

I am sure I would love it and it feels good that at least some people shares same or similar thoughts.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/74#issuecomment-395911526) on: **2018-6-9**

reminder this forum is shutting down in 5 hours 20 minutes. 
