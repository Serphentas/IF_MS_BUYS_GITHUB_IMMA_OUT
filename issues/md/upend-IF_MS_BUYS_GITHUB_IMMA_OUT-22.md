# [Replacement for github profiles/contribution timeline?](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/22)

> state: **open** opened by: **daveloyall** on: **2018-6-4**

Many github users work as freelancers and use their github profile as a sort of resumé.

I speculate that some users may want to leave github, but feel compelled to stay because they don&#x27;t want to &#x27;start over&#x27; on another profile elsewhere.

Shall we collect a list of recommendations for profile portability?

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/22#issuecomment-394401798) on: **2018-6-4**

In addition, we may want to establish &quot;best practices&quot; for keeping your GitHub profile and repos open as placeholders with forwarding addresses.
---
> from: [**revelt**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/22#issuecomment-394660574) on: **2018-6-5**

https://github.com/sallar/github-contributions-chart -like chart could be generated from git data and aggregating from all places, not just GH.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/22#issuecomment-395576617) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
