# [GitHub was never free and it&#x27;s a big company just like Microsoft](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/5)

> state: **closed** opened by: **greenboxal** on: **2018-6-4**

Get over it

### Comments

---
> from: [**greenboxal**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/5#issuecomment-394200125) on: **2018-6-4**

![2bjx96](https://user-images.githubusercontent.com/1245785/40892327-71cbacca-674a-11e8-81f1-4f242aa024f6.jpg)

---
> from: [**racerxdl**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/5#issuecomment-394200246) on: **2018-6-4**

![image](https://user-images.githubusercontent.com/578310/40892349-47fedf08-676c-11e8-81e4-e2d01b62ab5d.png)

---
> from: [**YgorCastor**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/5#issuecomment-394200386) on: **2018-6-4**

Off course it was! Just like facebook is! Ohh, no w8.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/5#issuecomment-394204531) on: **2018-6-4**

@greenboxal thanks for your opinion. **Independence** of the de-facto hub of open source is a critical thing.

What is not a good thing is **concentration of power**. GitHub got big organically, due to it success, and due to open source developers trusting it. Getting swallowed up by a non-nuetral behemoth is not a good thing. 

But if you don&#x27;t see things that way, let&#x27;s just agree to disagree. 
---
> from: [**bamorim**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/5#issuecomment-394239257) on: **2018-6-4**

Well, if they are so amazing why they are selling it to Microsoft?
---
> from: [**damm**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/5#issuecomment-394260992) on: **2018-6-4**

That&#x27;s a good conversation because knowing how github started without VC and no debt; I imagine bad management and infrastructure cost haunt them.

They give you/us free space and you don&#x27;t pay for anything with a free account; just a bunch of repos.  Is there advertising to help bring in revenue? right 

Don&#x27;t be shocked when you start seeing banners inbetween diff&#x27;s in a PR request.

---
> from: [**AlexSwensen**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/5#issuecomment-394552113) on: **2018-6-5**

@vassudanagunta The fact that you close all issues that disagree with your point of view shows that you are not the right person to lead this movement. You are closed to any ideas that disagree with your own.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/5#issuecomment-395581769) on: **2018-6-8**

On the question of censorship, please read Issue 114.
