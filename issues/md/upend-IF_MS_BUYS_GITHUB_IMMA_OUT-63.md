# [For now an Alternative - Bitbucket](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63)

> state: **open** opened by: **kuharan** on: **2018-6-5**

Have a look at this https://www.upguard.com/articles/github-vs-bitbucket 

Just in case anyone wants to have private repositories too.

### Comments

---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63#issuecomment-394794118) on: **2018-6-5**

I&#x27;ve always used Bitbucket for private repositories because they are free on there.
---
> from: [**licaon-kter**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63#issuecomment-394796290) on: **2018-6-5**

#40

/Close this
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63#issuecomment-394843520) on: **2018-6-5**

Also #25 already talks about taking BitBucket into consideration. Close this.
---
> from: [**Serphentas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63#issuecomment-394858500) on: **2018-6-5**

I personally don&#x27;t have any power of influence on this repo, but as you can see for yourself there are already 28 open issues and this whole affair is not easily coped with. It is in the interest of all of us to keep this place as tidy as possible to avoid getting lost in a sea of suggestions (as good as they may be).

To make it clear, my comment does not aim to bash someone or an idea, but rather to help people gather all their suggestions in places where discussions are already taking place.
---
> from: [**krixano**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63#issuecomment-394859717) on: **2018-6-5**

If this was in response to what I said, I was just meaning that instead of saying [blank] &quot;already talks about&quot; bitbucket. &quot;Close this.&quot; You could have been more polite and said: I think [blank] already talks about Bitbucket, so perhaps this should be closed so all related discussion is located in one place? Politeness goes a long way, imo.
---
> from: [**TwinProduction**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63#issuecomment-394895981) on: **2018-6-6**

My only problem with BitBucket is that the UI&#x27;s disgusting compared to GitHub&#x27;s.
---
> from: [**panchtatvam**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63#issuecomment-395085364) on: **2018-6-6**

Gitlab anyone ?
---
> from: [**licaon-kter**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63#issuecomment-395095804) on: **2018-6-6**

@panchtatvam #9
---
> from: [**kuharan**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63#issuecomment-395169473) on: **2018-6-6**

@panchtatvam yup checked out Gitlab yesterday. It has import repository (from GitHub) options too.  
Also, check out this - https://natfriedman.github.io/hello/
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/63#issuecomment-395578150) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
