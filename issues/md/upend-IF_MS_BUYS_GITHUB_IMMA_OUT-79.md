# [Prepare for GitHub CEO AMA on Reddit](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/79)

> state: **open** opened by: **TwinProduction** on: **2018-6-6**

The new CEO of GitHub, [Nat Friedman](https://en.wikipedia.org/wiki/Nat_Friedman), will be doing an AMA on Reddit in a few days. [Proof](https://www.reddit.com/r/programming/comments/8ojnah/hello_github_natfriedman/)

Also, see https://natfriedman.github.io/hello/

Prepare your questions wisely. 

---------

_Tbh, I&#x27;m already pissed at the guy. He says he&#x27;s been a developer since he was **6**._


### Comments

---
> from: [**Citizen163**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/79#issuecomment-394946446) on: **2018-6-6**

&quot;Why are you such sniveling waste of human excrement and what original sin did humanity commit to deserve you?&quot;
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/79#issuecomment-394959254) on: **2018-6-6**

You&#x27;re probably just venting, @Citizen163, but please, if you&#x27;re going to say things like that, do that as an individual, not as part of this effort. Don&#x27;t undermine the values we claim to hold and expect other people to hold. 
---
> from: [**Citizen163**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/79#issuecomment-394963153) on: **2018-6-6**

That&#x27;s not what I would write if I had a Reddit account and the time of day. I&#x27;m just echoing the questions that are most likely going to be asked, hence the quotation marks.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/79#issuecomment-394963840) on: **2018-6-6**

I think if there is one question that should be asked under the banner of this group, it is #88. 
---
> from: [**nukeop**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/79#issuecomment-394976238) on: **2018-6-6**

He&#x27;s the former Xamarin CEO, why would you think he&#x27;s bad in any way? He&#x27;s not a personification of Microsoft, he&#x27;s someone that&#x27;s likely to be well received by people interested in free software, that will be replaced by a corporate drone a year or two down the path.
---
> from: [**tobylane**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/79#issuecomment-395006226) on: **2018-6-6**

Why was a Microsoft employee chosen to lead the independent Github? The last three major acquisitions by Microsoft (Nokia, Mojang and LinkedIn) are all lead by pre-acquisition staff.

He won&#x27;t answer it fully and isn&#x27;t necessarily the right guy to ask but the pre-prepared PR answer will be interesting.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/79#issuecomment-395577283) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
