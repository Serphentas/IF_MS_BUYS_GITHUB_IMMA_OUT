# [Edit Bio And Fork The Repo To Spread The Word](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/104)

> state: **open** opened by: **ferittuncer** on: **2018-6-7**

As GitHub censored our repo from trending list, I think we should think other ways of spreading the word.

One way would be editing our GitHub profile bios by appending &quot;ms acquisition refugee&quot; or whatever phrase you like that will explain the same thing. 

I just edited mine.

Also forking and pinning the repo helps to spread the word I think.

### Comments

---
> from: [**fn-main**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/104#issuecomment-395276440) on: **2018-6-7**

We could make a r/programming post about it
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/104#issuecomment-395279180) on: **2018-6-7**

thought let&#x27;s not use &quot;refugee&quot;. Even though it feels like the right metaphor, there&#x27;s no reason to be disregard actual refugees who feel it trivializes what they go though. Please read the comments in #26

can we stick to &quot;evacuee&quot;? it&#x27;s been the title through most of this repos visibility and on twitter.  
---
> from: [**moorchegue**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/104#issuecomment-395334691) on: **2018-6-7**

I edited the bio and forked this very repo to raise awareness. Taking the whole migration very gradually, but making it loud enough.
---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/104#issuecomment-395449890) on: **2018-6-7**

I sent a pull request #112 @vassudanagunta 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/104#issuecomment-395580195) on: **2018-6-8**

I love this idea. Let&#x27;s talk more about it. But first, please read Issue 114.
