# [Setup Your Own Git Server : Explained](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/41)

> state: **open** opened by: **ozanonurtek** on: **2018-6-5**

Hello all, 
As librenotes team we are very happy to try contributing to this domain and we prepared a guide about how to setup your own git server and create a script for migration from github to your own server.
If you wish you can put it to readme, so anyone can use it as free as in freedom.

Checkout:
[Your Own Git](https://github.com/librenotes/your_own_git)

Best Wishes,
Librenotes Team

### Comments

---
> from: [**sbarre**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/41#issuecomment-394706293) on: **2018-6-5**

An even simpler option that I&#x27;ve been using is [Gitolite](http://gitolite.com/gitolite/install/) which simply layers repo-specific permissions on top of git-over-ssh.  

It takes about 30 minutes to set up, and is very easy to administer and operate, and is a great fit for small teams or small projects that don&#x27;t need a web interface to the Git repos.

---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/41#issuecomment-394773892) on: **2018-6-5**

But the problem is, we don&#x27;t want millions of separate servers, do we? In GitHub we can browse other projects because it&#x27;s centralized, if we all host a server on our own it won&#x27;t be possible I think. 
---
> from: [**ozanonurtek**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/41#issuecomment-394821932) on: **2018-6-5**

@ferittuncer ofcourse you are right. So maybe we can create a centralized(totally created by comunity not by a company or service) search mechanizm, which stores index of, millions of seperate servers(decentralized git servers) and we can query in this way? As you know, internet works like this. There is no one web page which contains thousands of different kind of information, every page on inernet provides information in different domains. There are no obstacles to do it on this way. And we can also put popular git services index, to this mechanism, so it serves to everyone. 

If community wishes in this way, as librenotes team, we are ready to dirty our hands, by writing free software for the goodness of the society and the love of freedom and privacy. 


---
> from: [**ferittuncer**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/41#issuecomment-394866952) on: **2018-6-5**

And that&#x27;s the main problem that needs the most significant effort actually: To create a platform. If it wouldn&#x27;t, there are plenty alternatives for maintaining your own isolated Git server.

We found such platform, surprisingly decentralized and already working. Please see #72 
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/41#issuecomment-395576911) on: **2018-6-7**

Hey guys. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
