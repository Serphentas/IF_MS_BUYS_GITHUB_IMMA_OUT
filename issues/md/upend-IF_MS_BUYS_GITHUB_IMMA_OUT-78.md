# [A decentralized repository of repositories](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/78)

> state: **open** opened by: **railken** on: **2018-6-6**

What about leaving the &quot;repository hosting part&quot; to Github/Gitlab/Bitbucket/YourLovingHub, so anyone can choose where to host the code?
What about building instead a &quot;repository&quot; to collect them all, to rule them all. 
A sort of &quot;index&quot; of all repositories, from github, from bitbucket, etc.

On top of that a &quot;social part&quot; that we all like and all love with all the stars, issues, etc.
Once registered, a user may link or not with gitlab or his &quot;Git-Compatible hosting&quot; favorite.
Maybe a multiple hosts can be set.

Once created a new repository, the &quot;default git host selected&quot; will be used to host the actual source code/.git, while all the stars/issues/etc will be saved in this &quot;global&quot; repository.

This way the storage needed will be drastically reduced and could be decentralized and hosted everywhere, fully complete.
If a &quot;x&quot; company is lost, one can simply migrate the code to another host, preserving the community.

Like github, but with the code hosted in a server that you choose, and with the stars and issues spread all over the world.

&quot;One Ring to rule them all, one Ring to find them,
One Ring to bring them all and in the darkness bind them&quot;

### Comments

---
> from: [**milewski**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/78#issuecomment-394928276) on: **2018-6-6**

bad idea actually .. this would generate two communities .... people against it would stay in github and people in favor would stay on the other side... better is built it on top of blockchain :D and lets everyone become a node 
---
> from: [**railken**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/78#issuecomment-394961875) on: **2018-6-6**

That&#x27;s the key! Everyone is a node, but because the is no &quot;code&quot; hosted, everyone can be a node and have the full index/community without having PB of storage at home.
---
> from: [**milewski**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/78#issuecomment-395002484) on: **2018-6-6**

maybe it is something similar to: https://mastodon.social/ a decentralized social media but for coders instead 
---
> from: [**prenex**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/78#issuecomment-395432022) on: **2018-6-7**

I support this idea. Also I would say it should be encouraged to &quot;mirror&quot; others repo: By default you can run a server with something like gitea, but &quot;share&quot; some repo publicly that is searchable by others. Do not store the stuff.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/78#issuecomment-395579811) on: **2018-6-8**

Hey all. I love the work you&#x27;ve been doing. Not everyone loves mine. Please read Issue 114.
