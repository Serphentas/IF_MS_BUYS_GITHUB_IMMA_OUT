# [Communication](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/101)

> state: **closed** opened by: **marcus-sa** on: **2018-6-6**

In response to all the issues around where contact should be, I&#x27;ve gone ahead and created an _official_ Discord server specifically for this purpose, amongst other related communication reasons.

https://discord.gg/mRFAg27

### Comments

---
> from: [**Flarp**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/101#issuecomment-395097939) on: **2018-6-6**

How is it official? Who made it official?

Having a Discord server just makes it harder to keep track of everything, not everyone has one.
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/101#issuecomment-395098496) on: **2018-6-6**

&gt; How is it official? Who made it official?

this.
