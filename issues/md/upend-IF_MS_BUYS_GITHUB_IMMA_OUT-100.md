# [troll](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/100)

> state: **closed** opened by: **lolnogenerics** on: **2018-6-6**

From the README:

&gt; If the above does not describe you, if this project is not for you, or if you disagree with it, please do not post here. [...] Please respect our wishes in our house.

This conflicts with a previous statement made in the same README:
&gt; We were the #1 project and the #4 &quot;developer&quot; before they deleted us. Maybe it was too embarrassing?
We have not violated any terms of service.

Please remove one of the two statements. You can either chose to censor people, or chose to be against it. You can&#x27;t do both as you will look like a hypocrite, and frankfully this hurts the credibility of the project. If the leaders of the revolution do the same injusticies as our current overloards, but just at a smaller scale, are they the leaders we need or just some people that want some attention and power? 


### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/100#issuecomment-395097100) on: **2018-6-6**

The ACLU fights for people&#x27;s civil liberties, and will even defend the right of the KKK to speak and assemble (peacefully). I applaud that. But does the ACLU let KKK members into its meetings and allow them to criticize the ACLU&#x27;s work for black civil rights right there, in those meetings? Of course not.

Would it have been a contradiction or hypocrisy for Martin Luther King, John Lewis and other leaders of the non-violent civil rights movement of the 60s to disallow the participation of the KKK (white supremacists) or the Black Panthers or the Nation of Islam (black rights groups advocating violence) in their meetings and protests? Of course not.
