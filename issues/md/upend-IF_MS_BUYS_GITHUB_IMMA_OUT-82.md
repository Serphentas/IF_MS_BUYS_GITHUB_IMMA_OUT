# [Revert &quot;Remove [allegedly] sexist imagery from readme/CTA&quot;](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/82)

> state: **closed** opened by: **nyetwurk** on: **2018-6-6**

This reverts commit 56ee6a8c76533a981500d680e33cfb8c2cc2849a.

### Comments

---
> from: [**sci4me**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/pull/82#issuecomment-395249724) on: **2018-6-7**

This should have been merged tbh but at this point it&#x27;s probably best we all just let it go, eh...
