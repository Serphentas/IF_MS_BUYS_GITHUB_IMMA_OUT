# [Already feels like home](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/19)

> state: **closed** opened by: **anatolinicolae** on: **2018-6-4**

Our fellow friend @martenbjork is helping us move to our new home by repainting the walls with his nice-🍑looking Chrome extension that brings back the best OS ever, **Windows 😝** &lt;sup&gt;(1)&lt;/sup&gt;.

Wanna feel like home? Get his extension:
https://github.com/martenbjork/github-xp

&lt;details&gt;
&lt;summary&gt;Notes&lt;/summary&gt;
&lt;br&gt;
1. 🤦‍♂️  Windows XP
&lt;/details&gt;

### Comments

---
> from: [**mistermantas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/19#issuecomment-394366752) on: **2018-6-4**

Well, that’s a waste of time &amp; effort.
---
> from: [**anatolinicolae**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/19#issuecomment-394367314) on: **2018-6-4**

I see, maybe Microsoft is already on their way to this, right? 🤔
---
> from: [**mistermantas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/19#issuecomment-394367772) on: **2018-6-4**

No, that’s the old Microsoft, they’ll probably redesign the site in an incosistent manner and add ads to like the search bar or something. *(cough) Windows 10 (cough)*
---
> from: [**anatolinicolae**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/19#issuecomment-394368625) on: **2018-6-4**

# Windows is not genuine
Your computer might not be running a counterfeit copy of Windows.
In order to view this comment you must activate today. [Activate Windows now.](http://fakeupdate.net/win10u/)
---
> from: [**mistermantas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/19#issuecomment-394369634) on: **2018-6-4**

&gt; Your computer might *not* be running a counterfeit copy of Windows.

Oh, is it supposed to be running a counterfeit copy? 🤔 
---
> from: [**anatolinicolae**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/19#issuecomment-394369837) on: **2018-6-4**

🤔
---
> from: [**mistermantas**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/19#issuecomment-394370374) on: **2018-6-4**

Well, now that that’s done, what do you want to do now?
---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/19#issuecomment-394390665) on: **2018-6-4**

Ok guys, you&#x27;ve had your fun. But this place is focused on a mission. Let&#x27;s not create a bunch of noise for the growing number of people following this repo. You don&#x27;t joke around in the Linux or Node or Python Issue databases, do you? I mean, if you guys had a joke that would make a lot of people laugh, sure. 😜 
