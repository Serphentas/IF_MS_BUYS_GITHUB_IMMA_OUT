# [We can make a new product which replaces github, open source, and the hosting costs can be shared](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/36)

> state: **closed** opened by: **cooldesigns** on: **2018-6-4**

We can find a method for share hosting costs: Example ideas:
- Calculating user&#x27;s hosting cost and invoicing unless the cost will be bigger than 1$
- Decentralize hosting. Using serverless. Like spotify.
- Donation on open collective
- The super idea: Make a feature: Allow users to donate to open source software and get commission for hosting costs.

Software is basic for us. We can develop gags, and we can make it&#x27;s features more social. It&#x27;s already ready and stable. 

### Comments

---
> from: [**vassudanagunta**](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT/issues/36#issuecomment-394467481) on: **2018-6-4**

Thank you! But this is a duplicate of #25. Please copy your ideas there. 
